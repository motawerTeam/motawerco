<?php

return [

    /**
     * Nova User resource tool class.
     */
    'userResource' => 'App\Nova\User',
    'GovermentResource' => 'App\Nova\Goverment',
    'headareaResource' => 'App\Nova\HeadArea',
    'schoollanguageResource' => 'App\Nova\SchoolLanguage',
    'schoolsResource' => 'App\Nova\Schools',
    'typeschoolResource' => 'App\Nova\TypeSchool',
    'areaResource' => 'App\Nova\Area',

    /**
     * The group associated with the resource
     */
    'roleResourceGroup' => 'Other',
];
