<?php $__env->startSection('title'); ?>
    About
<?php $__env->stopSection(); ?>
<?php $__env->startSection('menu'); ?>
    active
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <style>
        .elementor-599 .elementor-element.elementor-element-be15109 {
            background-color: #212d4b !important;
        }

        .home .page-title-bar {
            display: block !important;
            width: 100% !important;
        }
    </style>
    <div id="page-title-bar" class="page-title-bar">
        <div class="container">
            <div class="wrap w-100 d-flex align-items-center">
                <div class="page-title-bar-inner d-flex flex-column align-items-center w-100">
                    <div class="page-header"><h1 class="page-title typo-heading">About</h1></div>
                    <div class="breadcrumb"><span property="itemListElement" typeof="ListItem">
                            <a property="item"
                               typeof="WebPage"
                               title="Go to Startor."
                               href="<?php echo e(url('/')); ?>"
                               class="home"><span
                                    property="name">Motawer</span></a><meta property="position" content="1"></span> &gt;
                        <span class="post post-page current-item">About</span></div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div id="content" class="site-content">
        <div class="wrap">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div data-elementor-type="post" data-elementor-id="1208" class="elementor elementor-1208"
                         data-elementor-settings="[]">
                        <div class="elementor-inner">
                            <div class="elementor-section-wrap">
                                <div class="elementor-element elementor-element-5988135f elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                                     data-id="5988135f" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-no">
                                        <div class="elementor-row">
                                            <div class="elementor-element elementor-element-45743326 animated-fast elementor-invisible elementor-column elementor-col-33 elementor-top-column"
                                                 data-id="45743326" data-element_type="column"
                                                 data-settings="{&quot;animation&quot;:&quot;opal-move-up&quot;}">
                                                <div class="elementor-column-wrap  elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div class="elementor-element elementor-element-a9dc298 elementor-widget elementor-widget-heading"
                                                             data-id="a9dc298" data-element_type="widget"
                                                             data-widget_type="heading.default">
                                                            <div class="elementor-widget-container"><h2
                                                                    class="elementor-heading-title elementor-size-default">
                                                                    We design brands, campaigns & digital
                                                                    projects</h2></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-9452973 animated-fast elementor-invisible elementor-column elementor-col-33 elementor-top-column"
                                                 data-id="9452973" data-element_type="column"
                                                 data-settings="{&quot;animation&quot;:&quot;opal-move-up&quot;,&quot;animation_delay&quot;:150}">
                                                <div class="elementor-column-wrap  elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div class="elementor-element elementor-element-dac5119 elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                             data-id="dac5119" data-element_type="widget"
                                                             data-widget_type="icon-box.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="elementor-icon-box-wrapper">
                                                                    <div class="elementor-icon-box-content">
                                                                        <span class="elementor-icon-box-subtitle"></span>
                                                                        <h3 class="elementor-icon-box-title">
                                                                            <span>Unique designs</span></h3>
                                                                        <p class="elementor-icon-box-description">
                                                                            Pixel-perfect replication of the
                                                                            designers is intended for both
                                                                            front-end & back-end developers to
                                                                            build their pages with greater
                                                                            comfort.</p></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-f0af1f1 animated-fast elementor-invisible elementor-column elementor-col-33 elementor-top-column"
                                                 data-id="f0af1f1" data-element_type="column"
                                                 data-settings="{&quot;animation&quot;:&quot;opal-move-up&quot;,&quot;animation_delay&quot;:300}">
                                                <div class="elementor-column-wrap  elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div class="elementor-element elementor-element-e605a83 elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                             data-id="e605a83" data-element_type="widget"
                                                             data-widget_type="icon-box.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="elementor-icon-box-wrapper">
                                                                    <div class="elementor-icon-box-content">
                                                                        <span class="elementor-icon-box-subtitle"></span>
                                                                        <h3 class="elementor-icon-box-title">
                                                                            <span>Fresh ideas</span></h3>
                                                                        <p class="elementor-icon-box-description">
                                                                            You can select your favorite layouts
                                                                            & elements for particular projects
                                                                            with unlimited customization
                                                                            possibilities.</p></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>