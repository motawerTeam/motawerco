<!doctype html>
<html lang="en-US" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link href="<?php echo e(asset('dist/all.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('dist/fontawesome.css')); ?>" rel="stylesheet">
    <script defer src="<?php echo e(asset('dist/fontawesome.js')); ?>"></script>
    <script defer src="<?php echo e(asset('dist/all.js')); ?>"></script>
    <link type="text/css" media="all"
          href="https://demo2.wpopal.com/startor/wp-content/cache/autoptimize/css/autoptimize_b8129fce1d221678ee1a1a17e210f2b9.css"
          rel="stylesheet"/>
    <title><?php echo e(__('general.site')); ?> | <?php echo $__env->yieldContent('title'); ?></title>
    <link rel='dns-prefetch' href='//demo2.wpopal.com'/>
    <link rel='dns-prefetch' href='//fonts.googleapis.com'/>
    <link rel='dns-prefetch' href='//s.w.org'/>
    <link href='https://fonts.gstatic.com' crossorigin rel='preconnect'/>
    <script type="text/javascript">
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/",
            "svgExt": ".svg",
            "source": {"concatemoji": "https:\/\/demo2.wpopal.com\/startor\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.4"}
        };
        !function (a, b, c) {
            function d(a, b) {
                var c = String.fromCharCode;
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, a), 0, 0);
                var d = k.toDataURL();
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, b), 0, 0);
                var e = k.toDataURL();
                return d === e
            }

            function e(a) {
                var b;
                if (!l || !l.fillText) return !1;
                switch (l.textBaseline = "top", l.font = "600 32px Arial", a) {
                    case"flag":
                        return !(b = d([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819])) && (b = d([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]), !b);
                    case"emoji":
                        return b = d([55357, 56424, 55356, 57342, 8205, 55358, 56605, 8205, 55357, 56424, 55356, 57340], [55357, 56424, 55356, 57342, 8203, 55358, 56605, 8203, 55357, 56424, 55356, 57340]), !b
                }
                return !1
            }

            function f(a) {
                var c = b.createElement("script");
                c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
            }

            var g, h, i, j, k = b.createElement("canvas"), l = k.getContext && k.getContext("2d");
            for (j = Array("flag", "emoji"), c.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, i = 0; i < j.length; i++) c.supports[j[i]] = e(j[i]), c.supports.everything = c.supports.everything && c.supports[j[i]], "flag" !== j[i] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[j[i]]);
            c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function () {
                c.DOMReady = !0
            }, c.supports.everything || (h = function () {
                c.readyCallback()
            }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", h, !1), a.addEventListener("load", h, !1)) : (a.attachEvent("onload", h), b.attachEvent("onreadystatechange", function () {
                "complete" === b.readyState && c.readyCallback()
            })), g = c.source || {}, g.concatemoji ? f(g.concatemoji) : g.wpemoji && g.twemoji && (f(g.twemoji), f(g.wpemoji)))
        }(window, document, window._wpemojiSettings);</script>
    <link rel='stylesheet' id='elementor-post-599-css'
          href='https://demo2.wpopal.com/startor/wp-content/cache/autoptimize/css/autoptimize_single_ff0ca0cd076e884bebaff2cc5f7155a9.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='elementor-post-1069-css'
          href='https://demo2.wpopal.com/startor/wp-content/cache/autoptimize/css/autoptimize_single_af096684d083c8c9ea4623b75b438a2b.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='elementor-post-1039-css'
          href='https://demo2.wpopal.com/startor/wp-content/cache/autoptimize/css/autoptimize_single_93779ec624c4e3b44c71e9f41f5e2072.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='elementor-post-1716-css'
          href='https://demo2.wpopal.com/startor/wp-content/cache/autoptimize/css/autoptimize_single_58f5cc8ab46e6547826323431df3354a.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='elementor-post-143-css'
          href='https://demo2.wpopal.com/startor/wp-content/cache/autoptimize/css/autoptimize_single_01413975bd3c6dce13205a7b45cab46d.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='elementor-global-css'
          href='https://demo2.wpopal.com/startor/wp-content/cache/autoptimize/css/autoptimize_single_5726df5c1a781148ac4cecbd7af0d56c.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='elementor-post-176-css'
          href='https://demo2.wpopal.com/startor/wp-content/cache/autoptimize/css/autoptimize_single_93847e5a6fd6a705b397d7d70d05a715.css'
          type='text/css' media='all'/>
    <style id='startor-style-inline-css' type='text/css'>
        body, input, button, button[type="submit"], select, textarea {
            font-family: "Poppins", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
            font-weight: 400;
            color: #616a73
        }

        html {
            font-size: 16px;
        }

        .c-heading {
            color: #000000;
        }

        .c-primary {
            color: #5ac2f3;
        }

        .bg-primary {
            background-color: #5ac2f3;
        }

        .b-primary {
            border-color: #5ac2f3;
        }

        .button-primary:hover {
            background-color: #2ab0ef;
            border-color: #2ab0ef;
        }

        .c-secondary {
            color: #3d1099;
        }

        .bg-secondary {
            background-color: #3d1099;
        }

        .b-secondary {
            border-color: #3d1099;
        }

        .button-secondary:hover {
            background-color: #2b0b6a;
        }

        input[type="text"]::placeholder, input[type="email"]::placeholder, input[type="url"]::placeholder, input[type="password"]::placeholder, input[type="search"]::placeholder, input[type="number"]::placeholder, input[type="tel"]::placeholder, input[type="range"]::placeholder, input[type="date"]::placeholder, input[type="month"]::placeholder, input[type="week"]::placeholder, input[type="time"]::placeholder, input[type="datetime"]::placeholder, input[type="datetime-local"]::placeholder, input[type="color"]::placeholder, input[type="text"], input[type="email"], input[type="url"], input[type="password"], input[type="search"], input[type="number"], input[type="tel"], input[type="range"], input[type="date"], input[type="month"], input[type="week"], input[type="time"], input[type="datetime"], input[type="datetime-local"], input[type="color"], textarea::placeholder, textarea, a, .mainmenu-container li a span, .comment-metadata, .comment-metadata a, .widget.widget_archive a, .widget.widget_categories a, .widget.widget_nav_menu a, .widget.widget_meta a, .widget.widget_pages a, .c-body, .site-header-account .account-links-menu li a, .site-header-account .account-dashboard li a, .comment-form label, .comment-form a, .widget .tagcloud a, .widget.widget_tag_cloud a, .cart-collaterals .cart_totals th, #payment .payment_methods li.woocommerce-notice, #payment .payment_methods li.woocommerce-notice--info, #payment .payment_methods li.woocommerce-info, table.woocommerce-checkout-review-order-table th, .single-product .stock.out-of-stock, .opal-style-1.search-form-wapper .search-submit span, .opal-style-1.search-form-wapper .search-submit span:before, .woo-variation-swatches-stylesheet-disabled .variable-items-wrapper .variable-item.button-variable-item:not(.radio-variable-item):after, .woocommerce-shipping-fields .select2-container--default .select2-selection--single .select2-selection__rendered, .woocommerce-billing-fields .select2-container--default .select2-selection--single .select2-selection__rendered, .opal-currency_switcher .list-currency button[type="submit"], .select-items div {
            color: #616a73;
        }

        .widget-area strong, h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, h3.entry-title, .related-posts .related-heading, .comments-title, .comment-respond .comment-reply-title, .h4, .single .navigation .nav-link, h2.widget-title, h2.widgettitle, .column-item .entry-header .entry-title, .h5, .h6, blockquote, th, .main-navigation .top-menu > li > a, .main-navigation:not(.navigation-dark) .top-menu > li > a, .mainmenu-container ul ul .menu-item > a:hover, .mainmenu-container ul ul .menu-item > a:active, .mainmenu-container ul ul .menu-item > a:focus, .entry-content blockquote cite a, .entry-content strong, .entry-content dt, .entry-content th, .entry-content dt a, .entry-content th a, .single .navigation .nav-link a, .comment-content strong, .comment-author, .comment-author a, .comment-metadata a.comment-edit-link, .comment-reply-link, .comment-content table th, .comment-content table td a, .comment-content dt, .elementor-element .elementor-widget-wp-widget-recent-posts .title-post a, .widget a, .widget_rss .rss-date, .widget_rss li cite, .c-heading, fieldset legend, .entry-meta .author a:not(:hover), .author-wrapper .author-name, .search .site-content .page-title, .site-header-account .login-form-title, table.shop_table_responsive tbody th, .site-header-cart .widget_shopping_cart p.total .woocommerce-Price-amount, .site-header-cart .shopping_cart_nav p.total .woocommerce-Price-amount, .filter-close, table.cart:not(.wishlist_table) th, .cart-collaterals .cart_totals .order-total .woocommerce-Price-amount, .cart-collaterals .cart_totals .amount, .woocommerce-checkout .woocommerce-form-coupon-toggle .woocommerce-info, #payment .payment_methods > .wc_payment_method > label, table.woocommerce-checkout-review-order-table .order-total .woocommerce-Price-amount, table.woocommerce-checkout-review-order-table .product-name, .woocommerce-billing-fields label, .woocommerce-billing-fields > h3, .woocommerce-additional-fields label, .cart th, .shop_table th, .woocommerce-account .woocommerce-MyAccount-content strong, .woocommerce-account .woocommerce-MyAccount-content .woocommerce-Price-amount, .osf-sorting .display-mode button.active, .osf-sorting .display-mode button:hover, .woocommerce-Tabs-panel table.shop_attributes th, #osf-accordion-container table.shop_attributes th, .woocommerce-tabs#osf-accordion-container [data-accordion] [data-control], .elementor-accordion .elementor-tab-title, .elementor-featured-box-wrapper .elementor-featured-box-title, .elementor-widget-opal-image-hotspots .elementor-accordion .elementor-tab-title, .elementor-price-table__currency, .elementor-price-table__integer-part, .elementor-price-table__feature-inner span.item-active, .elementor-price-table__period, .elementor-progress-percentage, .elementor-widget-progress .elementor-title, .elementor-teams-wrapper .elementor-team-name, .wishlist_table .product-price {
            color: #000000;
        }

        blockquote:before, .mainmenu-container li.current-menu-parent > a, .mainmenu-container .menu-item > a:hover, .menu-toggle, .site-header .header-group .search-submit:hover, .site-header .header-group .search-submit:focus, .cat-links a, .entry-meta .cat-links a, .more-link, .pbr-social-share a:hover, .single .navigation .nav-title, .error404 .sub-h2-1, .breadcrumb a:hover, .breadcrumb a:hover span, .comment-author a:hover, .comment-metadata a:hover, .elementor-element .elementor-widget-wp-widget-recent-posts .title-post a:hover, .widget a:hover, .widget a:focus, .widget.widget_archive a:hover, .widget.widget_archive a:focus, .widget.widget_categories a:hover, .widget.widget_categories a:focus, .widget.widget_nav_menu a:hover, .widget.widget_nav_menu a:focus, .widget.widget_meta a:hover, .widget.widget_meta a:focus, .widget.widget_pages a:hover, .widget.widget_pages a:focus, .title-with-icon:before, .widget_recent_entries li a:hover, .widget_recent_entries li a:active, .widget_search button[type="submit"], .widget .tagcloud a:hover, .widget .tagcloud a:focus, .widget.widget_tag_cloud a:hover, .widget.widget_tag_cloud a:focus, .button-outline-primary, .elementor-wpcf7-button-outline_primary input[type="button"], .elementor-wpcf7-button-outline_primary input[type="submit"], .elementor-wpcf7-button-outline_primary button[type="submit"], .mailchimp-button-outline_primary button, .widget_shopping_cart .buttons .button:nth-child(odd), .elementor-element .elementor-button-outline_primary .elementor-button, .c-primary, .navigation-button .menu-toggle:hover, .navigation-button .menu-toggle:focus, .entry-header .entry-title a:hover, .entry-content blockquote cite a:hover, .site-header-account .account-dropdown a.register-link, .site-header-account .account-dropdown a.lostpass-link, .site-header-account .account-links-menu li a:hover, .site-header-account .account-dashboard li a:hover, .comment-form a:hover, .wp_widget_tag_cloud a:hover, .wp_widget_tag_cloud a:focus, #secondary .elementor-widget-container h5:first-of-type, .elementor-nav-menu-popup .mfp-close, .site-header-cart .widget_shopping_cart .product_list_widget li a:hover, .site-header-cart .widget_shopping_cart .product_list_widget li a:focus, .site-header-cart .shopping_cart_nav .product_list_widget li a:hover, .site-header-cart .shopping_cart_nav .product_list_widget li a:focus, .woocommerce-checkout .woocommerce-form-coupon-toggle .woocommerce-info a, .woocommerce-checkout .woocommerce-form-coupon-toggle .woocommerce-info a:hover, .woocommerce-privacy-policy-link, .opal-currency_switcher .list-currency button[type="submit"]:hover, .opal-currency_switcher .list-currency li.active button[type="submit"], ul.products li.product.osf-product-list .price, ul.products li.product .posfed_in a:hover, .select-items div:hover, .single-product div.product .woocommerce-product-gallery .woocommerce-product-gallery__trigger:hover, .button-wrapper #chart-button, .product_list_widget a:hover, .product_list_widget a:active, .product_list_widget a:focus, .woocommerce-product-list a:hover, .woocommerce-product-list a:active, .woocommerce-product-list a:focus, #secondary .elementor-widget-wp-widget-recent-posts a, .elementor-accordion .elementor-tab-title.elementor-active, .contactform-content .form-title, .elementor-widget-opal-countdown .elementor-countdown-digits, .elementor-featured-box-wrapper i, .elementor-widget-heading .sub-title, .elementor-widget-opal-image-hotspots .elementor-accordion .elementor-tab-title.elementor-active, .portfolio .portfolio-content .entry-title a:hover, .portfolio .entry-category a:hover, .elementor-portfolio-style-overlay .portfolio .portfolio-content .entry-title a:hover, .elementor-portfolio-style-overlay .portfolio .portfolio-content .entry-category a:hover, .elementor-portfolio__filter:hover, .single-portfolio-navigation .nav-link:hover span, .elementor-button-default .elementor-button, .elementor-teams-wrapper .elementor-team-name:hover, .elementor-teams-wrapper .team-icon-socials li:hover a, .elementor-widget-opal-testimonials .layout_4 .elementor-testimonial-quote, .elementor-video-icon, .header-button .count, .header-button:hover, .header-button:focus, .opal-canvas-filter .widget.widget_product_categories ul li a:hover, .opal-canvas-filter .widget.widget_product_categories ul li.current-cat a, .woocommerce-MyAccount-navigation ul li.is-active a, ul.products li.product .price, .single-product div.product .entry-summary .yith-wcwl-add-to-wishlist > div > a:hover:before, .single-product div.product .entry-summary .compare:hover:before, .single-product div.product .summary .price, .single-product div.product .product_meta .sku_wrapper a:hover, .single-product div.product .product_meta .posted_in a:hover, .single-product div.product .product_meta .tagged_as a:hover, .single-product div.product .pbr-social-share a:hover, .single-product .woocommerce-tabs .wc-tabs .active a, .woocommerce-tabs ul.tabs li a:hover, .widget_price_filter .price_slider_amount .price_label span, .woo-variation-swatches-stylesheet-disabled .variable-items-wrapper .variable-item.button-variable-item:not(.radio-variable-item):hover, .woo-variation-swatches-stylesheet-disabled .variable-items-wrapper .variable-item.button-variable-item.selected:not(.radio-variable-item), .product-style-1 li.product h2 a:hover, .product-style-1 li.product h3 a:hover, .product-style-1 li.product .woocommerce-loop-product__title a:hover {
            color: #5ac2f3;
        }

        .f-primary {
            fill: #5ac2f3;
        }

        .mp-level::-webkit-scrollbar-thumb, .page-numbers:not(ul):not(.dots):hover, .page-numbers:not(ul):not(.dots):focus, .page-numbers.current:not(ul):not(.dots), .comments-link span, .single .navigation > div:hover, .page-links a:hover .page-number, .page-links a:focus .page-number, .page-links > .page-number, .widget_meta a:hover:before, .widget_pages a:hover:before, .widget_archive a:hover:before, .widget_categories a:hover:before, .wp_widget_tag_cloud a:hover:before, .wp_widget_tag_cloud a:focus:before, .button-primary, input[type="reset"], input.secondary[type="button"], input.secondary[type="reset"], input.secondary[type="submit"], input[type="button"], input[type="submit"], button[type="submit"], .page .edit-link a.post-edit-link, .error404 .go-back, .scrollup, .comment-form .form-submit input[type="submit"], .elementor-wpcf7-button-primary input[type="button"][type="submit"], .elementor-wpcf7-button-primary input[type="submit"], .elementor-wpcf7-button-primary button[type="submit"], .mailchimp-button-primary button, .wc-proceed-to-checkout .button, .woocommerce-cart .return-to-shop a, .wishlist_table .product-add-to-cart a.button, .woocommerce-MyAccount-content .woocommerce-Pagination .woocommerce-button, .widget_shopping_cart .buttons .button, .button-default:hover, .elementor-element .elementor-button-primary .elementor-button, .button-outline-primary:hover, .elementor-wpcf7-button-outline_primary input:hover[type="button"], .elementor-wpcf7-button-outline_primary input:hover[type="submit"], .elementor-wpcf7-button-outline_primary button:hover[type="submit"], .mailchimp-button-outline_primary button:hover, .widget_shopping_cart .buttons .button:hover:nth-child(odd), .button-outline-primary:active, .elementor-wpcf7-button-outline_primary input:active[type="button"], .elementor-wpcf7-button-outline_primary input:active[type="submit"], .elementor-wpcf7-button-outline_primary button:active[type="submit"], .mailchimp-button-outline_primary button:active, .widget_shopping_cart .buttons .button:active:nth-child(odd), .button-outline-primary.active, .elementor-wpcf7-button-outline_primary input.active[type="button"], .elementor-wpcf7-button-outline_primary input.active[type="submit"], .elementor-wpcf7-button-outline_primary button.active[type="submit"], .mailchimp-button-outline_primary button.active, .widget_shopping_cart .buttons .active.button:nth-child(odd), .show > .button-outline-primary.dropdown-toggle, .elementor-wpcf7-button-outline_primary .show > input.dropdown-toggle[type="button"], .elementor-wpcf7-button-outline_primary .show > input.dropdown-toggle[type="submit"], .elementor-wpcf7-button-outline_primary .show > button.dropdown-toggle[type="submit"], .mailchimp-button-outline_primary .show > button.dropdown-toggle, .widget_shopping_cart .buttons .show > .dropdown-toggle.button:nth-child(odd), .elementor-element .elementor-button-outline_primary .elementor-button:hover, .elementor-element .elementor-button-outline_primary .elementor-button:active, .elementor-element .elementor-button-outline_primary .elementor-button:focus, .bg-primary, .elementor-widget-divider .elementor-divider-separator:before, .elementor-flip-box__front, .elementor-widget-heading .sub-title:after, .elementor-widget-opal-image-hotspots .scrollbar-inner > .scroll-element .scroll-bar, .opal-image-hotspots-main-icons .opal-image-hotspots-icon, .elementor-widget-opal-image-gallery .gallery-item-overlay, .elementor-widget-opal-image-gallery .elementor-galerry__filter.elementor-active, .single-portfolio-summary .pbr-social-share a:hover, .elementor-timeline-carousel .timeline-carosuel-item .timeline-number, .elementor-timeline-carousel .timeline-carosuel-item:hover .timeline-number, .elementor-timeline-carousel .timeline-carosuel-item.timeline-item-activate .timeline-number, .timeline-item .timeline-number, .elementor-timeline-view-vertical .timeline-number, .notification-added-to-cart .ns-content, #payment .place-order .button:hover, form.register .button[type="submit"]:hover, li.product .time, .shop-action a[class*="product_type_"]:hover, .shop-action a.loading[class*="product_type_"], .shop-action .yith-wcqv-button:hover, .shop-action .yith-wcwl-add-to-wishlist > div > a:hover, .shop-action .compare:hover, .shop-action .yith-wcwl-add-to-wishlist > div > a:hover + .opal-loading-wislist:before, .single-product[class*="opal-comment-form"] .comment-form .form-submit .submit:hover, .single-product[class*="opal-comment-form"] .comment-form .form-submit .submit:active, .single-product[class*="opal-comment-form"] .comment-form .form-submit .submit:focus, .single-product .single_add_to_cart_button, .single-product .single_add_to_cart_button.disabled[type="submit"], .widget_price_filter .ui-slider .ui-slider-handle, .widget_price_filter .ui-slider .ui-slider-range, .handheld-footer-bar .cart .footer-cart-contents .count {
            background-color: #5ac2f3;
        }

        .button-primary, input[type="reset"], input.secondary[type="button"], input.secondary[type="reset"], input.secondary[type="submit"], input[type="button"], input[type="submit"], button[type="submit"], .page .edit-link a.post-edit-link, .error404 .go-back, .scrollup, .comment-form .form-submit input[type="submit"], .elementor-wpcf7-button-primary input[type="button"][type="submit"], .elementor-wpcf7-button-primary input[type="submit"], .elementor-wpcf7-button-primary button[type="submit"], .mailchimp-button-primary button, .wc-proceed-to-checkout .button, .woocommerce-cart .return-to-shop a, .wishlist_table .product-add-to-cart a.button, .woocommerce-MyAccount-content .woocommerce-Pagination .woocommerce-button, .widget_shopping_cart .buttons .button, .button-secondary, .secondary-button .search-submit, .elementor-wpcf7-button-secondary input[type="button"][type="submit"], .elementor-wpcf7-button-secondary input[type="submit"], .elementor-wpcf7-button-secondary button[type="submit"], .mailchimp-button-secondary button, input[type="text"]:focus, input[type="email"]:focus, input[type="url"]:focus, input[type="password"]:focus, input[type="search"]:focus, input[type="number"]:focus, input[type="tel"]:focus, input[type="range"]:focus, input[type="date"]:focus, input[type="month"]:focus, input[type="week"]:focus, input[type="time"]:focus, input[type="datetime"]:focus, input[type="datetime-local"]:focus, input[type="color"]:focus, textarea:focus, .single .navigation > div:hover, .wp_widget_tag_cloud a:hover:after, .wp_widget_tag_cloud a:focus:after, .widget .tagcloud a:hover, .widget .tagcloud a:focus, .widget.widget_tag_cloud a:hover, .widget.widget_tag_cloud a:focus, .wp_widget_tag_cloud a:hover, .wp_widget_tag_cloud a:focus, .button-default:hover, .elementor-element .elementor-button-primary .elementor-button, .button-outline-primary, .elementor-wpcf7-button-outline_primary input[type="button"], .elementor-wpcf7-button-outline_primary input[type="submit"], .elementor-wpcf7-button-outline_primary button[type="submit"], .mailchimp-button-outline_primary button, .widget_shopping_cart .buttons .button:nth-child(odd), .elementor-element .elementor-button-outline_primary .elementor-button, .button-outline-primary:hover, .elementor-wpcf7-button-outline_primary input:hover[type="button"], .elementor-wpcf7-button-outline_primary input:hover[type="submit"], .elementor-wpcf7-button-outline_primary button:hover[type="submit"], .mailchimp-button-outline_primary button:hover, .widget_shopping_cart .buttons .button:hover:nth-child(odd), .button-outline-primary:active, .elementor-wpcf7-button-outline_primary input:active[type="button"], .elementor-wpcf7-button-outline_primary input:active[type="submit"], .elementor-wpcf7-button-outline_primary button:active[type="submit"], .mailchimp-button-outline_primary button:active, .widget_shopping_cart .buttons .button:active:nth-child(odd), .button-outline-primary.active, .elementor-wpcf7-button-outline_primary input.active[type="button"], .elementor-wpcf7-button-outline_primary input.active[type="submit"], .elementor-wpcf7-button-outline_primary button.active[type="submit"], .mailchimp-button-outline_primary button.active, .widget_shopping_cart .buttons .active.button:nth-child(odd), .show > .button-outline-primary.dropdown-toggle, .elementor-wpcf7-button-outline_primary .show > input.dropdown-toggle[type="button"], .elementor-wpcf7-button-outline_primary .show > input.dropdown-toggle[type="submit"], .elementor-wpcf7-button-outline_primary .show > button.dropdown-toggle[type="submit"], .mailchimp-button-outline_primary .show > button.dropdown-toggle, .widget_shopping_cart .buttons .show > .dropdown-toggle.button:nth-child(odd), .elementor-element .elementor-button-outline_primary .elementor-button:hover, .elementor-element .elementor-button-outline_primary .elementor-button:active, .elementor-element .elementor-button-outline_primary .elementor-button:focus, .b-primary, .elementor-widget-opal-image-gallery .elementor-galerry__filter.elementor-active:before, .elementor-nav-menu--main .elementor-nav-menu ul, ul.elementor-price-table__features-list, .elementor-timeline-carousel .timeline-carosuel-item:hover .timeline-number, .elementor-timeline-carousel .timeline-carosuel-item.timeline-item-activate .timeline-number, .site-header-cart .widget_shopping_cart, #payment .place-order .button:hover, form.register .button[type="submit"]:hover, .single-product div.product .entry-summary .yith-wcwl-add-to-wishlist > div > a:hover, .single-product div.product .entry-summary .compare:hover, .single-product[class*="opal-comment-form"] .comment-form .form-submit .submit:hover, .single-product[class*="opal-comment-form"] .comment-form .form-submit .submit:active, .single-product[class*="opal-comment-form"] .comment-form .form-submit .submit:focus, .single-product .single_add_to_cart_button, .single-product .single_add_to_cart_button.disabled[type="submit"], .woocommerce-tabs ul.tabs li a:after, .osf-product-deal .woocommerce-product-list .opal-countdown .day, .otf-product-recently-content li:hover .product-thumbnail img {
            border-color: #5ac2f3;
        }

        .btn-link:focus, .elementor-element .elementor-button-link .elementor-button:focus, .btn-link:hover, .elementor-element .elementor-button-link .elementor-button:hover, .button-link:focus, .button-link:hover, a:hover, a:active, .cat-links a:hover, .entry-meta .cat-links a:hover, .widget_search button[type="submit"]:hover, .widget_search button[type="submit"]:focus {
            color: #2ab0ef;
        }

        .error404 .return-home:hover, .button-primary:hover, input:hover[type="reset"], input:hover[type="button"], input:hover[type="submit"], button:hover[type="submit"], .page .edit-link a.post-edit-link:hover, .error404 .go-back:hover, .scrollup:hover, .comment-form .form-submit input:hover[type="submit"], .elementor-wpcf7-button-primary input:hover[type="submit"], .elementor-wpcf7-button-primary button:hover[type="submit"], .mailchimp-button-primary button:hover, .wc-proceed-to-checkout .button:hover, .woocommerce-cart .return-to-shop a:hover, .wishlist_table .product-add-to-cart a.button:hover, .woocommerce-MyAccount-content .woocommerce-Pagination .woocommerce-button:hover, .widget_shopping_cart .buttons .button:hover, .button-primary:active, input:active[type="reset"], input:active[type="button"], input:active[type="submit"], button:active[type="submit"], .page .edit-link a.post-edit-link:active, .error404 .go-back:active, .scrollup:active, .comment-form .form-submit input:active[type="submit"], .elementor-wpcf7-button-primary input:active[type="submit"], .elementor-wpcf7-button-primary button:active[type="submit"], .mailchimp-button-primary button:active, .wc-proceed-to-checkout .button:active, .woocommerce-cart .return-to-shop a:active, .wishlist_table .product-add-to-cart a.button:active, .woocommerce-MyAccount-content .woocommerce-Pagination .woocommerce-button:active, .widget_shopping_cart .buttons .button:active, .button-primary.active, input.active[type="reset"], input.active[type="button"], input.active[type="submit"], button.active[type="submit"], .page .edit-link a.active.post-edit-link, .error404 .active.go-back, .active.scrollup, .comment-form .form-submit input.active[type="submit"], .elementor-wpcf7-button-primary input.active[type="submit"], .elementor-wpcf7-button-primary button.active[type="submit"], .mailchimp-button-primary button.active, .wc-proceed-to-checkout .active.button, .woocommerce-cart .return-to-shop a.active, .wishlist_table .product-add-to-cart a.active.button, .woocommerce-MyAccount-content .woocommerce-Pagination .active.woocommerce-button, .widget_shopping_cart .buttons .active.button, .show > .button-primary.dropdown-toggle, .show > input.dropdown-toggle[type="reset"], .show > input.dropdown-toggle[type="button"], .show > input.dropdown-toggle[type="submit"], .show > button.dropdown-toggle[type="submit"], .page .edit-link .show > a.dropdown-toggle.post-edit-link, .error404 .show > .dropdown-toggle.go-back, .show > .dropdown-toggle.scrollup, .comment-form .form-submit .show > input.dropdown-toggle[type="submit"], .elementor-wpcf7-button-primary .show > input.dropdown-toggle[type="submit"], .elementor-wpcf7-button-primary .show > button.dropdown-toggle[type="submit"], .mailchimp-button-primary .show > button.dropdown-toggle, .wc-proceed-to-checkout .show > .dropdown-toggle.button, .woocommerce-cart .return-to-shop .show > a.dropdown-toggle, .wishlist_table .product-add-to-cart .show > a.dropdown-toggle.button, .woocommerce-MyAccount-content .woocommerce-Pagination .show > .dropdown-toggle.woocommerce-button, .widget_shopping_cart .buttons .show > .dropdown-toggle.button, .elementor-element .elementor-button-primary .elementor-button:hover, .elementor-element .elementor-button-primary .elementor-button:active, .elementor-element .elementor-button-primary .elementor-button:focus, .single-product .single_add_to_cart_button:hover, .single-product .single_add_to_cart_button.disabled[type="submit"]:hover {
            background-color: #2ab0ef;
        }

        .button-primary:active, input:active[type="reset"], input:active[type="button"], input:active[type="submit"], button:active[type="submit"], .page .edit-link a.post-edit-link:active, .error404 .go-back:active, .scrollup:active, .comment-form .form-submit input:active[type="submit"], .elementor-wpcf7-button-primary input:active[type="submit"], .elementor-wpcf7-button-primary button:active[type="submit"], .mailchimp-button-primary button:active, .wc-proceed-to-checkout .button:active, .woocommerce-cart .return-to-shop a:active, .wishlist_table .product-add-to-cart a.button:active, .woocommerce-MyAccount-content .woocommerce-Pagination .woocommerce-button:active, .widget_shopping_cart .buttons .button:active, .button-primary.active, input.active[type="reset"], input.active[type="button"], input.active[type="submit"], button.active[type="submit"], .page .edit-link a.active.post-edit-link, .error404 .active.go-back, .active.scrollup, .comment-form .form-submit input.active[type="submit"], .elementor-wpcf7-button-primary input.active[type="submit"], .elementor-wpcf7-button-primary button.active[type="submit"], .mailchimp-button-primary button.active, .wc-proceed-to-checkout .active.button, .woocommerce-cart .return-to-shop a.active, .wishlist_table .product-add-to-cart a.active.button, .woocommerce-MyAccount-content .woocommerce-Pagination .active.woocommerce-button, .widget_shopping_cart .buttons .active.button, .show > .button-primary.dropdown-toggle, .show > input.dropdown-toggle[type="reset"], .show > input.dropdown-toggle[type="button"], .show > input.dropdown-toggle[type="submit"], .show > button.dropdown-toggle[type="submit"], .page .edit-link .show > a.dropdown-toggle.post-edit-link, .error404 .show > .dropdown-toggle.go-back, .show > .dropdown-toggle.scrollup, .comment-form .form-submit .show > input.dropdown-toggle[type="submit"], .elementor-wpcf7-button-primary .show > input.dropdown-toggle[type="submit"], .elementor-wpcf7-button-primary .show > button.dropdown-toggle[type="submit"], .mailchimp-button-primary .show > button.dropdown-toggle, .wc-proceed-to-checkout .show > .dropdown-toggle.button, .woocommerce-cart .return-to-shop .show > a.dropdown-toggle, .wishlist_table .product-add-to-cart .show > a.dropdown-toggle.button, .woocommerce-MyAccount-content .woocommerce-Pagination .show > .dropdown-toggle.woocommerce-button, .widget_shopping_cart .buttons .show > .dropdown-toggle.button, .button-secondary:active, .secondary-button .search-submit:active, .elementor-wpcf7-button-secondary input:active[type="submit"], .elementor-wpcf7-button-secondary button:active[type="submit"], .mailchimp-button-secondary button:active, .button-secondary.active, .secondary-button .active.search-submit, .elementor-wpcf7-button-secondary input.active[type="submit"], .elementor-wpcf7-button-secondary button.active[type="submit"], .mailchimp-button-secondary button.active, .show > .button-secondary.dropdown-toggle, .secondary-button .show > .dropdown-toggle.search-submit, .elementor-wpcf7-button-secondary .show > input.dropdown-toggle[type="submit"], .elementor-wpcf7-button-secondary .show > button.dropdown-toggle[type="submit"], .mailchimp-button-secondary .show > button.dropdown-toggle, .error404 .return-home:hover, .button-primary:hover, input:hover[type="reset"], input:hover[type="button"], input:hover[type="submit"], button:hover[type="submit"], .page .edit-link a.post-edit-link:hover, .error404 .go-back:hover, .scrollup:hover, .comment-form .form-submit input:hover[type="submit"], .elementor-wpcf7-button-primary input:hover[type="submit"], .elementor-wpcf7-button-primary button:hover[type="submit"], .mailchimp-button-primary button:hover, .wc-proceed-to-checkout .button:hover, .woocommerce-cart .return-to-shop a:hover, .wishlist_table .product-add-to-cart a.button:hover, .woocommerce-MyAccount-content .woocommerce-Pagination .woocommerce-button:hover, .widget_shopping_cart .buttons .button:hover, .elementor-element .elementor-button-primary .elementor-button:hover, .elementor-element .elementor-button-primary .elementor-button:active, .elementor-element .elementor-button-primary .elementor-button:focus, .single-product .single_add_to_cart_button:hover, .single-product .single_add_to_cart_button.disabled[type="submit"]:hover {
            border-color: #2ab0ef;
        }

        .error404 .sub-h2-2, .button-outline-secondary, .elementor-wpcf7-button-outline_secondary input[type="button"][type="submit"], .elementor-wpcf7-button-outline_secondary input[type="submit"], .elementor-wpcf7-button-outline_secondary button[type="submit"], .mailchimp-button-outline_secondary button, .elementor-element .elementor-button-outline_secondary .elementor-button, .c-secondary, .author-wrapper .author-name h6, .contactform-content button.mfp-close {
            color: #3d1099;
        }

        .f-secondary {
            fill: #3d1099;
        }

        .button-secondary, .secondary-button .search-submit, .elementor-wpcf7-button-secondary input[type="button"][type="submit"], .elementor-wpcf7-button-secondary input[type="submit"], .elementor-wpcf7-button-secondary button[type="submit"], .mailchimp-button-secondary button, .elementor-button-secondary button[type="submit"], .elementor-button-secondary input[type="button"], .elementor-button-secondary input[type="submit"], .elementor-element .elementor-button-secondary .elementor-button, .button-outline-secondary:hover, .elementor-wpcf7-button-outline_secondary input:hover[type="submit"], .elementor-wpcf7-button-outline_secondary button:hover[type="submit"], .mailchimp-button-outline_secondary button:hover, .button-outline-secondary:active, .elementor-wpcf7-button-outline_secondary input:active[type="submit"], .elementor-wpcf7-button-outline_secondary button:active[type="submit"], .mailchimp-button-outline_secondary button:active, .button-outline-secondary.active, .elementor-wpcf7-button-outline_secondary input.active[type="submit"], .elementor-wpcf7-button-outline_secondary button.active[type="submit"], .mailchimp-button-outline_secondary button.active, .show > .button-outline-secondary.dropdown-toggle, .elementor-wpcf7-button-outline_secondary .show > input.dropdown-toggle[type="submit"], .elementor-wpcf7-button-outline_secondary .show > button.dropdown-toggle[type="submit"], .mailchimp-button-outline_secondary .show > button.dropdown-toggle, .elementor-element .elementor-button-outline_secondary .elementor-button:hover, .elementor-element .elementor-button-outline_secondary .elementor-button:active, .elementor-element .elementor-button-outline_secondary .elementor-button:focus, .bg-secondary, #secondary .elementor-widget-wp-widget-categories a:before, .elementor-flip-box__back, #secondary .elementor-nav-menu a:before, .e--pointer-dot a:before {
            background-color: #3d1099;
        }

        .form-control:focus, .button-secondary, .secondary-button .search-submit, .elementor-wpcf7-button-secondary input[type="button"][type="submit"], .elementor-wpcf7-button-secondary input[type="submit"], .elementor-wpcf7-button-secondary button[type="submit"], .mailchimp-button-secondary button, .elementor-button-secondary button[type="submit"], .elementor-button-secondary input[type="button"], .elementor-button-secondary input[type="submit"], .elementor-element .elementor-button-secondary .elementor-button, .button-outline-secondary, .elementor-wpcf7-button-outline_secondary input[type="button"][type="submit"], .elementor-wpcf7-button-outline_secondary input[type="submit"], .elementor-wpcf7-button-outline_secondary button[type="submit"], .mailchimp-button-outline_secondary button, .elementor-element .elementor-button-outline_secondary .elementor-button, .button-outline-secondary:hover, .elementor-wpcf7-button-outline_secondary input:hover[type="submit"], .elementor-wpcf7-button-outline_secondary button:hover[type="submit"], .button-outline-secondary:active, .elementor-wpcf7-button-outline_secondary input:active[type="submit"], .elementor-wpcf7-button-outline_secondary button:active[type="submit"], .button-outline-secondary.active, .elementor-wpcf7-button-outline_secondary input.active[type="submit"], .elementor-wpcf7-button-outline_secondary button.active[type="submit"], .show > .button-outline-secondary.dropdown-toggle, .elementor-wpcf7-button-outline_secondary .show > input.dropdown-toggle[type="submit"], .elementor-wpcf7-button-outline_secondary .show > button.dropdown-toggle[type="submit"], .mailchimp-button-outline_secondary .show > button.dropdown-toggle, .elementor-element .elementor-button-outline_secondary .elementor-button:hover, .elementor-element .elementor-button-outline_secondary .elementor-button:active, .elementor-element .elementor-button-outline_secondary .elementor-button:focus, .b-secondary {
            border-color: #3d1099;
        }

        .button-secondary:hover, .secondary-button .search-submit:hover, .elementor-wpcf7-button-secondary input:hover[type="submit"], .elementor-wpcf7-button-secondary button:hover[type="submit"], .mailchimp-button-secondary button:hover, .button-secondary:active, .secondary-button .search-submit:active, .elementor-wpcf7-button-secondary input:active[type="submit"], .elementor-wpcf7-button-secondary button:active[type="submit"], .mailchimp-button-secondary button:active, .button-secondary.active, .secondary-button .active.search-submit, .elementor-wpcf7-button-secondary input.active[type="submit"], .elementor-wpcf7-button-secondary button.active[type="submit"], .mailchimp-button-secondary button.active, .show > .button-secondary.dropdown-toggle, .secondary-button .show > .dropdown-toggle.search-submit, .elementor-wpcf7-button-secondary .show > input.dropdown-toggle[type="submit"], .elementor-wpcf7-button-secondary .show > button.dropdown-toggle[type="submit"], .mailchimp-button-secondary .show > button.dropdown-toggle, .elementor-button-secondary button[type="submit"]:hover, .elementor-button-secondary button[type="submit"]:active, .elementor-button-secondary button[type="submit"]:focus, .elementor-button-secondary input[type="button"]:hover, .elementor-button-secondary input[type="button"]:active, .elementor-button-secondary input[type="button"]:focus, .elementor-button-secondary input[type="submit"]:hover, .elementor-button-secondary input[type="submit"]:active, .elementor-button-secondary input[type="submit"]:focus, .elementor-element .elementor-button-secondary .elementor-button:hover, .elementor-element .elementor-button-secondary .elementor-button:active, .elementor-element .elementor-button-secondary .elementor-button:focus {
            background-color: #2b0b6a;
        }

        .button-secondary:hover, .secondary-button .search-submit:hover, .elementor-wpcf7-button-secondary input:hover[type="submit"], .elementor-wpcf7-button-secondary button:hover[type="submit"], .mailchimp-button-secondary button:hover, .button-secondary:active, .secondary-button .search-submit:active, .elementor-wpcf7-button-secondary input:active[type="submit"], .elementor-wpcf7-button-secondary button:active[type="submit"], .mailchimp-button-secondary button:active, .button-secondary.active, .secondary-button .active.search-submit, .elementor-wpcf7-button-secondary input.active[type="submit"], .elementor-wpcf7-button-secondary button.active[type="submit"], .mailchimp-button-secondary button.active, .show > .button-secondary.dropdown-toggle, .secondary-button .show > .dropdown-toggle.search-submit, .elementor-wpcf7-button-secondary .show > input.dropdown-toggle[type="submit"], .elementor-wpcf7-button-secondary .show > button.dropdown-toggle[type="submit"], .mailchimp-button-secondary .show > button.dropdown-toggle, .elementor-button-secondary button[type="submit"]:hover, .elementor-button-secondary button[type="submit"]:active, .elementor-button-secondary button[type="submit"]:focus, .elementor-button-secondary input[type="button"]:hover, .elementor-button-secondary input[type="button"]:active, .elementor-button-secondary input[type="button"]:focus, .elementor-button-secondary input[type="submit"]:hover, .elementor-button-secondary input[type="submit"]:active, .elementor-button-secondary input[type="submit"]:focus, .elementor-element .elementor-button-secondary .elementor-button:hover, .elementor-element .elementor-button-secondary .elementor-button:active, .elementor-element .elementor-button-secondary .elementor-button:focus {
            border-color: #2b0b6a;
        }

        .row, body.opal-default-content-layout-2cr #content .wrap, body.opal-content-layout-2cl #content .wrap, body.opal-content-layout-2cr #content .wrap, [data-opal-columns], .opal-archive-style-4.blog .site-main, .opal-archive-style-4.archive .site-main, .site-footer .widget-area, .comment-form, .widget .gallery, .elementor-element .gallery, .entry-gallery .gallery, .single .gallery, [data-elementor-columns], .single-portfolio-summary .single-portfolio-summary-inner, .opal-canvas-filter.top .opal-canvas-filter-wrap, .opal-canvas-filter.top .opal-canvas-filter-wrap section.WOOF_Widget .woof_redraw_zone, .woocommerce-cart .woocommerce, .woocommerce-billing-fields .woocommerce-billing-fields__field-wrapper, .woocommerce-MyAccount-content form[class^="woocommerce-"], .woocommerce-columns--addresses, form.track_order, .woocommerce-account .entry-content > .woocommerce, .woocommerce-account .entry-content > .woocommerce .u-columns.woocommerce-Addresses, .woocommerce-Addresses, .woocommerce-address-fields__field-wrapper, ul.products, .osf-sorting, .single-product div.product {
            margin-right: -15px;
            margin-left: -15px;
        }

        .col-1, .col-2, [data-elementor-columns-mobile="6"] .column-item, .col-3, [data-elementor-columns-mobile="4"] .column-item, .col-4, [data-elementor-columns-mobile="3"] .column-item, .col-5, .col-6, [data-elementor-columns-mobile="2"] .column-item, .single-product.opal-comment-form-2 .comment-form-author, .single-product.opal-comment-form-2 .comment-form-email, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .related-posts .column-item, .opal-default-content-layout-2cr .related-posts .column-item, .opal-content-layout-2cr .related-posts .column-item, .opal-content-layout-2cl .related-posts .column-item, .site-footer .widget-area .widget-column, .comment-form > p, .comment-form > .comment-form-rating, .widget .gallery-columns-1 .gallery-item, .elementor-element .gallery-columns-1 .gallery-item, .entry-gallery .gallery-columns-1 .gallery-item, .single .gallery-columns-1 .gallery-item, [data-elementor-columns-mobile="1"] .column-item, .single-portfolio-summary .single-portfolio-summary-inner .single-portfolio-summary-meta-title, .single-portfolio-summary .single-portfolio-summary-meta, .single-portfolio-summary .single-portfolio-summary-content, .single-portfolio-summary.col-lg-5 .single-portfolio-summary-meta, .single-portfolio-summary.col-lg-5 .single-portfolio-summary-content, .woocommerce-cart .cart-empty, .woocommerce-cart .return-to-shop, .woocommerce-billing-fields .form-row-wide, .woocommerce-MyAccount-content form[class^="woocommerce-"] > *:not(fieldset), .woocommerce-MyAccount-content form[class^="woocommerce-"] .form-row-wide, #customer_details [class*='col'], .woocommerce-Addresses .woocommerce-Address, .columns-1 ul.products li.product, .columns-1 ul.products > li, .woocommerce-tabs, .col, body #secondary, .opal-canvas-filter.top .opal-canvas-filter-wrap section, .opal-canvas-filter.top .opal-canvas-filter-wrap section.WOOF_Widget .woof_redraw_zone .woof_container, form.track_order p.form-row-first, form.track_order p.form-row-last, .columns-5 ul.products li.product, .columns-5 ul.products > li, .col-auto, .col-sm-1, [data-opal-columns="12"] .column-item, .col-sm-2, [data-opal-columns="6"] .column-item, .columns-6 ul.products li.product, .columns-6 ul.products > li, .col-sm-3, [data-opal-columns="4"] .column-item, .col-sm-4, [data-opal-columns="3"] .column-item, .comment-form .comment-form-email, .comment-form .comment-form-url, .comment-form .comment-form-author, .widget .gallery-columns-6 .gallery-item, .elementor-element .gallery-columns-6 .gallery-item, .entry-gallery .gallery-columns-6 .gallery-item, .single .gallery-columns-6 .gallery-item, .col-sm-5, .col-sm-6, [data-opal-columns="2"] .column-item, .opal-archive-style-2 .column-item, .opal-archive-style-5 .column-item, .opal-archive-style-4 .column-item, .opal-archive-style-3 .column-item, .widget .gallery-columns-2 .gallery-item, .elementor-element .gallery-columns-2 .gallery-item, .entry-gallery .gallery-columns-2 .gallery-item, .single .gallery-columns-2 .gallery-item, .widget .gallery-columns-3 .gallery-item, .elementor-element .gallery-columns-3 .gallery-item, .entry-gallery .gallery-columns-3 .gallery-item, .single .gallery-columns-3 .gallery-item, .widget .gallery-columns-4 .gallery-item, .elementor-element .gallery-columns-4 .gallery-item, .entry-gallery .gallery-columns-4 .gallery-item, .single .gallery-columns-4 .gallery-item, .elementor-timeline-view-vertical .timeline-thumbnail, .elementor-timeline-view-vertical .timeline-content, .woocommerce-billing-fields .form-row-first, .woocommerce-billing-fields .form-row-last, .woocommerce-MyAccount-content form[class^="woocommerce-"] .form-row-first, .woocommerce-MyAccount-content form[class^="woocommerce-"] .form-row-last, ul.products li.product, .columns-2 ul.products li.product, .columns-2 ul.products > li, .columns-3 ul.products li.product, .columns-3 ul.products > li, .columns-4 ul.products li.product, .columns-4 ul.products > li, .opal-content-layout-2cl .columns-3 ul.products li.product, .opal-content-layout-2cl .columns-3 ul.products > li, .opal-content-layout-2cr .columns-3 ul.products li.product, .opal-content-layout-2cr .columns-3 ul.products > li, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, [data-opal-columns="1"] .column-item, .opal-archive-style-2.opal-content-layout-2cr .column-item, .opal-archive-style-5.opal-content-layout-2cr .column-item, .opal-archive-style-4.opal-content-layout-2cr .column-item, .opal-archive-style-3.opal-content-layout-2cr .column-item, .elementor-widget-opal-image-hotspots .opal-image-hotspots-accordion, .elementor-widget-opal-image-hotspots .opal-image-hotspots-accordion + .opal-image-hotspots-container, .cart-collaterals .cross-sells, .woocommerce-columns--addresses .woocommerce-column, .woocommerce-account .entry-content > .woocommerce .u-columns [class^="u-column"], .woocommerce-account .woocommerce-ResetPassword, .woocommerce-address-fields__field-wrapper .form-row, .woocommerce-product-carousel ul.products li.product, .osf-sorting .woocommerce-message, .osf-sorting .woocommerce-notice, .opal-content-layout-2cl .osf-sorting .osf-sorting-group, .opal-content-layout-2cr .osf-sorting .osf-sorting-group, .single-product div.product .entry-summary, .single-product div.product .images, .col-sm, .col-sm-auto, .col-md-1, .col-md-2, [data-elementor-columns-tablet="6"] .column-item, .col-md-3, [data-elementor-columns-tablet="4"] .column-item, .col-md-4, [data-elementor-columns-tablet="3"] .column-item, .col-md-5, .osf-sorting .osf-sorting-group, .col-md-6, [data-elementor-columns-tablet="2"] .column-item, .col-md-7, .osf-sorting .osf-sorting-group + .osf-sorting-group, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, [data-elementor-columns-tablet="1"] .column-item, .woocommerce-cart .woocommerce-cart-form, .woocommerce-ResetPassword.lost_reset_password, .woocommerce-account .woocommerce-MyAccount-navigation, .woocommerce-account .woocommerce-MyAccount-content, .col-md, .col-md-auto, .col-lg-1, .col-lg-2, [data-elementor-columns="6"] .column-item, .col-lg-3, [data-elementor-columns="4"] .column-item, .col-lg-4, [data-elementor-columns="3"] .column-item, .col-lg-5, .col-lg-6, [data-elementor-columns="2"] .column-item, .col-lg-7, .col-lg-8, .opal-content-layout-2cl .osf-sorting .osf-sorting-group + .osf-sorting-group, .opal-content-layout-2cr .osf-sorting .osf-sorting-group + .osf-sorting-group, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, [data-elementor-columns="1"] .column-item, .cart-collaterals, .col-lg, .col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl, .col-xl-auto {
            padding-right: 15px;
            padding-left: 15px;
        }

        .container, #content, ul.elementor-nav-menu--dropdown.mega-containerwidth > li.mega-menu-item > .elementor, .opal-canvas-filter.top, .single-product .related.products, .single-product .up-sells.products, .otf-product-recently-content .widget_recently_viewed_products {
            padding-right: 15px;
            padding-left: 15px;
        }

        @media (min-width: 576px) {
            .container, #content, ul.elementor-nav-menu--dropdown.mega-containerwidth > li.mega-menu-item > .elementor, .opal-canvas-filter.top, .single-product .related.products, .single-product .up-sells.products, .otf-product-recently-content .widget_recently_viewed_products {
                max-width: 540px;
            }
        }

        @media (min-width: 768px) {
            .container, #content, ul.elementor-nav-menu--dropdown.mega-containerwidth > li.mega-menu-item > .elementor, .opal-canvas-filter.top, .single-product .related.products, .single-product .up-sells.products, .otf-product-recently-content .widget_recently_viewed_products {
                max-width: 720px;
            }
        }

        @media (min-width: 992px) {
            .container, #content, ul.elementor-nav-menu--dropdown.mega-containerwidth > li.mega-menu-item > .elementor, .opal-canvas-filter.top, .single-product .related.products, .single-product .up-sells.products, .otf-product-recently-content .widget_recently_viewed_products {
                max-width: 960px;
            }
        }

        @media (min-width: 1200px) {
            .container, #content, ul.elementor-nav-menu--dropdown.mega-containerwidth > li.mega-menu-item > .elementor, .opal-canvas-filter.top, .single-product .related.products, .single-product .up-sells.products, .otf-product-recently-content .widget_recently_viewed_products {
                max-width: 1140px;
            }
        }

        .elementor-widget-heading .elementor-heading-title {
            font-family: "Poppins", -apple-system, BlinkMacSystemFont, Sans-serif;
        }

        .elementor-widget-heading .elementor-heading-title, .elementor-text-editor b {
            font-weight: 700;
        }

        .elementor-widget-heading .elementor-heading-title {
            font-family: "Poppins", -apple-system, BlinkMacSystemFont, Sans-serif;
        }

        .elementor-widget-heading .elementor-heading-title, .elementor-text-editor b {
            font-weight: 700;
        }

        .typo-heading, .related-posts .related-heading, .author-wrapper .author-name, .error404 .error-404 .error-title, .error404 .error-404-subtitle h2, .comments-title, .comment-respond .comment-reply-title, h2.widget-title, h2.widgettitle, #secondary .elementor-widget-container h5:first-of-type, .contactform-content .form-title, .mc4wp-form-fields button, form.track_order label, .single-product div.product .pbr-social-share a, .osf-product-deal .woocommerce-product-list .opal-countdown {
            font-family: "Poppins", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
            font-weight: 700;
        }

        @media  screen and (min-width: 1200px) {
            .container, #content, .single-product .related.products, .single-product .up-sells.products, ul.elementor-nav-menu--dropdown.mega-containerwidth > li.mega-menu-item > .elementor {
                max-width: 1366px;
            }
        }

        @media  screen and (min-width: 768px) {
            .container, #content, .container-fluid, .single-product .related.products, .single-product .up-sells.products, ul.elementor-nav-menu--dropdown.mega-containerwidth > li.mega-menu-item > .elementor {
                padding-left: 30px;
                padding-right: 30px;
            }
        }

        .page-title-bar {
            background-image: url(https://demo2.wpopal.com/startor/wp-content/uploads/2019/04/startor_breadcrumb.png);
            background-repeat: no-repeat;
            background-position: center center;;
        }

        .page-title-bar .page-title {
            color: #ffffff;
        }

        .breadcrumb, .breadcrumb span, .breadcrumb * {
            color: #b4bbc2;;
        }

        .breadcrumb a:hover, .breadcrumb a:hover span {
            color: #ffffff;
        }

        .button-primary, input[type="reset"], input.secondary[type="button"], input.secondary[type="reset"], input.secondary[type="submit"], input[type="button"], input[type="submit"], button[type="submit"], .page .edit-link a.post-edit-link, .error404 .go-back, .scrollup, .comment-form .form-submit input[type="submit"], .elementor-wpcf7-button-primary input[type="button"][type="submit"], .elementor-wpcf7-button-primary input[type="submit"], .elementor-wpcf7-button-primary button[type="submit"], .mailchimp-button-primary button, .wc-proceed-to-checkout .button, .woocommerce-cart .return-to-shop a, .wishlist_table .product-add-to-cart a.button, .woocommerce-MyAccount-content .woocommerce-Pagination .woocommerce-button, .widget_shopping_cart .buttons .button, .button-default:hover, .elementor-element .elementor-button-primary .elementor-button {
            background-color: #5ac2f3;
            border-color: #5ac2f3;
            color: #fff;
            border-radius: 5px;
        }

        .button-primary:hover, input:hover[type="reset"], input:hover[type="button"], input:hover[type="submit"], button:hover[type="submit"], .page .edit-link a.post-edit-link:hover, .error404 .go-back:hover, .scrollup:hover, .comment-form .form-submit input:hover[type="submit"], .elementor-wpcf7-button-primary input:hover[type="submit"], .elementor-wpcf7-button-primary button:hover[type="submit"], .mailchimp-button-primary button:hover, .wc-proceed-to-checkout .button:hover, .woocommerce-cart .return-to-shop a:hover, .wishlist_table .product-add-to-cart a.button:hover, .woocommerce-MyAccount-content .woocommerce-Pagination .woocommerce-button:hover, .widget_shopping_cart .buttons .button:hover, .button-primary:active, input:active[type="reset"], input:active[type="button"], input:active[type="submit"], button:active[type="submit"], .page .edit-link a.post-edit-link:active, .error404 .go-back:active, .scrollup:active, .comment-form .form-submit input:active[type="submit"], .elementor-wpcf7-button-primary input:active[type="submit"], .elementor-wpcf7-button-primary button:active[type="submit"], .mailchimp-button-primary button:active, .wc-proceed-to-checkout .button:active, .woocommerce-cart .return-to-shop a:active, .wishlist_table .product-add-to-cart a.button:active, .woocommerce-MyAccount-content .woocommerce-Pagination .woocommerce-button:active, .widget_shopping_cart .buttons .button:active, .button-primary.active, input.active[type="reset"], input.active[type="button"], input.active[type="submit"], button.active[type="submit"], .page .edit-link a.active.post-edit-link, .error404 .active.go-back, .active.scrollup, .comment-form .form-submit input.active[type="submit"], .elementor-wpcf7-button-primary input.active[type="submit"], .elementor-wpcf7-button-primary button.active[type="submit"], .mailchimp-button-primary button.active, .wc-proceed-to-checkout .active.button, .woocommerce-cart .return-to-shop a.active, .wishlist_table .product-add-to-cart a.active.button, .woocommerce-MyAccount-content .woocommerce-Pagination .active.woocommerce-button, .widget_shopping_cart .buttons .active.button, .show > .button-primary.dropdown-toggle, .show > input.dropdown-toggle[type="reset"], .show > input.dropdown-toggle[type="button"], .show > input.dropdown-toggle[type="submit"], .show > button.dropdown-toggle[type="submit"], .page .edit-link .show > a.dropdown-toggle.post-edit-link, .error404 .show > .dropdown-toggle.go-back, .show > .dropdown-toggle.scrollup, .comment-form .form-submit .show > input.dropdown-toggle[type="submit"], .elementor-wpcf7-button-primary .show > input.dropdown-toggle[type="submit"], .elementor-wpcf7-button-primary .show > button.dropdown-toggle[type="submit"], .mailchimp-button-primary .show > button.dropdown-toggle, .wc-proceed-to-checkout .show > .dropdown-toggle.button, .woocommerce-cart .return-to-shop .show > a.dropdown-toggle, .wishlist_table .product-add-to-cart .show > a.dropdown-toggle.button, .woocommerce-MyAccount-content .woocommerce-Pagination .show > .dropdown-toggle.woocommerce-button, .widget_shopping_cart .buttons .show > .dropdown-toggle.button, .elementor-element .elementor-button-primary .elementor-button:hover, .elementor-element .elementor-button-primary .elementor-button:active, .elementor-element .elementor-button-primary .elementor-button:focus {
            background-color: #2ab0ef;
            border-color: #2ab0ef;
            color: #fff;
        }

        .button-primary, input[type="reset"], input.secondary[type="button"], input.secondary[type="reset"], input.secondary[type="submit"], input[type="button"], input[type="submit"], button[type="submit"], .page .edit-link a.post-edit-link, .error404 .go-back, .scrollup, .comment-form .form-submit input[type="submit"], .elementor-wpcf7-button-primary input[type="button"][type="submit"], .elementor-wpcf7-button-primary input[type="submit"], .elementor-wpcf7-button-primary button[type="submit"], .mailchimp-button-primary button, .wc-proceed-to-checkout .button, .woocommerce-cart .return-to-shop a, .wishlist_table .product-add-to-cart a.button, .woocommerce-MyAccount-content .woocommerce-Pagination .woocommerce-button, .widget_shopping_cart .buttons .button, .button-default:hover, .elementor-element .elementor-button-primary .elementor-button {
            background-color: #5ac2f3;
            border-color: #5ac2f3;
            color: #fff;
            border-radius: 5px;
        }

        .button-primary:hover, input:hover[type="reset"], input:hover[type="button"], input:hover[type="submit"], button:hover[type="submit"], .page .edit-link a.post-edit-link:hover, .error404 .go-back:hover, .scrollup:hover, .comment-form .form-submit input:hover[type="submit"], .elementor-wpcf7-button-primary input:hover[type="submit"], .elementor-wpcf7-button-primary button:hover[type="submit"], .mailchimp-button-primary button:hover, .wc-proceed-to-checkout .button:hover, .woocommerce-cart .return-to-shop a:hover, .wishlist_table .product-add-to-cart a.button:hover, .woocommerce-MyAccount-content .woocommerce-Pagination .woocommerce-button:hover, .widget_shopping_cart .buttons .button:hover, .button-primary:active, input:active[type="reset"], input:active[type="button"], input:active[type="submit"], button:active[type="submit"], .page .edit-link a.post-edit-link:active, .error404 .go-back:active, .scrollup:active, .comment-form .form-submit input:active[type="submit"], .elementor-wpcf7-button-primary input:active[type="submit"], .elementor-wpcf7-button-primary button:active[type="submit"], .mailchimp-button-primary button:active, .wc-proceed-to-checkout .button:active, .woocommerce-cart .return-to-shop a:active, .wishlist_table .product-add-to-cart a.button:active, .woocommerce-MyAccount-content .woocommerce-Pagination .woocommerce-button:active, .widget_shopping_cart .buttons .button:active, .button-primary.active, input.active[type="reset"], input.active[type="button"], input.active[type="submit"], button.active[type="submit"], .page .edit-link a.active.post-edit-link, .error404 .active.go-back, .active.scrollup, .comment-form .form-submit input.active[type="submit"], .elementor-wpcf7-button-primary input.active[type="submit"], .elementor-wpcf7-button-primary button.active[type="submit"], .mailchimp-button-primary button.active, .wc-proceed-to-checkout .active.button, .woocommerce-cart .return-to-shop a.active, .wishlist_table .product-add-to-cart a.active.button, .woocommerce-MyAccount-content .woocommerce-Pagination .active.woocommerce-button, .widget_shopping_cart .buttons .active.button, .show > .button-primary.dropdown-toggle, .show > input.dropdown-toggle[type="reset"], .show > input.dropdown-toggle[type="button"], .show > input.dropdown-toggle[type="submit"], .show > button.dropdown-toggle[type="submit"], .page .edit-link .show > a.dropdown-toggle.post-edit-link, .error404 .show > .dropdown-toggle.go-back, .show > .dropdown-toggle.scrollup, .comment-form .form-submit .show > input.dropdown-toggle[type="submit"], .elementor-wpcf7-button-primary .show > input.dropdown-toggle[type="submit"], .elementor-wpcf7-button-primary .show > button.dropdown-toggle[type="submit"], .mailchimp-button-primary .show > button.dropdown-toggle, .wc-proceed-to-checkout .show > .dropdown-toggle.button, .woocommerce-cart .return-to-shop .show > a.dropdown-toggle, .wishlist_table .product-add-to-cart .show > a.dropdown-toggle.button, .woocommerce-MyAccount-content .woocommerce-Pagination .show > .dropdown-toggle.woocommerce-button, .widget_shopping_cart .buttons .show > .dropdown-toggle.button, .elementor-element .elementor-button-primary .elementor-button:hover, .elementor-element .elementor-button-primary .elementor-button:active, .elementor-element .elementor-button-primary .elementor-button:focus {
            background-color: #2ab0ef;
            border-color: #2ab0ef;
            color: #fff;
        }

        .button-secondary, .secondary-button .search-submit, .elementor-wpcf7-button-secondary input[type="button"][type="submit"], .elementor-wpcf7-button-secondary input[type="submit"], .elementor-wpcf7-button-secondary button[type="submit"], .mailchimp-button-secondary button, .elementor-button-secondary button[type="submit"], .elementor-button-secondary input[type="button"], .elementor-button-secondary input[type="submit"], .elementor-element .elementor-button-secondary .elementor-button {
            background-color: #3d1099;
            border-color: #3d1099;
            color: #fff;
            border-radius: 5px;
        }

        .button-secondary:hover, .secondary-button .search-submit:hover, .elementor-wpcf7-button-secondary input:hover[type="submit"], .elementor-wpcf7-button-secondary button:hover[type="submit"], .mailchimp-button-secondary button:hover, .button-secondary:active, .secondary-button .search-submit:active, .elementor-wpcf7-button-secondary input:active[type="submit"], .elementor-wpcf7-button-secondary button:active[type="submit"], .mailchimp-button-secondary button:active, .button-secondary.active, .secondary-button .active.search-submit, .elementor-wpcf7-button-secondary input.active[type="submit"], .elementor-wpcf7-button-secondary button.active[type="submit"], .mailchimp-button-secondary button.active, .show > .button-secondary.dropdown-toggle, .secondary-button .show > .dropdown-toggle.search-submit, .elementor-wpcf7-button-secondary .show > input.dropdown-toggle[type="submit"], .elementor-wpcf7-button-secondary .show > button.dropdown-toggle[type="submit"], .mailchimp-button-secondary .show > button.dropdown-toggle, .elementor-button-secondary button[type="submit"]:hover, .elementor-button-secondary button[type="submit"]:active, .elementor-button-secondary button[type="submit"]:focus, .elementor-button-secondary input[type="button"]:hover, .elementor-button-secondary input[type="button"]:active, .elementor-button-secondary input[type="button"]:focus, .elementor-button-secondary input[type="submit"]:hover, .elementor-button-secondary input[type="submit"]:active, .elementor-button-secondary input[type="submit"]:focus, .elementor-element .elementor-button-secondary .elementor-button:hover, .elementor-element .elementor-button-secondary .elementor-button:active, .elementor-element .elementor-button-secondary .elementor-button:focus {
            background-color: #2b0b6a;
            border-color: #2b0b6a;
            color: #fff;
        }

        button, input[type="submit"], input[type="reset"], input[type="button"], .button, .btn {
        }

        .elementor-button[class*='elementor-size-'] {
            border-radius: 5px;
        }

        @media (max-width: 991px) {
            .opal-header-absolute .site-header {
                background: #212d4b;
            }
        }</style>
    <link rel='stylesheet' id='otf-fonts-css'
          href='https://fonts.googleapis.com/css?family=Poppins%3A400%7CPoppins%3A700&#038;subset=latin-ext%2Clatin-ext'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='google-fonts-1-css'
          href='https://fonts.googleapis.com/css?family=Poppins%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;ver=5.2.4'
          type='text/css' media='all'/>
    <script type='text/javascript'
            src='https://demo2.wpopal.com/startor/wp-includes/js/jquery/jquery.js'></script>
    <script type='text/javascript'
            src='https://demo2.wpopal.com/startor/wp-content/themes/startor/assets/js/libs/html5.js'></script>
    <link rel='https://api.w.org/' href='https://demo2.wpopal.com/startor/wp-json/'/>
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://demo2.wpopal.com/startor/xmlrpc.php?rsd"/>
    <link rel="wlwmanifest" type="application/wlwmanifest+xml"
          href="https://demo2.wpopal.com/startor/wp-includes/wlwmanifest.xml"/>
    <meta name="generator" content="WordPress 5.2.4"/>
    <meta name="generator" content="WooCommerce 3.6.2"/>
    <link rel="canonical" href="<?php echo e(url('/')); ?>"/>
    <link rel='shortlink' href='<?php echo e(url('/')); ?>'/>
    <noscript>
        <style type="text/css">#wptime-plugin-preloader {
                display: none !important;
            }</style>
    </noscript>
    <noscript>
        <style>.woocommerce-product-gallery {
                opacity: 1 !important;
            }</style>
    </noscript>
    <meta name="generator"
          content="Powered by Slider Revolution 5.4.8.3 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface."/>
    <script type="text/javascript">
        function setREVStartSize(e) {
            try {
                e.c = jQuery(e.c);
                var i = jQuery(window).width(), t = 9999, r = 0, n = 0, l = 0, f = 0, s = 0, h = 0;
                if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                    f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                    var u = (e.c.width(), jQuery(window).height());
                    if (void 0 != e.fullScreenOffsetContainer) {
                        var c = e.fullScreenOffsetContainer.split(",");
                        if (c) jQuery.each(c, function (e, i) {
                            u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                        }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                    }
                    f = u
                } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                e.c.closest(".rev_slider_wrapper").css({height: f})
            } catch (d) {
                console.log("Failure at Presize of Slider:" + d)
            }
        };
    </script>
</head>
<body class="home page-template page-template-page-elementor page-template-page-elementor-php page page-id-176 custom-background wp-custom-logo woocommerce-no-js opal-style gecko platform-windows woocommerce-active product-style-1 opal-layout-wide opal-page-title-left-right opal-footer-skin-light startor-front-page opal-header-absolute elementor-default elementor-page elementor-page-176">
<div id="wptime-plugin-preloader"></div>
<div class="opal-wrapper">
    <div id="page" class="site">
        <?php echo $__env->make('partials.menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div id="page-title-bar" class="page-title-bar"></div>
        <div class="site-content-contain">
            <div id="content" class="site-content">
                <div id="primary" class="content-area">
                    <main id="main" class="site-main">
                        <div data-elementor-type="post" data-elementor-id="176" class="elementor elementor-176"
                             data-elementor-settings="[]">
                            <div class="elementor-inner">
                                <div class="elementor-section-wrap">
                                    <div
                                        class="elementor-element elementor-element-afa8ea6 elementor-section-stretched elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                                        data-id="afa8ea6" data-element_type="section"
                                        data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
                                        <div class="elementor-container elementor-column-gap-no">
                                            <div class="elementor-row">
                                                <?php echo $__env->yieldContent('header'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo $__env->yieldContent('content'); ?>
                                    <script type="text/javascript">(function ($) {
                                            "use strict";
                                            var granularParallaxElementorFront = {
                                                init: function () {
                                                    elementorFrontend.hooks.addAction('frontend/element_ready/global', granularParallaxElementorFront.initWidget);
                                                },
                                                initWidget: function ($scope) {
                                                    $('.elementor-element-aa75aa0').jarallax({
                                                        type: 'scroll',
                                                        speed: 1.2,
                                                        keepImg: true,
                                                        imgSize: 'cover',
                                                        imgPosition: '50% 0%',
                                                        noAndroid: false,
                                                        noIos: false
                                                    });
                                                }
                                            };
                                            $(window).on('elementor/frontend/init', granularParallaxElementorFront.init);
                                        }(jQuery));</script>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
            </div>
        </div>
        <footer id="colophon" class="site-footer">
            <div class="wrap">
                <div class="container">
                    <div data-elementor-type="post" data-elementor-id="143" class="elementor elementor-143"
                         data-elementor-settings="[]">
                        <div class="elementor-inner">
                            <div class="elementor-section-wrap">
                                <div
                                    class="elementor-element elementor-element-27c622e elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                                    data-id="27c622e" data-element_type="section"
                                    data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
                                    <div class="elementor-container elementor-column-gap-no">
                                        <div class="elementor-row">
                                            <div
                                                class="elementor-element elementor-element-f764cd2 elementor-column elementor-col-100 elementor-top-column"
                                                data-id="f764cd2" data-element_type="column">
                                                <div class="elementor-column-wrap  elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div
                                                            class="elementor-element elementor-element-e4d8386 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                            data-id="e4d8386" data-element_type="section">
                                                            <div class="elementor-container elementor-column-gap-no">
                                                                <div class="elementor-row">
                                                                    <div
                                                                        class="elementor-element elementor-element-65d0714 elementor-column elementor-col-25 elementor-inner-column"
                                                                        data-id="65d0714" data-element_type="column">
                                                                        <div
                                                                            class="elementor-column-wrap  elementor-element-populated">
                                                                            <div class="elementor-widget-wrap">
                                                                                <div
                                                                                    class="elementor-element elementor-element-6f1d02f elementor-widget elementor-widget-opal-site-logo elementor-widget-image"
                                                                                    data-id="6f1d02f"
                                                                                    data-element_type="widget"
                                                                                    data-widget_type="opal-site-logo.default">
                                                                                    <div
                                                                                        class="elementor-widget-container">
                                                                                        <div class="elementor-image">
                                                                                            <a href="<?php echo e(asset('/')); ?>">
                                                                                                <img
                                                                                                    src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/logo_startor_footer.svg"
                                                                                                    class="attachment-full size-full"
                                                                                                    alt=""/> </a></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        class="elementor-element elementor-element-f5456d1 elementor-column elementor-col-25 elementor-inner-column"
                                                                        data-id="f5456d1" data-element_type="column">
                                                                        <div
                                                                            class="elementor-column-wrap  elementor-element-populated">
                                                                            <div class="elementor-widget-wrap">
                                                                                <div
                                                                                    class="elementor-element elementor-element-cba110f elementor-tablet-align-center elementor-widget elementor-widget-heading"
                                                                                    data-id="cba110f"
                                                                                    data-element_type="widget"
                                                                                    data-widget_type="heading.default">
                                                                                    <div
                                                                                        class="elementor-widget-container">
                                                                                        <h2 class="elementor-heading-title elementor-size-default">
                                                                                            Site Map</h2></div>
                                                                                </div>
                                                                                <div
                                                                                    class="elementor-element elementor-element-7f1dc12 elementor-nav-menu--indicator-none elementor-nav-menu-tablet__align-center elementor-widget elementor-widget-opal-nav-menu"
                                                                                    data-id="7f1dc12"
                                                                                    data-element_type="widget"
                                                                                    data-settings="{&quot;layout&quot;:&quot;vertical&quot;}"
                                                                                    data-widget_type="opal-nav-menu.default">
                                                                                    <div
                                                                                        class="elementor-widget-container">
                                                                                        <nav data-subMenusMinWidth="50"
                                                                                             data-subMenusMaxWidth="500"
                                                                                             class="elementor-nav-menu--main elementor-nav-menu__container elementor-nav-menu--layout-vertical e--pointer-none">
                                                                                            <ul id="menu-1-7f1dc12"
                                                                                                class="elementor-nav-menu sm-vertical">
                                                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-159">
                                                                                                    <a href="<?php echo e(url('/about')); ?>"
                                                                                                       class="elementor-item"><span
                                                                                                            class="menu-title">Our Company</span></a>
                                                                                                </li>
                                                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-160">
                                                                                                    <a href="<?php echo e(url('/contact')); ?>"
                                                                                                       class="elementor-item"><span
                                                                                                            class="menu-title">Contact Us</span></a>
                                                                                                </li>
                                                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-161">
                                                                                                    <a href="<?php echo e(url('/portfolio')); ?>"
                                                                                                       class="elementor-item"><span
                                                                                                            class="menu-title">Our Project</span></a>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </nav>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        class="elementor-element elementor-element-540f4c5 elementor-column elementor-col-25 elementor-inner-column"
                                                                        data-id="540f4c5" data-element_type="column">
                                                                        <div
                                                                            class="elementor-column-wrap  elementor-element-populated">
                                                                            <div class="elementor-widget-wrap">
                                                                                <div
                                                                                    class="elementor-element elementor-element-abf2ded elementor-tablet-align-center elementor-widget elementor-widget-heading"
                                                                                    data-id="abf2ded"
                                                                                    data-element_type="widget"
                                                                                    data-widget_type="heading.default">
                                                                                    <div
                                                                                        class="elementor-widget-container">
                                                                                        <h2 class="elementor-heading-title elementor-size-default">
                                                                                            contact us</h2></div>
                                                                                </div>
                                                                                <div
                                                                                    class="elementor-element elementor-element-ce7d410 elementor-widget elementor-widget-text-editor"
                                                                                    data-id="ce7d410"
                                                                                    data-element_type="widget"
                                                                                    data-widget_type="text-editor.default">
                                                                                    <div
                                                                                        class="elementor-widget-container">
                                                                                        <div
                                                                                            class="elementor-text-editor elementor-clearfix">
                                                                                            <p>Build a campaign of
                                                                                                kindness and <br/>see
                                                                                                how far one good can go.
                                                                                            </p></div>
                                                                                    </div>
                                                                                </div>
                                                                                <div
                                                                                    class="elementor-element elementor-element-ec00a8c elementor-widget elementor-widget-text-editor"
                                                                                    data-id="ec00a8c"
                                                                                    data-element_type="widget"
                                                                                    data-widget_type="text-editor.default">
                                                                                    <div
                                                                                        class="elementor-widget-container">
                                                                                        <div
                                                                                            class="elementor-text-editor elementor-clearfix">
                                                                                            <p>+84 (0) 1800 333 555</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div
                                                                                    class="elementor-element elementor-element-40db98c elementor-widget elementor-widget-text-editor"
                                                                                    data-id="40db98c"
                                                                                    data-element_type="widget"
                                                                                    data-widget_type="text-editor.default">
                                                                                    <div
                                                                                        class="elementor-widget-container">
                                                                                        <div
                                                                                            class="elementor-text-editor elementor-clearfix">
                                                                                            <p>
                                                                                                <a href="mailto:contact@example.com">info@motawer.co</a>
                                                                                            </p></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        class="elementor-element elementor-element-8ae5dc1 elementor-column elementor-col-25 elementor-inner-column"
                                                                        data-id="8ae5dc1" data-element_type="column">
                                                                        <div
                                                                            class="elementor-column-wrap  elementor-element-populated">
                                                                            <div class="elementor-widget-wrap">
                                                                                <div
                                                                                    class="elementor-element elementor-element-0e534db elementor-tablet-align-center elementor-widget elementor-widget-heading"
                                                                                    data-id="0e534db"
                                                                                    data-element_type="widget"
                                                                                    data-widget_type="heading.default">
                                                                                    <div
                                                                                        class="elementor-widget-container">
                                                                                        <h2 class="elementor-heading-title elementor-size-default">
                                                                                            location</h2></div>
                                                                                </div>
                                                                                <div
                                                                                    class="elementor-element elementor-element-4c6badf elementor-widget elementor-widget-text-editor"
                                                                                    data-id="4c6badf"
                                                                                    data-element_type="widget"
                                                                                    data-widget_type="text-editor.default">
                                                                                    <div
                                                                                        class="elementor-widget-container">
                                                                                        <div
                                                                                            class="elementor-text-editor elementor-clearfix">
                                                                                            <p>Box 565, <br/>Charlestown,
                                                                                                Nevis, <br/>West Indies,
                                                                                                <br/>Caribbean.</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="elementor-element elementor-element-6554e68 elementor-section-full_width elementor-section-content-middle elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                            data-id="6554e68" data-element_type="section">
                                                            <div class="elementor-container elementor-column-gap-no">
                                                                <div class="elementor-row">
                                                                    <div
                                                                        class="elementor-element elementor-element-9bed973 elementor-column elementor-col-50 elementor-inner-column"
                                                                        data-id="9bed973" data-element_type="column">
                                                                        <div
                                                                            class="elementor-column-wrap  elementor-element-populated">
                                                                            <div class="elementor-widget-wrap">
                                                                                <div
                                                                                    class="elementor-element elementor-element-b3c16c5 elementor-widget elementor-widget-text-editor"
                                                                                    data-id="b3c16c5"
                                                                                    data-element_type="widget"
                                                                                    data-widget_type="text-editor.default">
                                                                                    <div
                                                                                        class="elementor-widget-container">
                                                                                        <div
                                                                                            class="elementor-text-editor elementor-clearfix">
                                                                                            <p>Copyright © 2019. All
                                                                                                Rights Reserved.
                                                                                                Coder79 by <a href="http://motawer.co">Motawer</a>.
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        class="elementor-element elementor-element-88dd530 elementor-column elementor-col-50 elementor-inner-column"
                                                                        data-id="88dd530" data-element_type="column">
                                                                        <div
                                                                            class="elementor-column-wrap  elementor-element-populated">
                                                                            <div class="elementor-widget-wrap">
                                                                                <div
                                                                                    class="elementor-element elementor-element-58b8c91 elementor-shape-circle elementor-widget elementor-widget-social-icons"
                                                                                    data-id="58b8c91"
                                                                                    data-element_type="widget"
                                                                                    data-widget_type="social-icons.default">
                                                                                    <div
                                                                                        class="elementor-widget-container">
                                                                                        <div
                                                                                            class="elementor-social-icons-wrapper">
                                                                                            <a class="elementor-icon elementor-social-icon elementor-social-icon-facebook"
                                                                                               href="" target="_blank">
                                                                                                <span
                                                                                                    class="elementor-screen-only">Facebook</span>
                                                                                                <i class="fa fa-facebook"></i>
                                                                                            </a> <a
                                                                                                class="elementor-icon elementor-social-icon elementor-social-icon-linkedin"
                                                                                                href="" target="_blank">
                                                                                                <span
                                                                                                    class="elementor-screen-only">Linkedin</span>
                                                                                                <i class="fa fa-linkedin"></i>
                                                                                            </a> <a
                                                                                                class="elementor-icon elementor-social-icon elementor-social-icon-twitter"
                                                                                                href="" target="_blank">
                                                                                                <span
                                                                                                    class="elementor-screen-only">Twitter</span>
                                                                                                <i class="fa fa-twitter"></i>
                                                                                            </a> <a
                                                                                                class="elementor-icon elementor-social-icon elementor-social-icon-youtube"
                                                                                                href="" target="_blank">
                                                                                                <span
                                                                                                    class="elementor-screen-only">Youtube</span>
                                                                                                <i class="fa fa-youtube"></i>
                                                                                            </a></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
<a href="#" class="scrollup"><span class="icon fa fa-angle-up"></span></a>
<script type="text/javascript">var c = document.body.className;
    c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
    document.body.className = c;</script>
<script type="text/javascript">function revslider_showDoubleJqueryError(sliderID) {
        var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
        errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
        errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
        errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
        errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
        jQuery(sliderID).show().html(errorMessage);
    }</script>
<script
    type='text/javascript'>var osfAjax = {"ajaxurl": "https:\/\/demo2.wpopal.com\/startor\/wp-admin\/admin-ajax.php"};
    var poemeJS = {"quote": "<i class=\"fa-quote-right\"><\/i>", "smoothCallback": ""};</script>
<script type='text/javascript'>
    var elementorFrontendConfig = {
        "environmentMode": {"edit": false, "wpPreview": false},
        "is_rtl": false,
        "breakpoints": {"xs": 0, "sm": 480, "md": 768, "lg": 1025, "xl": 1440, "xxl": 1600},
        "version": "2.5.14",
        "urls": {"assets": "https:\/\/demo2.wpopal.com\/startor\/wp-content\/plugins\/elementor\/assets\/"},
        "settings": {
            "page": [],
            "general": {"elementor_global_image_lightbox": "yes", "elementor_enable_lightbox_in_editor": "yes"}
        },
        "post": {"id": 176, "title": "Home 1", "excerpt": ""}
    };</script>
<script type="text/javascript" defer
        src="<?php echo e(asset('dist/wp-content/cache/autoptimize/js/autoptimize_8e92464942d1e473bea9253f3fb84d34.js')); ?>"></script>
</body>
</html>
