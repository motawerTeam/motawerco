<div
    class="elementor-element elementor-element-063b714 elementor-column elementor-col-100 elementor-top-column"
    data-id="063b714" data-element_type="column">
    <div class="elementor-column-wrap  elementor-element-populated">
        <div class="elementor-widget-wrap">
            <div
                class="elementor-element elementor-element-5c3ce3e elementor-widget elementor-widget-opal-revslider"
                data-id="5c3ce3e" data-element_type="widget"
                data-widget_type="opal-revslider.default">
                <div class="elementor-widget-container">
                    <link
                        href="https://fonts.googleapis.com/css?family=Open+Sans:400%7CPoppins:700%2C400%2C500"
                        rel="stylesheet" property="stylesheet"
                        type="text/css" media="all">
                    <div id="rev_slider_1_1_wrapper"
                         class="rev_slider_wrapper fullwidthbanner-container"
                         data-source="gallery"
                         style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
                        <div id="rev_slider_1_1"
                             class="rev_slider fullwidthabanner"
                             style="display:none;"
                             data-version="5.4.8.3">
                            <ul>
                                <li data-index="rs-1"
                                    data-transition="fade"
                                    data-slotamount="default"
                                    data-hideafterloop="0"
                                    data-hideslideonmobile="off"
                                    data-easein="default"
                                    data-easeout="default"
                                    data-masterspeed="300" data-thumb=""
                                    data-rotate="0"
                                    data-saveperformance="off"
                                    data-title="Slide" data-param1=""
                                    data-param2="" data-param3=""
                                    data-param4="" data-param5=""
                                    data-param6="" data-param7=""
                                    data-param8="" data-param9=""
                                    data-param10="" data-description="">
                                    <img
                                        src="https://demo2.wpopal.com/startor/wp-content/plugins/revslider/admin/assets/images/transparent.png"
                                        alt="" title="rev_slidehome1_14"
                                        width="1964" height="1078"
                                        data-bgposition="center top"
                                        data-bgfit="103% 100%"
                                        data-bgrepeat="no-repeat"
                                        data-bgparallax="off"
                                        class="rev-slidebg"
                                        data-no-retina>
                                    <div
                                        class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                                        id="slide-1-layer-19"
                                        data-x="['center','center','center','center']"
                                        data-hoffset="['0','0','0','0']"
                                        data-y="['top','top','top','top']"
                                        data-voffset="['0','0','0','0']"
                                        data-width="full"
                                        data-height="full"
                                        data-whitespace="nowrap"
                                        data-visibility="['off','on','on','on']"
                                        data-type="shape"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','inherit']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 5;background-color:rgb(61,16,153);"></div>
                                    <div class="tp-caption  "
                                         id="slide-1-layer-18"
                                         data-x="['center','center','center','center']"
                                         data-hoffset="['0','0','0','0']"
                                         data-y="['top','top','top','top']"
                                         data-voffset="['0','0','0','0']"
                                         data-width="100%"
                                         data-height="100%"
                                         data-whitespace="nowrap"
                                         data-visibility="['on','off','off','off']"
                                         data-type="text"
                                         data-basealign="slide"
                                         data-responsive_offset="off"
                                         data-responsive="off"
                                         data-frames='[{"delay":10,"speed":300,"frame":"0","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","ease":"Power3.easeInOut"}]'
                                         data-textAlign="['inherit','inherit','inherit','inherit']"
                                         data-paddingtop="[0,0,0,0]"
                                         data-paddingright="[0,0,0,0]"
                                         data-paddingbottom="[0,0,0,0]"
                                         data-paddingleft="[0,0,0,0]"
                                         style="z-index: 6; min-width: 100%px; max-width: 100%px; max-width: 100%px; max-width: 100%px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Open Sans;">
                                        <svg version="1.1" id="Layer_1"
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                             x="0px" y="0px"
                                             viewBox="0 0 1920 1082"
                                             enable-background="new 0 0 1920 1082"
                                             xml:space="preserve"> <path
                                                class="f-secondary"
                                                fill-rule="evenodd"
                                                clip-rule="evenodd"
                                                d="M1920,0H0v972.7l1827.4,109.2c16.5,1.1,30.7-11.3,31.9-27.9l60.7-662.3V0z"/> </svg>
                                    </div>
                                    <div
                                        class="tp-caption   tp-resizeme"
                                        id="slide-1-layer-3"
                                        data-x="['left','left','left','center']"
                                        data-hoffset="['54','83','54','0']"
                                        data-y="['top','top','top','top']"
                                        data-voffset="['308','167','100','65']"
                                        data-fontsize="['60','60','42','32']"
                                        data-lineheight="['73','73','56','38']"
                                        data-width="['473','473','473','418']"
                                        data-height="none"
                                        data-whitespace="normal"
                                        data-type="text"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":300,"split":"words","splitdelay":0.05,"speed":1000,"split_direction":"forward","frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","split":"words","splitdelay":0.1,"speed":300,"split_direction":"forward","frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 7; min-width: 473px; max-width: 473px; white-space: normal; font-size: 60px; line-height: 73px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Poppins;">
                                        Build a Culture of Kindness
                                    </div>
                                    <div
                                        class="tp-caption   tp-resizeme"
                                        id="slide-1-layer-5"
                                        data-x="['left','left','left','center']"
                                        data-hoffset="['54','83','54','2']"
                                        data-y="['top','top','top','top']"
                                        data-voffset="['478','337','225','154']"
                                        data-fontsize="['18','18','16','14']"
                                        data-width="['none','none','none','386']"
                                        data-height="none"
                                        data-whitespace="['nowrap','nowrap','nowrap','normal']"
                                        data-type="text"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":630,"split":"words","splitdelay":0.05,"speed":1000,"split_direction":"forward","frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","split":"words","splitdelay":0.1,"speed":300,"split_direction":"middletoedge","frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 8; white-space: nowrap; font-size: 18px; line-height: 24px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Poppins;">
                                        See just how far one good deed
                                        can go, with<strong>
                                            Startor</strong></div>
                                    <div
                                        class="tp-caption   tp-resizeme"
                                        id="slide-1-layer-8"
                                        data-x="['right','right','right','right']"
                                        data-hoffset="['56','21','21','21']"
                                        data-y="['top','top','top','top']"
                                        data-voffset="['176','261','261','266']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="normal"
                                        data-type="image"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":1160,"speed":1500,"frame":"0","from":"x:[175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[-100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','inherit']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 9;"><img
                                            src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/rev_slidehome1_5.png"
                                            alt=""
                                            data-ww="['642px','642px','588px','386px']"
                                            data-hh="['631px','631px','578px','379px']"
                                            width="642" height="631"
                                            data-no-retina></div>
                                    <div
                                        class="tp-caption   tp-resizeme"
                                        id="slide-1-layer-7"
                                        data-x="['left','left','left','left']"
                                        data-hoffset="['556','147','8','14']"
                                        data-y="['top','top','top','top']"
                                        data-voffset="['491','543','555','474']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="normal"
                                        data-type="image"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":1550,"speed":890,"frame":"0","from":"x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','inherit']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 10;"><img
                                            src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/rev_slidehome1_2.png"
                                            alt=""
                                            data-ww="['179px','179px','153px','111px']"
                                            data-hh="['301px','301px','257px','187px']"
                                            width="179" height="301"
                                            data-no-retina></div>
                                    <div
                                        class="tp-caption   tp-resizeme"
                                        id="slide-1-layer-9"
                                        data-x="['left','left','left','left']"
                                        data-hoffset="['689','331','158','68']"
                                        data-y="['top','top','top','top']"
                                        data-voffset="['332','398','402','289']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="normal"
                                        data-type="image"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":1700,"speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','inherit']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 11;"><img
                                            src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/rev_slidehome1_4.png"
                                            alt=""
                                            data-ww="['80px','80px','68px','65px']"
                                            data-hh="['266px','266px','227px','215px']"
                                            width="80" height="266"
                                            data-no-retina></div>
                                    <div
                                        class="tp-caption   tp-resizeme"
                                        id="slide-1-layer-10"
                                        data-x="['left','left','left','left']"
                                        data-hoffset="['765','416','237','134']"
                                        data-y="['top','top','top','top']"
                                        data-voffset="['468','542','540','425']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="normal"
                                        data-type="image"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":1830,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','inherit']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 12;"><img
                                            src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/rev_slidehome1_3.png"
                                            alt=""
                                            data-ww="['79px','79px','54px','43px']"
                                            data-hh="['60px','60px','41px','33px']"
                                            width="79" height="60"
                                            data-no-retina></div>
                                    <div
                                        class="tp-caption   tp-resizeme"
                                        id="slide-1-layer-11"
                                        data-x="['left','left','left','left']"
                                        data-hoffset="['919','554','366','201']"
                                        data-y="['top','top','top','top']"
                                        data-voffset="['235','270','277','273']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="normal"
                                        data-type="image"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":2080,"speed":1040,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','inherit']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 13;"><img
                                            src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/rev_slidehome1_6.png"
                                            alt=""
                                            data-ww="['75px','75px','67px','47px']"
                                            data-hh="['188px','188px','169px','119px']"
                                            width="75" height="188"
                                            data-no-retina></div>
                                    <div
                                        class="tp-caption   tp-resizeme rs-parallaxlevel-1"
                                        id="slide-1-layer-12"
                                        data-x="['left','left','left','left']"
                                        data-hoffset="['865','770','338','198']"
                                        data-y="['top','top','top','top']"
                                        data-voffset="['385','390','483','623']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="normal"
                                        data-type="image"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":2360,"speed":1040,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','inherit']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 14;">
                                        <div class="rs-looped rs-wave"
                                             data-speed="2"
                                             data-angle="20"
                                             data-radius="5px"
                                             data-origin="50% 50%"><img
                                                src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/rev_slidehome1_7.png"
                                                alt=""
                                                data-ww="['11px','11px','11px','11px']"
                                                data-hh="['11px','11px','11px','11px']"
                                                width="11" height="11"
                                                data-no-retina></div>
                                    </div>
                                    <div
                                        class="tp-caption   tp-resizeme rs-parallaxlevel-1"
                                        id="slide-1-layer-13"
                                        data-x="['left','left','left','left']"
                                        data-hoffset="['1306','898','670','403']"
                                        data-y="['top','top','top','top']"
                                        data-voffset="['239','219','250','231']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="normal"
                                        data-type="image"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":2620,"speed":770,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','inherit']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 15;">
                                        <div class="rs-looped rs-wave"
                                             data-speed="2"
                                             data-angle="0"
                                             data-radius="5px"
                                             data-origin="50% 50%"><img
                                                src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/rev_slidehome1_8.png"
                                                alt=""
                                                data-ww="['39px','39px','34px','23px']"
                                                data-hh="['39px','39px','34px','23px']"
                                                width="39" height="39"
                                                data-no-retina></div>
                                    </div>
                                    <div
                                        class="tp-caption   tp-resizeme rs-parallaxlevel-1"
                                        id="slide-1-layer-14"
                                        data-x="['left','left','left','left']"
                                        data-hoffset="['1368','901','656','656']"
                                        data-y="['top','top','top','top']"
                                        data-voffset="['702','851','830','830']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="normal"
                                        data-type="image"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":2850,"speed":850,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','inherit']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 16;">
                                        <div
                                            class="rs-looped rs-slideloop"
                                            data-easing=""
                                            data-speed="2" data-xs="0"
                                            data-xe="0" data-ys="-5"
                                            data-ye="10"><img
                                                src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/rev_slidehome1_9.png"
                                                alt=""
                                                data-ww="['21px','21px','21px','21px']"
                                                data-hh="['21px','21px','21px','21px']"
                                                width="21" height="21"
                                                data-no-retina></div>
                                    </div>
                                    <div
                                        class="tp-caption   tp-resizeme rs-parallaxlevel-1"
                                        id="slide-1-layer-15"
                                        data-x="['left','left','left','left']"
                                        data-hoffset="['789','433','233','233']"
                                        data-y="['top','top','top','top']"
                                        data-voffset="['783','876','823','823']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="normal"
                                        data-type="image"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":3100,"speed":770,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','inherit']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 17;">
                                        <div class="rs-looped rs-rotate"
                                             data-easing="Power0.easeInOut"
                                             data-startdeg="0"
                                             data-enddeg="360"
                                             data-speed="4"
                                             data-origin="50% 50%"><img
                                                src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/rev_slidehome1_10.png"
                                                alt=""
                                                data-ww="['34px','34px','34px','34px']"
                                                data-hh="['34px','34px','34px','34px']"
                                                width="34" height="34"
                                                data-no-retina></div>
                                    </div>
                                    <div
                                        class="tp-caption   tp-resizeme rs-parallaxlevel-1"
                                        id="slide-1-layer-16"
                                        data-x="['left','left','left','left']"
                                        data-hoffset="['1453','951','704','704']"
                                        data-y="['top','top','top','top']"
                                        data-voffset="['816','827','808','808']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="normal"
                                        data-type="image"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":3370,"speed":830,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','inherit']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 18;">
                                        <div class="rs-looped rs-wave"
                                             data-speed="2"
                                             data-angle="0"
                                             data-radius="5px"
                                             data-origin="50% 50%"><img
                                                src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/rev_slidehome1_11.png"
                                                alt=""
                                                data-ww="['16px','16px','16px','16px']"
                                                data-hh="['16px','16px','16px','16px']"
                                                width="16" height="16"
                                                data-no-retina></div>
                                    </div>
                                    <div
                                        class="tp-caption   tp-resizeme"
                                        id="slide-1-layer-21"
                                        data-x="['center','center','center','center']"
                                        data-hoffset="['0','0','0','0']"
                                        data-y="['bottom','bottom','bottom','bottom']"
                                        data-voffset="['0','0','0','0']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"
                                        data-type="image"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','inherit']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 19;"><img
                                            src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/04/rev_slidehome1_121.png"
                                            alt=""
                                            data-ww="['1946px','1946px','1946px','1946px']"
                                            data-hh="['692px','692px','692px','692px']"
                                            width="1946" height="692"
                                            data-no-retina></div>
                                    <div class="tp-caption rev-btn "
                                         id="slide-1-layer-6"
                                         data-x="['left','left','left','center']"
                                         data-hoffset="['57','86','57','0']"
                                         data-y="['top','top','top','top']"
                                         data-voffset="['545','404','292','197']"
                                         data-fontsize="['14','14','14','12']"
                                         data-lineheight="['52','52','44','38']"
                                         data-width="none"
                                         data-height="none"
                                         data-whitespace="nowrap"
                                         data-type="button"
                                         data-responsive_offset="on"
                                         data-responsive="off"
                                         data-frames='[{"delay":1340,"speed":690,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgb(255,255,255);"}]'
                                         data-textAlign="['inherit','inherit','inherit','inherit']"
                                         data-paddingtop="[0,0,0,0]"
                                         data-paddingright="[50,50,50,25]"
                                         data-paddingbottom="[0,0,0,0]"
                                         data-paddingleft="[50,50,50,25]"
                                         style="z-index: 20; white-space: nowrap; font-size: 14px; line-height: 52px; font-weight: 500; color: #ffffff; letter-spacing: px;font-family:Poppins;background-color:rgb(90,194,243);border-radius:8px 8px 8px 8px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                                        Take A Tour
                                    </div>
                                </li>
                                <li data-index="rs-6"
                                    data-transition="fade"
                                    data-slotamount="default"
                                    data-hideafterloop="0"
                                    data-hideslideonmobile="off"
                                    data-easein="default"
                                    data-easeout="default"
                                    data-masterspeed="300" data-thumb=""
                                    data-rotate="0"
                                    data-saveperformance="off"
                                    data-title="Slide" data-param1=""
                                    data-param2="" data-param3=""
                                    data-param4="" data-param5=""
                                    data-param6="" data-param7=""
                                    data-param8="" data-param9=""
                                    data-param10="" data-description="">
                                    <img
                                        src="https://demo2.wpopal.com/startor/wp-content/plugins/revslider/admin/assets/images/transparent.png"
                                        alt="" title="rev_slidehome1_14"
                                        width="1964" height="1078"
                                        data-bgposition="center top"
                                        data-bgfit="103% 100%"
                                        data-bgrepeat="no-repeat"
                                        data-bgparallax="off"
                                        class="rev-slidebg"
                                        data-no-retina>
                                    <div
                                        class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                                        id="slide-6-layer-28"
                                        data-x="['center','center','center','center']"
                                        data-hoffset="['0','0','0','0']"
                                        data-y="['top','top','top','top']"
                                        data-voffset="['0','0','0','0']"
                                        data-width="full"
                                        data-height="full"
                                        data-whitespace="nowrap"
                                        data-visibility="['off','on','on','on']"
                                        data-type="shape"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','inherit']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 5;background-color:rgb(61,16,153);"></div>
                                    <div class="tp-caption  "
                                         id="slide-6-layer-27"
                                         data-x="['center','center','center','center']"
                                         data-hoffset="['0','0','0','0']"
                                         data-y="['top','top','top','top']"
                                         data-voffset="['0','0','0','0']"
                                         data-width="100%"
                                         data-height="100%"
                                         data-whitespace="nowrap"
                                         data-visibility="['on','off','off','off']"
                                         data-type="text"
                                         data-basealign="slide"
                                         data-responsive_offset="off"
                                         data-responsive="off"
                                         data-frames='[{"delay":10,"speed":460,"frame":"0","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","ease":"Power3.easeInOut"}]'
                                         data-textAlign="['inherit','inherit','inherit','inherit']"
                                         data-paddingtop="[0,0,0,0]"
                                         data-paddingright="[0,0,0,0]"
                                         data-paddingbottom="[0,0,0,0]"
                                         data-paddingleft="[0,0,0,0]"
                                         style="z-index: 6; min-width: 100%px; max-width: 100%px; max-width: 100%px; max-width: 100%px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Open Sans;">
                                        <svg version="1.1" id="Layer_1"
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                             x="0px" y="0px"
                                             viewBox="0 0 1920 1082"
                                             enable-background="new 0 0 1920 1082"
                                             xml:space="preserve"> <path
                                                class="f-secondary"
                                                fill-rule="evenodd"
                                                clip-rule="evenodd"
                                                d="M1920,0H0v972.7l1827.4,109.2c16.5,1.1,30.7-11.3,31.9-27.9l60.7-662.3V0z"/> </svg>
                                    </div>
                                    <div
                                        class="tp-caption   tp-resizeme"
                                        id="slide-6-layer-31"
                                        data-x="['center','center','center','center']"
                                        data-hoffset="['0','0','0','0']"
                                        data-y="['bottom','bottom','bottom','bottom']"
                                        data-voffset="['0','0','0','0']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"
                                        data-type="image"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":210,"speed":720,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','inherit']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 7;"><img
                                            src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/04/rev_slidehome1_121.png"
                                            alt=""
                                            data-ww="['1946px','1946px','1946px','1946px']"
                                            data-hh="['692px','692px','692px','692px']"
                                            width="1946" height="692"
                                            data-no-retina></div>
                                    <div
                                        class="tp-caption   tp-resizeme"
                                        id="slide-6-layer-3"
                                        data-x="['left','left','left','center']"
                                        data-hoffset="['54','83','54','-1']"
                                        data-y="['top','top','top','top']"
                                        data-voffset="['308','167','100','78']"
                                        data-fontsize="['60','54','42','38']"
                                        data-lineheight="['73','67','56','46']"
                                        data-width="['585','563','473','400']"
                                        data-height="['none','135','none','115']"
                                        data-whitespace="normal"
                                        data-type="text"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":300,"split":"words","splitdelay":0.05,"speed":1000,"split_direction":"forward","frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","split":"words","splitdelay":0.1,"speed":300,"split_direction":"forward","frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 8; min-width: 585px; max-width: 585px; white-space: normal; font-size: 60px; line-height: 73px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Poppins;">
                                        Fast, reliable & efficient
                                        solutions
                                    </div>
                                    <div
                                        class="tp-caption   tp-resizeme"
                                        id="slide-6-layer-5"
                                        data-x="['left','left','left','center']"
                                        data-hoffset="['54','83','54','0']"
                                        data-y="['top','top','top','top']"
                                        data-voffset="['478','324','225','192']"
                                        data-fontsize="['18','18','16','14']"
                                        data-width="['none','none','none','371']"
                                        data-height="none"
                                        data-whitespace="['nowrap','nowrap','nowrap','normal']"
                                        data-type="text"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":630,"split":"words","splitdelay":0.05,"speed":1000,"split_direction":"forward","frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","split":"words","splitdelay":0.1,"speed":300,"split_direction":"middletoedge","frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 9; white-space: nowrap; font-size: 18px; line-height: 24px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Poppins;">
                                        See just how far one good deed
                                        can go, with<strong>
                                            Startor</strong></div>
                                    <div class="tp-caption rev-btn "
                                         id="slide-6-layer-6"
                                         data-x="['left','left','left','center']"
                                         data-hoffset="['57','86','57','0']"
                                         data-y="['top','top','top','top']"
                                         data-voffset="['545','389','292','236']"
                                         data-fontsize="['14','14','14','12']"
                                         data-lineheight="['52','52','44','38']"
                                         data-width="none"
                                         data-height="none"
                                         data-whitespace="nowrap"
                                         data-type="button"
                                         data-responsive_offset="on"
                                         data-responsive="off"
                                         data-frames='[{"delay":1340,"speed":690,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgb(255,255,255);"}]'
                                         data-textAlign="['inherit','inherit','inherit','inherit']"
                                         data-paddingtop="[0,0,0,0]"
                                         data-paddingright="[50,50,50,25]"
                                         data-paddingbottom="[0,0,0,0]"
                                         data-paddingleft="[50,50,50,25]"
                                         style="z-index: 10; white-space: nowrap; font-size: 14px; line-height: 52px; font-weight: 500; color: #ffffff; letter-spacing: px;font-family:Poppins;background-color:rgb(90,194,243);border-radius:8px 8px 8px 8px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                                        Take A Tour
                                    </div>
                                    <div
                                        class="tp-caption   tp-resizeme"
                                        id="slide-6-layer-23"
                                        data-x="['left','left','left','left']"
                                        data-hoffset="['737','317','222','48']"
                                        data-y="['top','top','top','top']"
                                        data-voffset="['338','439','439','381']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"
                                        data-type="image"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":890,"speed":1260,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','inherit']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 11;">
                                        <div
                                            class="rs-looped rs-slideloop"
                                            data-easing=""
                                            data-speed="2" data-xs="5"
                                            data-xe="-20" data-ys="0"
                                            data-ye="0"><img
                                                src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/rev_slidehome1_17.png"
                                                alt=""
                                                data-ww="['196px','196px','146px','128px']"
                                                data-hh="['303px','303px','226px','198px']"
                                                width="196" height="303"
                                                data-no-retina></div>
                                    </div>
                                    <div
                                        class="tp-caption   tp-resizeme"
                                        id="slide-6-layer-22"
                                        data-x="['left','left','left','left']"
                                        data-hoffset="['860','440','313','133']"
                                        data-y="['top','top','top','top']"
                                        data-voffset="['179','280','282','271']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"
                                        data-type="image"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":500,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','inherit']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 12;"><img
                                            src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/rev_slidehome1_18.png"
                                            alt=""
                                            data-ww="['335px','335px','285px','219px']"
                                            data-hh="['572px','572px','487px','374px']"
                                            width="335" height="572"
                                            data-no-retina></div>
                                    <div
                                        class="tp-caption   tp-resizeme"
                                        id="slide-6-layer-24"
                                        data-x="['left','left','left','left']"
                                        data-hoffset="['1202','782','589','352']"
                                        data-y="['top','top','top','top']"
                                        data-voffset="['411','512','512','449']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"
                                        data-type="image"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":1160,"speed":1210,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','inherit']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 13;">
                                        <div
                                            class="rs-looped rs-slideloop"
                                            data-easing=""
                                            data-speed="2" data-xs="5"
                                            data-xe="-20" data-ys="0"
                                            data-ye="0"><img
                                                src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/rev_slidehome1_16.png"
                                                alt=""
                                                data-ww="['198px','198px','148px','115px']"
                                                data-hh="['255px','255px','191px','148px']"
                                                width="198" height="255"
                                                data-no-retina></div>
                                    </div>
                                    <div
                                        class="tp-caption   tp-resizeme"
                                        id="slide-6-layer-25"
                                        data-x="['left','left','left','left']"
                                        data-hoffset="['511','91','52','5']"
                                        data-y="['top','top','top','top']"
                                        data-voffset="['439','540','540','503']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"
                                        data-type="image"
                                        data-responsive_offset="on"
                                        data-frames='[{"delay":1420,"speed":1270,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','inherit']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="z-index: 14;"><img
                                            src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/rev_slidehome1_15.png"
                                            alt=""
                                            data-ww="['255px','255px','205px','116px']"
                                            data-hh="['279px','279px','224','127px']"
                                            width="255" height="279"
                                            data-no-retina></div>
                                </li>
                            </ul>
                            <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                var htmlDivCss = "";
                                if (htmlDiv) {
                                    htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                } else {
                                    var htmlDiv = document.createElement("div");
                                    htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                    document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                }</script>
                            <div class="tp-bannertimer tp-bottom"
                                 style="visibility: hidden !important;"></div>
                        </div>
                        <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                            var htmlDivCss = "";
                            if (htmlDiv) {
                                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                            } else {
                                var htmlDiv = document.createElement("div");
                                htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                            }</script>
                        <script
                            type="text/javascript">if (setREVStartSize !== undefined) setREVStartSize(
                                {
                                    c: '#rev_slider_1_1',
                                    responsiveLevels: [1240, 1024, 778, 480],
                                    gridwidth: [1400, 1024, 778, 480],
                                    gridheight: [1080, 1000, 960, 720],
                                    sliderLayout: 'fullwidth'
                                });

                            var revapi1,
                                tpj;
                            (function () {
                                if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded", onLoad); else onLoad();

                                function onLoad() {
                                    if (tpj === undefined) {
                                        tpj = jQuery;
                                        if ("off" == "on") tpj.noConflict();
                                    }
                                    if (tpj("#rev_slider_1_1").revolution == undefined) {
                                        revslider_showDoubleJqueryError("#rev_slider_1_1");
                                    } else {
                                        revapi1 = tpj("#rev_slider_1_1").show().revolution({
                                            sliderType: "standard",
                                            jsFileLocation: "//demo2.wpopal.com/startor/wp-content/plugins/revslider/public/assets/js/",
                                            sliderLayout: "fullwidth",
                                            dottedOverlay: "none",
                                            delay: 9000,
                                            navigation: {
                                                keyboardNavigation: "off",
                                                keyboard_direction: "horizontal",
                                                mouseScrollNavigation: "off",
                                                mouseScrollReverse: "default",
                                                onHoverStop: "off",
                                                arrows: {
                                                    style: "uranus",
                                                    enable: true,
                                                    hide_onmobile: true,
                                                    hide_under: 1024,
                                                    hide_onleave: false,
                                                    tmp: '',
                                                    left: {
                                                        h_align: "left",
                                                        v_align: "center",
                                                        h_offset: 20,
                                                        v_offset: 0
                                                    },
                                                    right: {
                                                        h_align: "right",
                                                        v_align: "center",
                                                        h_offset: 20,
                                                        v_offset: 0
                                                    }
                                                }
                                                ,
                                                bullets: {
                                                    enable: true,
                                                    hide_onmobile: false,
                                                    hide_over: 1024,
                                                    style: "hermes",
                                                    hide_onleave: false,
                                                    direction: "vertical",
                                                    h_align: "left",
                                                    v_align: "center",
                                                    h_offset: 25,
                                                    v_offset: 20,
                                                    space: 8,
                                                    tmp: ''
                                                }
                                            },
                                            responsiveLevels: [1240, 1024, 778, 480],
                                            visibilityLevels: [1240, 1024, 778, 480],
                                            gridwidth: [1400, 1024, 778, 480],
                                            gridheight: [1080, 1000, 960, 720],
                                            lazyType: "none",
                                            parallax: {
                                                type: "mouse",
                                                origo: "enterpoint",
                                                speed: 400,
                                                speedbg: 0,
                                                speedls: 0,
                                                levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
                                            },
                                            shadow: 0,
                                            spinner: "spinner0",
                                            stopLoop: "off",
                                            stopAfterLoops: -1,
                                            stopAtSlide: -1,
                                            shuffle: "off",
                                            autoHeight: "off",
                                            disableProgressBar: "on",
                                            hideThumbsOnMobile: "off",
                                            hideSliderAtLimit: 0,
                                            hideCaptionAtLimit: 0,
                                            hideAllCaptionAtLilmit: 0,
                                            debugMode: false,
                                            fallbacks: {
                                                simplifyAll: "off",
                                                nextSlideOnWindowFocus: "off",
                                                disableFocusListener: false,
                                            }
                                        });
                                    }
                                    ; /* END OF revapi call */

                                }; /* END OF ON LOAD FUNCTION */
                            }()); /* END OF WRAPPING FUNCTION */</script>
                        <script>var htmlDivCss = unescape("%23rev_slider_1_1%20.uranus.tparrows%20%7B%0A%20%20width%3A50px%3B%0A%20%20height%3A50px%3B%0A%20%20background%3Argba%28255%2C255%2C255%2C0%29%3B%0A%20%7D%0A%20%23rev_slider_1_1%20.uranus.tparrows%3Abefore%20%7B%0A%20width%3A50px%3B%0A%20height%3A50px%3B%0A%20line-height%3A50px%3B%0A%20font-size%3A40px%3B%0A%20transition%3Aall%200.3s%3B%0A-webkit-transition%3Aall%200.3s%3B%0A%20%7D%0A%20%0A%20%20%23rev_slider_1_1%20.uranus.tparrows%3Ahover%3Abefore%20%7B%0A%20%20%20%20opacity%3A0.75%3B%0A%20%20%7D%0A.hermes.tp-bullets%20%7B%0A%7D%0A%0A.hermes%20.tp-bullet%20%7B%0A%20%20%20%20overflow%3Ahidden%3B%0A%20%20%20%20border-radius%3A50%25%3B%0A%20%20%20%20width%3A16px%3B%0A%20%20%20%20height%3A16px%3B%0A%20%20%20%20background-color%3A%20rgba%280%2C%200%2C%200%2C%200%29%3B%0A%20%20%20%20box-shadow%3A%20inset%200%200%200%202px%20rgb%28255%2C%20255%2C%20255%29%3B%0A%20%20%20%20-webkit-transition%3A%20background%200.3s%20ease%3B%0A%20%20%20%20transition%3A%20background%200.3s%20ease%3B%0A%20%20%20%20position%3Aabsolute%3B%0A%7D%0A%0A.hermes%20.tp-bullet%3Ahover%20%7B%0A%09%20%20background-color%3A%20rgba%280%2C0%2C0%2C0.21%29%3B%0A%7D%0A.hermes%20.tp-bullet%3Aafter%20%7B%0A%20%20content%3A%20%27%20%27%3B%0A%20%20position%3A%20absolute%3B%0A%20%20bottom%3A%200%3B%0A%20%20height%3A%200%3B%0A%20%20left%3A%200%3B%0A%20%20width%3A%20100%25%3B%0A%20%20background-color%3A%20rgb%28255%2C%20255%2C%20255%29%3B%0A%20%20box-shadow%3A%200%200%201px%20rgb%28255%2C%20255%2C%20255%29%3B%0A%20%20-webkit-transition%3A%20height%200.3s%20ease%3B%0A%20%20transition%3A%20height%200.3s%20ease%3B%0A%7D%0A.hermes%20.tp-bullet.selected%3Aafter%20%7B%0A%20%20height%3A100%25%3B%0A%7D%0A%0A");
                            var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                            if (htmlDiv) {
                                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                            } else {
                                var htmlDiv = document.createElement('div');
                                htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                            }</script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
