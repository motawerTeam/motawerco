<header id="masthead" class="site-header">
    <div class="site-header">
        <div data-elementor-type="post" data-elementor-id="599" class="elementor elementor-599"
             data-elementor-settings="[]">
            <div class="elementor-inner">
                <div class="elementor-section-wrap">
                    <div
                        class="elementor-element elementor-element-be15109 elementor-section-stretched elementor-section-content-middle osf-sticky-active elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                        data-id="be15109" data-element_type="section"
                        data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
                        <div class="elementor-container elementor-column-gap-no">
                            <div class="elementor-row">
                                <div
                                    class="elementor-element elementor-element-5a42fc7 elementor-column elementor-col-33 elementor-top-column"
                                    data-id="5a42fc7" data-element_type="column">
                                    <div class="elementor-column-wrap  elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div
                                                class="elementor-element elementor-element-25c0998 elementor-widget elementor-widget-opal-site-logo elementor-widget-image"
                                                data-id="25c0998" data-element_type="widget"
                                                data-widget_type="opal-site-logo.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-image"><a
                                                            href="<?php echo e(url('/')); ?>"> <img
                                                                src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/logo_startor_footer.svg"
                                                                class="attachment-full size-full" alt=""/> </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="elementor-element elementor-element-635f023 elementor-column elementor-col-33 elementor-top-column"
                                    data-id="635f023" data-element_type="column">
                                    <div class="elementor-column-wrap  elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div
                                                class="elementor-element elementor-element-0ecdfc9 elementor-nav-menu--indicator-chevron elementor-nav-menu--dropdown-mobile elementor-menu-toggle-mobile__align-right elementor-nav-menu__align-left elementor-nav-menu-tablet__align-left elementor-nav-menu__text-align-aside elementor-nav-menu--toggle elementor-nav-menu--burger elementor-widget elementor-widget-opal-nav-menu"
                                                data-id="0ecdfc9" data-element_type="widget"
                                                data-settings="{&quot;layout&quot;:&quot;horizontal&quot;,&quot;toggle&quot;:&quot;burger&quot;}"
                                                data-widget_type="opal-nav-menu.default">
                                                <div class="elementor-widget-container">
                                                    <nav
                                                        class="elementor-nav-menu--mobile-enable elementor-nav-menu--main elementor-nav-menu__container elementor-nav-menu--layout-horizontal e--pointer-none"
                                                        data-subMenusMinWidth="260" data-subMenusMaxWidth="500">
                                                        <ul id="menu-1-0ecdfc9" class="elementor-nav-menu">
                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-176 current_page_item current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-365">
                                                                <a href="<?php echo e(url('/')); ?>"
                                                                   class="elementor-item  elementor-item-<?php echo $__env->yieldContent('menu'); ?>"><span
                                                                        class="menu-title">Home</span></a>
                                                            </li>
                                                            <li id="menu-item-367"
                                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-367 has-mega-menu">
                                                                <a href="<?php echo e(url('/portfolio')); ?>" class="elementor-item-<?php echo $__env->yieldContent('menu'); ?>">
                                                                    <span
                                                                        class="menu-title">Our Project</span></a>
                                                            </li>
                                                            <li id="menu-item-368"
                                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-368 has-mega-menu">
                                                                <a href="<?php echo e(url('/about')); ?>"
                                                                   class="elementor-item-<?php echo $__env->yieldContent('menu'); ?>"><span
                                                                        class="menu-title">About</span></a>
                                                            </li>
                                                            <li id="menu-item-1309"
                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1309">
                                                                <a href="<?php echo e(url('/contact')); ?>"
                                                                   class="elementor-item-<?php echo $__env->yieldContent('menu'); ?>"><span
                                                                        class="menu-title">Contact</span></a>
                                                            </li>
                                                        </ul>
                                                    </nav>
                                                    <div class="elementor-menu-toggle"
                                                         data-target="#menu-0ecdfc9"><i class="eicon"
                                                                                        aria-hidden="true"></i>
                                                        <span class="menu-toggle-title"></span></div>
                                                    <nav id="menu-0ecdfc9"
                                                         class="elementor-nav-menu--canvas mp-menu">
                                                        <ul id="menu-3-0ecdfc9" class="nav-menu--canvas">
                                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-368">
                                                                <a href="<?php echo e(url('/')); ?>"
                                                                   aria-current="page">Home</a>
                                                            </li>
                                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-368">
                                                                <a href="<?php echo e(url('/portfolio')); ?>">Our Project</a>
                                                            </li>
                                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-368">
                                                                <a href="<?php echo e(url('/about')); ?>">About</a>
                                                            </li>
                                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-368">
                                                                <a href="<?php echo e(url('/contact')); ?>">Contact</a>
                                                            </li>
                                                        </ul>
                                                    </nav>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
