importScripts('https://www.gstatic.com/firebasejs/4.6.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.6.2/firebase-messaging.js')

var config = {
    apiKey: "AIzaSyBknmbxed23sri_SNpUnSMfIyGslZGiczU",
    authDomain: "gobus-256711.firebaseapp.com",
    databaseURL: "https://gobus-256711.firebaseio.com",
    projectId: "gobus-256711",
    storageBucket: "",
    messagingSenderId: "534901265253"
};
firebase.initializeApp(config);

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload){
    // console.log('onMessage',payload);
    // console.log(payload.notification.body);
    const title = payload.notification.title;
    const options = {
        body : payload.notification.body,
        icon : asset('dist/img/page-header/logo.png'),
        data : payload.notification.click_action
    }

    return self.registration.showNotification(title,options);
})

