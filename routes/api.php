<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Authcontroller@login');
Route::post('google', 'Authcontroller@googleAuth');
Route::post('reset-password', 'Authcontroller@reset');

Route::middleware(
    ['auth:api'])->group(function ()
{
    Route::post('user/token', 'ProfileController@updateUserFireBaseToken');

    Route::middleware('Verify')->group(function () {

        Route::get('logout', 'ProfileController@logout');
        Route::get('profile', 'ProfileController@profile');
    });
});

