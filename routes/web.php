<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/portfolio', function () {
    return view('Portfolio');
});
Route::get('/about', function () {
    return view('About');
});
Route::get('/contact', function () {
    return view('Contact');
});

Auth::routes();



Route::get('in', function (){
    return view('tests.checkin');
});
Route::get('salary', function (){
    return view('tests.salary');
});
//Route::get('in', 'AttendanceSheetController@viewin');

Route::post('checkin', 'AttendanceSheetController@attend')->name('checkin');
