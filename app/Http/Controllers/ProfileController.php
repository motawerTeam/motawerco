<?php

namespace App\Http\Controllers;

use App\UserFirebaseTokens;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function profile(){
        $user = Auth::user();
        $tokenResult = $user->createToken('Personal Access Token');

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'userInfo' => $user,
            'message' => __("general.profile"),
            'success' => true,
        ]);

    }

    public function updateUserFireBaseToken(Request $request){

        \Validator::make(
            $request->all(),
            [
                'firebase_token' => 'required|string',
                'login_type' => 'required',
            ]
        )->validate();
        $user = Auth::user();
        $userToken = $user->firebase_tokens()->where('login_type',$request->login_type)->first();
        if(!$userToken){
            $userToken = new UserFirebaseTokens();
            $userToken->user_id = $user->id;
        }
        $userToken->firebase_token = $request->firebase_token;
        $userToken->login_type = $request->login_type;


        if($userToken->save()){
            return response()->json(['status' => true], 200);
        }else{
            return response()->json(['status' => false], 500);
        }
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out',
            'success' => true
        ]);
    }

    public function logsUser(Request $request){

    }
}
