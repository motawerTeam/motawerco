<?php

namespace App\Http\Controllers;

use App\SchoolLanguages;
use Illuminate\Http\Request;

class SchoolLanguagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SchoolLanguages  $schoolLanguages
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolLanguages $schoolLanguages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SchoolLanguages  $schoolLanguages
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolLanguages $schoolLanguages)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SchoolLanguages  $schoolLanguages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolLanguages $schoolLanguages)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SchoolLanguages  $schoolLanguages
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolLanguages $schoolLanguages)
    {
        //
    }
}
