<?php

namespace App\Http\Controllers;

use App\HoldingArea;
use Illuminate\Http\Request;

class HoldingAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HoldingArea  $holdingArea
     * @return \Illuminate\Http\Response
     */
    public function show(HoldingArea $holdingArea)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HoldingArea  $holdingArea
     * @return \Illuminate\Http\Response
     */
    public function edit(HoldingArea $holdingArea)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HoldingArea  $holdingArea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HoldingArea $holdingArea)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HoldingArea  $holdingArea
     * @return \Illuminate\Http\Response
     */
    public function destroy(HoldingArea $holdingArea)
    {
        //
    }
}
