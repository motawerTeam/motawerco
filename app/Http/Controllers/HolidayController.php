<?php

namespace App\Http\Controllers;

use App\Holiday;
use Illuminate\Http\Request;

class HolidayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Holyday  $holyday
     * @return \Illuminate\Http\Response
     */
    public function show(Holyday $holyday)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Holyday  $holyday
     * @return \Illuminate\Http\Response
     */
    public function edit(Holyday $holyday)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Holyday  $holyday
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Holyday $holyday)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Holyday  $holyday
     * @return \Illuminate\Http\Response
     */
    public function destroy(Holyday $holyday)
    {
        //
    }
}
