<?php

namespace App\Http\Controllers;

use App\User;
use App\UserFirebaseTokens;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Authcontroller extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);

        if (!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized',
                'error' => __("general.loginE"),
                'success' => false,

            ], 401);

        $user = $request->user();

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        $active = User::find($user->id);
        $active->is_verify = 1;
        $active->save();

        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);

        $token->save();

        if ($request->device_token) {
            $oneSignal = User::find($user->id);
            $oneSignal->device_id = $request->device_token;
            $oneSignal->save();
        }
        $this->sendNotificationsToUser('Welcome' . Auth::user()->name . '.', Auth::user()->id, false, "https://motawer.co");

        $user = Auth::user();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'userInfo' => $user,
            'message' => __("general.login"),
            'success' => true,
        ]);
    }

    public function reset(Request $request)
    {
        $input = $request->all();

        $query = $request->email;
        if ($query == null) {
            return response()->json(['error' => 'no email found'])->setStatusCode(400);
        }
        if (!$query && $query == '')
            return response()->json(['error' => 300, 'message' => 'check your email'])->setStatusCode(400);

        $pass_reset = rand(10000, 99999);//(6);

        $regenerate = Hash::make($pass_reset);

        $user = User::select('*')
            ->where('email', '=', $query)
            ->update(['password' => $regenerate, "v_code" => $pass_reset]);

        if (!$user) {
            return response()->json(['error' => 300, 'message' => 'check your Email Address please'])->setStatusCode(400);
        }
        $user_id = User::select('*')->where('email', '=', $query)->get('id');


        $this->sendNotificationsToUser('Your Password Is :' . $pass_reset . '.', $user_id[0]->id, false, "https://psycounselor.com/#/home");

        return response()->json(['result' => 'Successfully Resend', 'success' => true]);
    }

}
