<?php

namespace App\Http\Controllers;

use App\UserFirebaseTokens;
use App\WebNotifications;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Log;

define("GOOGLE_API_KEY", "AAAAfIqZk2U:APA91bFpYg5JhSarDNuKGgXzAYhcZUNHM5j3NAMwA532Mv_QnAFMQHD1CMsJsJje5P6zvy5CRcZqjryRbG8nFSFysBKRGj3PFlGrJSlJx41cTvVRly_JkzMbH67NVRU0qFGscoI1KwAo");

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function sendNotificationsToUser($msg,$user = 0 ,$withAdmin = true , $clickAction = "" ,$msgTitle = ''){
        $url = "https://fcm.googleapis.com/fcm/send";

        $usersToNotify = [];

        if($user != 0){
            $usersToNotify[] = $user;
        }
        // Notify Admins if wanted
        if($withAdmin){
            $usersToNotify = array_merge($usersToNotify);
            //dd($usersToNotify);
        }

        $userTokens = UserFirebaseTokens::whereIn('user_id',$usersToNotify)->pluck('firebase_token')->toArray();

        WebNotifications::create([
            'user_id' => $user,
            'title' => $msgTitle,
            'body' => $msg,

        ]);

        //dd($userTokens);
        if(count($userTokens) > 0) {
            $fields = array(
                'registration_ids' => $userTokens,
                'notification' => [
                    "title" => config('APP_NAME','Motawer'),
                    "body" => $msg,
                    "icon" => asset('dist/img/logo.png'),
                    "click_action" => url($clickAction)
                ]
            );

            $headers = array(
                'Authorization: key=' . GOOGLE_API_KEY,
                'Content-Type: application/json'
            );
            // Open connection
            $ch = curl_init();

            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Execute post
            $result = curl_exec($ch);
            //dd($result);
            if ($result === FALSE) {
                //die('Curl failed: ' . curl_error($ch));
                Log::error('Curl failed: ' . curl_error($ch));
            } else {
                //echo $result;
                Log::info($result);
            }

            // Close connection
            curl_close($ch);
        }
        return true;
    }

}
