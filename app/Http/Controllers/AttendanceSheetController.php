<?php

namespace App\Http\Controllers;

use App\LogUser;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AttendanceSheetController extends Controller
{
    public function attend(Request $request)
    {
        $user = Auth()->user()->id;
        $id = $request->id;

        if ($request->has('in') && $request->in === 'on') {

            $day = LogUser::Create([
                'day' => Carbon::today(),
                'in' => Carbon::now(),
                'user_id' => $user
            ]);

            return response()->json($day->id);

        } else {
            $in = LogUser::find($request->id)->in;
            $day = LogUser::find($request->id)->day;
            $days = Carbon::now()->diffInDays($in);

            if ($days > 0) {
                $day1 = LogUser::find($id);

                $day1->update([
                    'user_id' => $user,
                    'out' => $in->format('Y-m-d 23:59:59'),          // 1919
                ]);

                for ($i = 0; $i <$days-1; $i++) {

                    $day = $day->addDays(1);
                    LogUser::create([
                        'day' => $day,
                        'in' => $day->format('Y-m-d 0:00:00'),
                        'out' => $day->format('Y-m-d 23:59:59'),
                        'user_id' => $user,
                    ]);
                }
                $today = LogUser::create([
                    'day' => Carbon::today(),
                    'in' => $day->format('Y-m-d 0:00:00'),
                    'out' => Carbon::now(),
                    'user_id' => $user,
                ]);

                return response()->json($today->id);
            }
        }
    }
}
