<?php

namespace App\Http\Controllers;

use App\TypeSchool;
use Illuminate\Http\Request;

class TypeSchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TypeSchool  $typeSchool
     * @return \Illuminate\Http\Response
     */
    public function show(TypeSchool $typeSchool)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TypeSchool  $typeSchool
     * @return \Illuminate\Http\Response
     */
    public function edit(TypeSchool $typeSchool)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TypeSchool  $typeSchool
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TypeSchool $typeSchool)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TypeSchool  $typeSchool
     * @return \Illuminate\Http\Response
     */
    public function destroy(TypeSchool $typeSchool)
    {
        //
    }
}
