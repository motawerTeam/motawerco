<?php

namespace App\Http\Controllers;

use App\TotalSalary;
use Illuminate\Http\Request;

class TotalSalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TotalSalary  $totalSalary
     * @return \Illuminate\Http\Response
     */
    public function show(TotalSalary $totalSalary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TotalSalary  $totalSalary
     * @return \Illuminate\Http\Response
     */
    public function edit(TotalSalary $totalSalary)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TotalSalary  $totalSalary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TotalSalary $totalSalary)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TotalSalary  $totalSalary
     * @return \Illuminate\Http\Response
     */
    public function destroy(TotalSalary $totalSalary)
    {
        //
    }
}
