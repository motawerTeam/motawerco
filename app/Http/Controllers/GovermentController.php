<?php

namespace App\Http\Controllers;

use App\Goverment;
use Illuminate\Http\Request;

class GovermentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Goverment  $goverment
     * @return \Illuminate\Http\Response
     */
    public function show(Goverment $goverment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Goverment  $goverment
     * @return \Illuminate\Http\Response
     */
    public function edit(Goverment $goverment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Goverment  $goverment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Goverment $goverment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Goverment  $goverment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Goverment $goverment)
    {
        //
    }
}
