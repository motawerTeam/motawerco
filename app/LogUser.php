<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property int $log_time
 * @property int $absent_time
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 */
class LogUser extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'day',  // date
        'in',   // time
        'out',  // time
        'absent_time',
        'created_at',
        'updated_at'
    ];
    protected $casts =
        [
            'day' => 'Datetime',
            'in' => 'Datetime',
            'out' => 'Datetime',
        ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
