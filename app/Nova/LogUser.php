<?php

namespace App\Nova;

use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Http\Requests\NovaRequest;

class LogUser extends Resource
{
    public static $group = 'الاداره';
    public static $displayInNavigation = false;

    public static function label()
    {
        return 'كشف حضور الموظفين';
    }

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\LogUser';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            DateTime::make('Day', 'day')->withMeta(['extraAttributes' => [
                'readonly' => true
            ]])->format('D , M, Y'),
            DateTime::make('check in', 'in')->withMeta(['extraAttributes' => [
                'readonly' => true
            ]])->format('D , M, Y'),
            DateTime::make('check out', 'out')
                ->withMeta(['extraAttributes' => [
                    'readonly' => true
                ]])->format('H:mm'),
            BelongsTo::make('المستخدم', 'user', 'App\Nova\User')->withMeta(['extraAttributes' => [
                'readonly' => true
            ]])
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
