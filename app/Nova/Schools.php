<?php

namespace App\Nova;

use Acm\NovaGmap\NovaGmap;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Markdown;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Whitecube\NovaFlexibleContent\Flexible;

class Schools extends Resource
{
    public static $group = 'اداره المدارس';

    public static function label() {
        return 'المدارس';
    }

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Schools';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Text::make('اسم المدرسه','name')->rules('required')->sortable(),
            Flexible::make('بينات المدرسه','data')
                ->addLayout('المحتوى', 'wysiwyg', [
                    Text::make('العنوان','address')->rules('required'),
                    Number::make('رقم الهاتف','phone'),
                    Number::make('رقم الموبيل','mobile'),
                    Text::make('الاميل','email'),
                    Text::make('صفحه الفيسبوك','fb'),
                ]),
            Flexible::make('Notes','notes')
                ->addLayout('Content', 'wysiwyg', [
                    Text::make('Title'),
                    Markdown::make('Content'),
                ]),
            NovaGmap::make('Location'),
            BelongsTo::make('نوع لغه المدرسه','schoolLanguage','App\Nova\SchoolLanguage')->sortable(),
            BelongsTo::make('محتوى جنسيه المدرسه','typeSchool','App\Nova\TypeSchool')->sortable(),
            BelongsTo::make('الحكومه التابعه للمدرسه','goverment','App\Nova\Goverment')->sortable(),
            BelongsTo::make('المنطقه التعليميه','holdingArea','App\Nova\HeadArea')->sortable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
