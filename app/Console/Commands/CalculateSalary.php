<?php

namespace App\Console\Commands;

use App\LogUser;
use App\Policies\TotalSalaries;
use App\TotalSalary;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CalculateSalary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'salary:calculate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate Employees Salaries every month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();
        foreach ($users as $user){

            $deduction = 0;
            $bonus = 0;
            $bonusRate =  (5/100);
            $growthRate = (25/100);
            $totalHours = 0;
            $net = $user->salary()->net;
            $monthlyWorkDays = 22;
            $dailyWorkHours = 8;
            $dailyBreakHours = 1;   // TODO : must add break time manually for every day or using fingerprint machine
            $hourPrice = $net/($monthlyWorkDays*($dailyWorkHours+$dailyBreakHours));

            $generalHolidays = Holyday::where('general', 1)
                ->whereBetween('holiday', [
                    Carbon::now()->startOfMonth(),
                    Carbon::now()->endOfMonth()
                ])->count();

            $userHolidays = Holyday::where('general', 0)
                ->whereBetween('holiday', [
                    Carbon::now()->startOfMonth(),
                    Carbon::now()->endOfMonth()
                ])
                ->where('user_id', $user->id)
                ->count();

            $workdays = LogUser::whereBetween('day',[
                Carbon::now()->startOfMonth(),
                Carbon::now()->endOfMonth()
            ])
                ->where('user_id', $user->id)
                ->count();

            $totalDays = $workdays + $generalHolidays + $userHolidays;

            $days = LogUser::whereBetween('day',[
                Carbon::now()->startOfMonth(),
                Carbon::now()->endOfMonth()
            ])
                ->where('user_id', $user->id)
                ->get();

            foreach ($days as $day){
                if (!($day->in === null or $day->out === null)){
                    $minutes = $day->out->diffInMinutes($day->in);
                    $hours = $minutes /60;
                    $totalHours+=$hours;
                }
            }

            $totalHours += (($generalHolidays + $userHolidays) * $dailyWorkHours);
            $totalNetSalary = $hourPrice * $totalHours;
            $growthSalary = $totalNetSalary + ($net * $growthRate);

            if($totalDays >$monthlyWorkDays){
                $bonusDays = $totalDays - $monthlyWorkDays;
                $bonus = $bonusDays * ($net * $bonusRate);
            }

            $totalSalary = $growthSalary + $bonus - $deduction;

            TotalSalary::create([
                'total' =>  $totalSalary,
                'user_id'	=>  $user->id
            ]);
        }
    }
}
