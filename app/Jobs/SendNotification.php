<?php

namespace App\Jobs;

use App\UserFirebaseTokens;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

define("GOOGLE_API_KEY", "AAAAfIqZk2U:APA91bFpYg5JhSarDNuKGgXzAYhcZUNHM5j3NAMwA532Mv_QnAFMQHD1CMsJsJje5P6zvy5CRcZqjryRbG8nFSFysBKRGj3PFlGrJSlJx41cTvVRly_JkzMbH67NVRU0qFGscoI1KwAo");
class SendNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $msg , $user , $withAdmin , $clickAction;

    /**
     * Create a new job instance.
     * @param $msg
     * @param $user
     * @param $withAdmin
     * @param $clickAction
     *
     */
    public function __construct($msg,$user = 0 ,$withAdmin = true , $clickAction = "")
    {
        //
        $this->msg = $msg;
        $this->user = $user;
        $this->withAdmin = $withAdmin;
        $this->clickAction = $clickAction;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $url = "https://fcm.googleapis.com/fcm/send";

        $usersToNotify = [];

        if($this->user != 0){
            $usersToNotify[] = $this->user;
        }
        // Notify Admins if wanted
        if($this->withAdmin){
            $usersToNotify = array_merge($usersToNotify);

        }

        $userTokens = UserFirebaseTokens::whereIn('user_id',$usersToNotify)->pluck('firebase_token')->toArray();

        if(count($userTokens) > 0) {
            $fields = array(
                'registration_ids' => $userTokens,
                'notification' => [
                    "title" => config('APP_NAME','Motawer'),
                    "body" => $this->msg,
                    "icon" => asset('dist/img/logo.png'),
                    "click_action" => $this->clickAction
                ]
            );

            $headers = array(
                'Authorization: key=' . GOOGLE_API_KEY,
                'Content-Type: application/json'
            );
            // Open connection
            $ch = curl_init();

            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Execute post
            $result = curl_exec($ch);
            //dd($result);
            if ($result === FALSE) {
                //die('Curl failed: ' . curl_error($ch));
                Log::error('Curl failed: ' . curl_error($ch));
            } else {
                //echo $result;
                Log::info($result);
            }

            // Close connection
            curl_close($ch);
        }

        Log::info("no tokens");
    }
}
