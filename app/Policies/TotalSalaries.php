<?php

namespace App\Policies;

use App\User;
use App\TotalSalary;
use Illuminate\Auth\Access\HandlesAuthorization;

class TotalSalaries
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the total salary.
     *
     * @param  \App\User  $user
     * @param  \App\TotalSalary  $totalSalary
     * @return mixed
     */
    public function view(User $user, TotalSalary $totalSalary)
    {
        return true;
    }

    /**
     * Determine whether the user can create total salaries.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the total salary.
     *
     * @param  \App\User  $user
     * @param  \App\TotalSalary  $totalSalary
     * @return mixed
     */
    public function update(User $user, TotalSalary $totalSalary)
    {
        return false;

    }

    /**
     * Determine whether the user can delete the total salary.
     *
     * @param  \App\User  $user
     * @param  \App\TotalSalary  $totalSalary
     * @return mixed
     */
    public function delete(User $user, TotalSalary $totalSalary)
    {
        return false;

    }

    /**
     * Determine whether the user can restore the total salary.
     *
     * @param  \App\User  $user
     * @param  \App\TotalSalary  $totalSalary
     * @return mixed
     */
    public function restore(User $user, TotalSalary $totalSalary)
    {
        return true;
    }

    /**
     * Determine whether the user can permanently delete the total salary.
     *
     * @param  \App\User  $user
     * @param  \App\TotalSalary  $totalSalary
     * @return mixed
     */
    public function forceDelete(User $user, TotalSalaries $totalSalary)
    {
        return false;
    }
}
