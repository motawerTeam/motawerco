<?php

namespace App\Policies;

use App\User;
use App\LogUser;
use Illuminate\Auth\Access\HandlesAuthorization;

class Logs
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the log user.
     *
     * @param  \App\User  $user
     * @param  \App\LogUser  $logUser
     * @return mixed
     */
    public function view(User $user, LogUser $logUser)
    {
        return true;
    }

    /**
     * Determine whether the user can create log users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return false;

    }

    /**
     * Determine whether the user can update the log user.
     *
     * @param  \App\User  $user
     * @param  \App\LogUser  $logUser
     * @return mixed
     */
    public function update(User $user, LogUser $logUser)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the log user.
     *
     * @param  \App\User  $user
     * @param  \App\LogUser  $logUser
     * @return mixed
     */
    public function delete(User $user, LogUser $logUser)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the log user.
     *
     * @param  \App\User  $user
     * @param  \App\LogUser  $logUser
     * @return mixed
     */
    public function restore(User $user, LogUser $logUser)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the log user.
     *
     * @param  \App\User  $user
     * @param  \App\LogUser  $logUser
     * @return mixed
     */
    public function forceDelete(User $user, LogUser $logUser)
    {
        return false;
    }
}
