<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    protected $fillable =[
        'holiday','general','description','user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
