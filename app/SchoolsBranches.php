<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $school_id
 * @property string $address
 * @property string $created_at
 * @property string $updated_at
 * @property School $school
 */
class SchoolsBranches extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['school_id', 'data', 'longitude', 'latitudes', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getLocationAttribute()
    {
        return (object)[
            'latitude' => $this->latitudes,
            'longitude' => $this->longitude,
        ];
    }


    /*
    Transform the returned value from the Nova field
    */
    public function setLocationAttribute($value)
    {
        $location_lat = round(object_get($value, 'latitude'), 7);
        $location_lng = round(object_get($value, 'longitude'), 7);
        $this->attributes['latitudes'] = $location_lat;
        $this->attributes['longitude'] = $location_lng;
    }

    public function school()
    {
        return $this->belongsTo('App\Schools');
    }
}
