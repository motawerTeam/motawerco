<?php

namespace App;

use Illuminate\Database\Eloquent\Concerns\HasEvents;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Nova\Actions\Actionable;
use Laravel\Passport\HasApiTokens;
use Silvanite\Brandenburg\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles, HasApiTokens , Actionable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function perm()
    {
        return $this->belongsTo(Permissions::class, 'permissions_id', 'id');
    }
    public function firebase_tokens()
    {
        return $this->hasMany(UserFirebaseTokens::class);
    }
    public function salary()
    {
        return $this->hasOne(Salary::class, 'user_id', 'id');
    }
    public function absent()
    {
        return $this->hasMany(LogUser::class, 'user_id', 'id');
    }
    public function total()
    {
        return $this->hasOne(TotalSalary::class, 'user_id', 'id');
    }

    public function log()
    {
        return $this->hasMany(LogUser::class);
    }

    public function holidays()
    {
        return $this->hasMany(Holyday::class);
    }


}
