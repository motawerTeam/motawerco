<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSchoolAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schools', function (Blueprint $table) {
            $table->unsignedInteger('school_type')->after('name');
            $table->foreign('school_type')->references('id')->on('type_schools')->onDelete('cascade');

            $table->unsignedInteger('goverments_id')->after('name');
            $table->foreign('goverments_id')->references('id')->on('goverments')->onDelete('cascade');

            $table->unsignedInteger('holding_areas_id')->after('name');
            $table->foreign('holding_areas_id')->references('id')->on('holding_areas')->onDelete('cascade');

            $table->unsignedInteger('school_languages_id')->after('name');
            $table->foreign('school_languages_id')->references('id')->on('school_languages')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
