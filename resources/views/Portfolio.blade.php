@extends('layouts.app')
@section('title')
    Portfolio
@endsection
@section('menu')
    active
@endsection
@section('header')
    <style>
        .elementor-599 .elementor-element.elementor-element-be15109 {
            background-color: #212d4b !important;
        }

        .home .page-title-bar {
            display: block !important;
            width: 100% !important;
        }
    </style>
    <div id="page-title-bar" class="page-title-bar">
        <div class="container">
            <div class="wrap w-100 d-flex align-items-center">
                <div class="page-title-bar-inner d-flex flex-column align-items-center w-100">
                    <div class="page-header"><h1 class="page-title typo-heading">Portfolio</h1></div>
                    <div class="breadcrumb"><span property="itemListElement" typeof="ListItem">
                            <a property="item"
                               typeof="WebPage"
                               title="Go to Startor."
                               href="{{url('/')}}"
                               class="home"><span
                                    property="name">Motawer</span></a><meta property="position" content="1"></span> &gt;
                        <span class="post post-page current-item">Portfolio</span></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="site-content-contain">
        <div id="content" class="site-content">
            <div class="wrap">
                <div id="primary" class="content-area">
                    <main id="main" class="site-main" role="main">
                        <div class="row isotope-grid elementor-portfolio-style-default" data-elementor-columns="3"
                             data-elementor-columns-tablet="2" data-elementor-columns-mobile="1">
                            <div class="column-item portfolio-entries">
                                <article id="post-313"
                                         class="portfolio post-313 osf_portfolio type-osf_portfolio status-publish has-post-thumbnail hentry osf_portfolio_cat-creative osf_portfolio_cat-illustration">
                                    <div class="portfolio-inner">
                                        <div class="portfolio-post-thumbnail"><img width="700" height="554"
                                                                                   src="../wp-content/uploads/2019/03/portfolio-5-1-700x554.jpg"
                                                                                   class="attachment-startor-gallery-image size-startor-gallery-image wp-post-image"
                                                                                   alt=""
                                                                                   srcset="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-5-1-700x554.jpg 700w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-5-1-300x238.jpg 300w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-5-1-768x608.jpg 768w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-5-1.jpg 1000w"
                                                                                   sizes="(max-width: 700px) 100vw, 700px"/>
                                        </div>
                                        <div class="portfolio-content">
                                            <div class="portfolio-content-inner">
                                                <div class="entry-category"><a
                                                        href="../category-portfolio/creative/index.html">Creative</a>,
                                                    <a href="../category-portfolio/illustration/index.html">Illustration</a>
                                                </div>
                                                <h2 class="entry-title"><a href="charlie-xi-to-xviii/index.html">Charlie
                                                        Xi To Xviii</a></h2></div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="column-item portfolio-entries">
                                <article id="post-312"
                                         class="portfolio post-312 osf_portfolio type-osf_portfolio status-publish has-post-thumbnail hentry osf_portfolio_cat-illustration">
                                    <div class="portfolio-inner">
                                        <div class="portfolio-post-thumbnail"><img width="672" height="455"
                                                                                   src="../wp-content/uploads/2019/03/portfolio-5-e1553744289320.jpg"
                                                                                   class="attachment-startor-gallery-image size-startor-gallery-image wp-post-image"
                                                                                   alt=""
                                                                                   srcset="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-5-e1553744289320.jpg 672w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-5-e1553744289320-300x203.jpg 300w"
                                                                                   sizes="(max-width: 672px) 100vw, 672px"/>
                                        </div>
                                        <div class="portfolio-content">
                                            <div class="portfolio-content-inner">
                                                <div class="entry-category"><a
                                                        href="../category-portfolio/illustration/index.html">Illustration</a>
                                                </div>
                                                <h2 class="entry-title"><a
                                                        href="the-new-design-frontier/index.html">The New Design
                                                        Frontier</a></h2></div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="column-item portfolio-entries">
                                <article id="post-311"
                                         class="portfolio post-311 osf_portfolio type-osf_portfolio status-publish has-post-thumbnail hentry osf_portfolio_cat-digital osf_portfolio_cat-photography">
                                    <div class="portfolio-inner">
                                        <div class="portfolio-post-thumbnail"><img width="700" height="485"
                                                                                   src="../wp-content/uploads/2019/03/portfolio-6-1-700x485.jpg"
                                                                                   class="attachment-startor-gallery-image size-startor-gallery-image wp-post-image"
                                                                                   alt=""
                                                                                   srcset="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-6-1-700x485.jpg 700w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-6-1-300x208.jpg 300w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-6-1-768x532.jpg 768w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-6-1-1024x709.jpg 1024w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-6-1.jpg 1040w"
                                                                                   sizes="(max-width: 700px) 100vw, 700px"/>
                                        </div>
                                        <div class="portfolio-content">
                                            <div class="portfolio-content-inner">
                                                <div class="entry-category"><a
                                                        href="../category-portfolio/digital/index.html">Digital</a>,
                                                    <a href="../category-portfolio/photography/index.html">Photography</a>
                                                </div>
                                                <h2 class="entry-title"><a href="praxisklinik-branding/index.html">Praxisklinik
                                                        | branding</a></h2></div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="column-item portfolio-entries">
                                <article id="post-310"
                                         class="portfolio post-310 osf_portfolio type-osf_portfolio status-publish has-post-thumbnail hentry osf_portfolio_cat-creative">
                                    <div class="portfolio-inner">
                                        <div class="portfolio-post-thumbnail"><img width="700" height="525"
                                                                                   src="../wp-content/uploads/2019/03/portfolio-2-700x525.jpg"
                                                                                   class="attachment-startor-gallery-image size-startor-gallery-image wp-post-image"
                                                                                   alt=""
                                                                                   srcset="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-2-700x525.jpg 700w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-2-300x225.jpg 300w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-2-768x576.jpg 768w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-2-1024x768.jpg 1024w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-2.jpg 1200w"
                                                                                   sizes="(max-width: 700px) 100vw, 700px"/>
                                        </div>
                                        <div class="portfolio-content">
                                            <div class="portfolio-content-inner">
                                                <div class="entry-category"><a
                                                        href="../category-portfolio/creative/index.html">Creative</a>
                                                </div>
                                                <h2 class="entry-title"><a
                                                        href="dissection/index.html">Dissection</a></h2></div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="column-item portfolio-entries">
                                <article id="post-309"
                                         class="portfolio post-309 osf_portfolio type-osf_portfolio status-publish has-post-thumbnail hentry osf_portfolio_cat-illustration">
                                    <div class="portfolio-inner">
                                        <div class="portfolio-post-thumbnail"><img width="700" height="591"
                                                                                   src="../wp-content/uploads/2019/03/portfolio-1-1-700x591.jpg"
                                                                                   class="attachment-startor-gallery-image size-startor-gallery-image wp-post-image"
                                                                                   alt=""
                                                                                   srcset="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-1-1-700x591.jpg 700w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-1-1-300x253.jpg 300w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-1-1-768x648.jpg 768w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-1-1-1024x864.jpg 1024w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-1-1.jpg 1200w"
                                                                                   sizes="(max-width: 700px) 100vw, 700px"/>
                                        </div>
                                        <div class="portfolio-content">
                                            <div class="portfolio-content-inner">
                                                <div class="entry-category"><a
                                                        href="../category-portfolio/illustration/index.html">Illustration</a>
                                                </div>
                                                <h2 class="entry-title"><a href="isotope-generation/index.html">Isotope
                                                        Generation</a></h2></div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="column-item portfolio-entries">
                                <article id="post-308"
                                         class="portfolio post-308 osf_portfolio type-osf_portfolio status-publish has-post-thumbnail hentry osf_portfolio_cat-digital">
                                    <div class="portfolio-inner">
                                        <div class="portfolio-post-thumbnail"><img width="700" height="438"
                                                                                   src="../wp-content/uploads/2019/03/portfolio-1-700x438.jpg"
                                                                                   class="attachment-startor-gallery-image size-startor-gallery-image wp-post-image"
                                                                                   alt=""
                                                                                   srcset="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-1-700x438.jpg 700w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-1-300x188.jpg 300w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-1-768x480.jpg 768w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-1-1024x640.jpg 1024w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-1.jpg 1200w"
                                                                                   sizes="(max-width: 700px) 100vw, 700px"/>
                                        </div>
                                        <div class="portfolio-content">
                                            <div class="portfolio-content-inner">
                                                <div class="entry-category"><a
                                                        href="../category-portfolio/digital/index.html">Digital</a>
                                                </div>
                                                <h2 class="entry-title"><a href="aurora-biennal/index.html">Aurora
                                                        Biennal</a></h2></div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="column-item portfolio-entries">
                                <article id="post-307"
                                         class="portfolio post-307 osf_portfolio type-osf_portfolio status-publish has-post-thumbnail hentry osf_portfolio_cat-photography">
                                    <div class="portfolio-inner">
                                        <div class="portfolio-post-thumbnail"><img width="700" height="525"
                                                                                   src="../wp-content/uploads/2019/03/portfolio-2-700x525.jpg"
                                                                                   class="attachment-startor-gallery-image size-startor-gallery-image wp-post-image"
                                                                                   alt=""
                                                                                   srcset="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-2-700x525.jpg 700w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-2-300x225.jpg 300w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-2-768x576.jpg 768w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-2-1024x768.jpg 1024w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-2.jpg 1200w"
                                                                                   sizes="(max-width: 700px) 100vw, 700px"/>
                                        </div>
                                        <div class="portfolio-content">
                                            <div class="portfolio-content-inner">
                                                <div class="entry-category"><a
                                                        href="../category-portfolio/photography/index.html">Photography</a>
                                                </div>
                                                <h2 class="entry-title"><a href="witcher-3-portraits/index.html">Witcher
                                                        3 Portraits</a></h2></div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="column-item portfolio-entries">
                                <article id="post-306"
                                         class="portfolio post-306 osf_portfolio type-osf_portfolio status-publish has-post-thumbnail hentry osf_portfolio_cat-illustration">
                                    <div class="portfolio-inner">
                                        <div class="portfolio-post-thumbnail"><img width="700" height="525"
                                                                                   src="../wp-content/uploads/2019/03/portfolio-6-700x525.jpg"
                                                                                   class="attachment-startor-gallery-image size-startor-gallery-image wp-post-image"
                                                                                   alt=""
                                                                                   srcset="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-6-700x525.jpg 700w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-6-300x225.jpg 300w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-6-768x576.jpg 768w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-6-1024x768.jpg 1024w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-6.jpg 1200w"
                                                                                   sizes="(max-width: 700px) 100vw, 700px"/>
                                        </div>
                                        <div class="portfolio-content">
                                            <div class="portfolio-content-inner">
                                                <div class="entry-category"><a
                                                        href="../category-portfolio/illustration/index.html">Illustration</a>
                                                </div>
                                                <h2 class="entry-title"><a href="better-booch/index.html">Better
                                                        Booch</a></h2></div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="column-item portfolio-entries">
                                <article id="post-305"
                                         class="portfolio post-305 osf_portfolio type-osf_portfolio status-publish has-post-thumbnail hentry osf_portfolio_cat-agriculture">
                                    <div class="portfolio-inner">
                                        <div class="portfolio-post-thumbnail"><img width="700" height="525"
                                                                                   src="../wp-content/uploads/2019/03/portfolio-3-1-700x525.jpg"
                                                                                   class="attachment-startor-gallery-image size-startor-gallery-image wp-post-image"
                                                                                   alt=""
                                                                                   srcset="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-3-1-700x525.jpg 700w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-3-1-300x225.jpg 300w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-3-1-768x576.jpg 768w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-3-1-1024x768.jpg 1024w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-3-1.jpg 1200w"
                                                                                   sizes="(max-width: 700px) 100vw, 700px"/>
                                        </div>
                                        <div class="portfolio-content">
                                            <div class="portfolio-content-inner">
                                                <div class="entry-category"><a
                                                        href="../category-portfolio/agriculture/index.html">Agriculture</a>
                                                </div>
                                                <h2 class="entry-title"><a href="the-magic-plant/index.html">The
                                                        Magic Plant</a></h2></div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="column-item portfolio-entries">
                                <article id="post-304"
                                         class="portfolio post-304 osf_portfolio type-osf_portfolio status-publish has-post-thumbnail hentry osf_portfolio_cat-photography">
                                    <div class="portfolio-inner">
                                        <div class="portfolio-post-thumbnail"><img width="700" height="485"
                                                                                   src="../wp-content/uploads/2019/03/portfolio-7-700x485.jpg"
                                                                                   class="attachment-startor-gallery-image size-startor-gallery-image wp-post-image"
                                                                                   alt=""
                                                                                   srcset="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-7-700x485.jpg 700w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-7-300x208.jpg 300w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-7-768x532.jpg 768w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-7-1024x709.jpg 1024w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/portfolio-7.jpg 1040w"
                                                                                   sizes="(max-width: 700px) 100vw, 700px"/>
                                        </div>
                                        <div class="portfolio-content">
                                            <div class="portfolio-content-inner">
                                                <div class="entry-category"><a
                                                        href="../category-portfolio/photography/index.html">Photography</a>
                                                </div>
                                                <h2 class="entry-title"><a href="le-petit-soldat/index.html">Le
                                                        Petit Soldat</a></h2></div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>
                        <nav class="navigation pagination" role="navigation"><h2 class="screen-reader-text">Posts
                                navigation</h2>
                            <div class="nav-links"><span aria-current='page' class='page-numbers current'><span
                                        class="meta-nav screen-reader-text">Page </span>1</span> <a class='page-numbers'
                                                                                                    href='page/2/index.html'><span
                                        class="meta-nav screen-reader-text">Page </span>2</a> <span
                                    class="page-numbers dots">&hellip;</span> <a class='page-numbers'
                                                                                 href='page/9/index.html'><span
                                        class="meta-nav screen-reader-text">Page </span>9</a> <a
                                    class="next page-numbers" href="page/2/index.html"><span
                                        class="screen-reader-text">Next page</span><span
                                        class="opal-icon-chevron-right"></span></a></div>
                        </nav>
                    </main>
                </div>
            </div>
        </div>
    </div>
@endsection
