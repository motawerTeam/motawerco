@extends('layouts.app')
@section('title')
    Home
@endsection
@section('menu')
active
@endsection

@section('header')
    @include('partials.slider')
@endsection
@section('content')
    <div
        class="elementor-element elementor-element-4c8012b elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
        data-id="4c8012b" data-element_type="section"
        data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-container elementor-column-gap-no">
            <div class="elementor-row">
                <div
                    class="elementor-element elementor-element-84ef2d1 elementor-column elementor-col-100 elementor-top-column"
                    data-id="84ef2d1" data-element_type="column">
                    <div class="elementor-column-wrap  elementor-element-populated">
                        <div class="elementor-widget-wrap">
                            <div
                                class="elementor-element elementor-element-c33a8c1 elementor-align-center animated-fast elementor-invisible elementor-widget elementor-widget-heading"
                                data-id="c33a8c1" data-element_type="widget"
                                data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;}"
                                data-widget_type="heading.default">
                                <div class="elementor-widget-container"><span
                                        class="sub-title">What's Startor</span>
                                    <h2 class="elementor-heading-title elementor-size-default">
                                        Creating a world of good</h2></div>
                            </div>
                            <div
                                class="elementor-element elementor-element-ef72661 animated-fast elementor-invisible elementor-widget elementor-widget-text-editor"
                                data-id="ef72661" data-element_type="widget"
                                data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;}"
                                data-widget_type="text-editor.default">
                                <div class="elementor-widget-container">
                                    <div
                                        class="elementor-text-editor elementor-clearfix">
                                        <p>Startor is an app for iOS and Android that
                                            allows you to create and contribute to
                                            a<br/>chain-reaction of kindness.</p></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div
        class="elementor-element elementor-element-cc9db1e elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
        data-id="cc9db1e" data-element_type="section">
        <div class="elementor-container elementor-column-gap-no">
            <div class="elementor-row">
                <div
                    class="elementor-element elementor-element-d5f81f4 animated-fast elementor-invisible elementor-column elementor-col-33 elementor-top-column"
                    data-id="d5f81f4" data-element_type="column"
                    data-settings="{&quot;animation&quot;:&quot;opal-move-up&quot;}">
                    <div class="elementor-column-wrap  elementor-element-populated">
                        <div class="elementor-widget-wrap">
                            <div
                                class="elementor-element elementor-element-f49be3d elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-image-box"
                                data-id="f49be3d" data-element_type="widget"
                                data-widget_type="image-box.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-image-box-wrapper">
                                        <div class="elementor-image-framed">
                                            <figure class="elementor-image-box-img">
                                                <svg enable-background="new 0 0 250 120"
                                                     viewBox="0 0 250 120"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="m19.9 118.5h205.3v1.5h-205.3z"
                                                        fill="#377dff"></path>
                                                    <path d="m12.9 118.5h4.8v1.5h-4.8z"
                                                          fill="#377dff"></path>
                                                    <path d="m227.7 118.5h9.4v1.5h-9.4z"
                                                          fill="#377dff"></path>
                                                    <path
                                                        d="m84.1 114c0 2.6-2.1 4.7-4.7 4.7h-44.9c-2.6 0-4.7-2.1-4.7-4.7v-84c0-2.6 2.1-4.7 4.7-4.7h44.9c2.6 0 4.7 2.1 4.7 4.7z"
                                                        fill="#5ac2f3"></path>
                                                    <path
                                                        d="m79.4 119.3h-44.9c-2.9 0-5.2-2.3-5.2-5.2v-84.1c0-2.9 2.3-5.2 5.2-5.2h44.9c2.9 0 5.2 2.3 5.2 5.2v84c0 2.9-2.3 5.3-5.2 5.3zm-44.9-93.4c-2.3 0-4.1 1.8-4.1 4.1v84c0 2.3 1.8 4.1 4.1 4.1h44.9c2.3 0 4.1-1.8 4.1-4.1v-84c0-2.3-1.8-4.1-4.1-4.1z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m64.4 32.2h-15.5c-.3 0-.6-.3-.6-.6s.3-.6.6-.6h15.5c.3 0 .6.3.6.6s-.3.6-.6.6z"
                                                        fill="#000100"></path>
                                                    <path
                                                        d="m34.3 38.3h44.9v64.2h-44.9z"
                                                        fill="#fff"></path>
                                                    <path
                                                        d="m79.2 103.1h-44.9c-.3 0-.6-.3-.6-.6v-64.2c0-.3.3-.6.6-.6h44.9c.3 0 .6.3.6.6v64.2c0 .3-.3.6-.6.6zm-44.4-1.2h43.8v-63h-43.8z"
                                                        fill="#377dff"></path>
                                                    <path d="m39.2 47.7h6.6v6.6h-6.6z"
                                                          fill="#19a0ff"></path>
                                                    <path d="m49 47.7h6.6v6.6h-6.6z"
                                                          fill="#fff"></path>
                                                    <path
                                                        d="m55.5 54.9h-6.5c-.3 0-.6-.3-.6-.6v-6.6c0-.3.3-.6.6-.6h6.6c.3 0 .6.3.6.6v6.6c-.1.3-.4.6-.7.6zm-6-1.2h5.5v-5.4h-5.4v5.4z"
                                                        fill="#377dff"></path>
                                                    <path d="m58.3 47.7h6.6v6.6h-6.6z"
                                                          fill="#19a0ff"></path>
                                                    <path d="m67.5 47.7h6.6v6.6h-6.6z"
                                                          fill="#19a0ff"></path>
                                                    <path d="m67.5 58.6h6.6v6.6h-6.6z"
                                                          fill="#fff"></path>
                                                    <path
                                                        d="m74.1 65.8h-6.6c-.3 0-.6-.3-.6-.6v-6.6c0-.3.3-.6.6-.6h6.6c.3 0 .6.3.6.6v6.6c-.1.3-.3.6-.6.6zm-6-1.2h5.4v-5.4h-5.4z"
                                                        fill="#377dff"></path>
                                                    <path d="m39.2 58.6h6.6v6.6h-6.6z"
                                                          fill="#5ac2f3"></path>
                                                    <path d="m49 58.6h6.6v6.6h-6.6z"
                                                          fill="#19a0ff"></path>
                                                    <path d="m58.3 58.6h6.6v6.6h-6.6z"
                                                          fill="#19a0ff"></path>
                                                    <path d="m67.5 69.9h6.6v6.6h-6.6z"
                                                          fill="#5ac2f3"></path>
                                                    <path d="m39.2 69.9h6.6v6.6h-6.6z"
                                                          fill="#19a0ff"></path>
                                                    <path d="m49 69.9h6.6v6.6h-6.6z"
                                                          fill="#fff"></path>
                                                    <path
                                                        d="m55.5 77.1h-6.5c-.3 0-.6-.3-.6-.6v-6.6c0-.3.3-.6.6-.6h6.6c.3 0 .6.3.6.6v6.6c-.1.3-.4.6-.7.6zm-6-1.2h5.5v-5.4h-5.4v5.4z"
                                                        fill="#377dff"></path>
                                                    <path d="m58.3 69.9h6.6v6.6h-6.6z"
                                                          fill="#19a0ff"></path>
                                                    <path
                                                        d="m60.1 83.4h10.6v10.6h-10.6z"
                                                        fill="#fff"></path>
                                                    <path
                                                        d="m70.7 94.6h-10.6c-.3 0-.6-.3-.6-.6v-10.6c0-.3.3-.6.6-.6h10.6c.3 0 .6.3.6.6v10.6c0 .4-.3.6-.6.6zm-10-1.1h9.5v-9.5h-9.5z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m49.7 95.8c-.3 0-.6-.3-.6-.6v-12.9c0-.3.3-.6.6-.6s.6.3.6.6v12.9c0 .3-.3.6-.6.6z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m56.2 89.3h-13c-.3 0-.6-.3-.6-.6s.3-.6.6-.6h12.9c.3 0 .6.3.6.6s-.2.6-.5.6z"
                                                        fill="#377dff"></path>
                                                    <circle cx="57" cy="110.5"
                                                            fill="#fff"
                                                            r="4.3"></circle>
                                                    <circle cx="57" cy="110.5"
                                                            fill="#5ac2f3"
                                                            r="1.6"></circle>
                                                    <path d="m80.1 60h82.1v58.5h-82.1z"
                                                          fill="#fff"></path>
                                                    <path
                                                        d="m162.1 119.4h-82c-.5 0-.8-.4-.8-.8v-58.6c0-.5.4-.8.8-.8h82.1c.5 0 .8.4.8.8v58.5c0 .5-.4.9-.9.9zm-81.2-1.7h80.4v-56.8h-80.4z"
                                                        fill="#377dff"></path>
                                                    <path d="m84.9 94h13.9v13.1h-13.9z"
                                                          fill="#19a0ff"></path>
                                                    <path
                                                        d="m168.7 104.4c0 3.6-2.9 6.5-6.5 6.5h-67v-57.2h73.5v46.3z"
                                                        fill="#5ac2f3"></path>
                                                    <path
                                                        d="m162.1 111.8h-67c-.5 0-.8-.4-.8-.8v-57.3c0-.5.4-.8.8-.8h73.5c.5 0 .8.4.8.8v50.7c.1 4.1-3.2 7.4-7.3 7.4zm-66.1-1.7h66.1c3.1 0 5.7-2.5 5.7-5.7v-49.8h-71.8z"
                                                        fill="#377dff"></path>
                                                    <circle cx="95.2" cy="80.2"
                                                            fill="#5ac2f3"
                                                            r="10.2"></circle>
                                                    <path
                                                        d="m98.8 57.1v50h69.3c.4-.8.6-1.7.6-2.7v-4.4-42.9z"
                                                        fill="#fff"></path>
                                                    <path
                                                        d="m162.1 111.8h-67c-.5 0-.8-.4-.8-.8v-57.3c0-.5.4-.8.8-.8h73.5c.5 0 .8.4.8.8s-.4.8-.8.8h-72.6v55.5h66.1c3.1 0 5.7-2.5 5.7-5.7v-4.3c0-.5.4-.8.8-.8.5 0 .8.4.8.8v4.4c.1 4.1-3.2 7.4-7.3 7.4z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m168.7 50.3c0-3.6-2.9-6.5-6.5-6.5s-6.5 2.9-6.5 6.5v54.6c0-3.6 2.9-6.5 6.5-6.5s6.5 2.9 6.5 6.5z"
                                                        fill="#fff"></path>
                                                    <g fill="#377dff">
                                                        <path
                                                            d="m168.7 105.8c-.5 0-.8-.4-.8-.8 0-3.1-2.5-5.7-5.7-5.7-3.1 0-5.7 2.5-5.7 5.7 0 .5-.4.8-.8.8s-.8-.4-.8-.8v-54.7c0-4.1 3.3-7.4 7.4-7.4s7.4 3.3 7.4 7.4v54.6c-.2.5-.6.9-1 .9zm-6.6-8.3c2.3 0 4.3 1 5.7 2.7v-49.9c0-3.1-2.5-5.7-5.7-5.7-3.1 0-5.7 2.5-5.7 5.7v49.9c1.4-1.6 3.5-2.7 5.7-2.7z"></path>
                                                        <path
                                                            d="m123.5 88.6h-20.2c-.5 0-.8-.4-.8-.8s.4-.8.8-.8h20.2c.5 0 .8.4.8.8s-.3.8-.8.8z"></path>
                                                        <path
                                                            d="m151.1 88.6h-19.6c-.5 0-.8-.4-.8-.8s.4-.8.8-.8h19.6c.5 0 .8.4.8.8s-.3.8-.8.8z"></path>
                                                        <path
                                                            d="m123.5 94.9h-20.2c-.5 0-.8-.4-.8-.8s.4-.8.8-.8h20.2c.5 0 .8.4.8.8s-.3.8-.8.8z"></path>
                                                        <path
                                                            d="m151.1 94.9h-19.6c-.5 0-.8-.4-.8-.8s.4-.8.8-.8h19.6c.5 0 .8.4.8.8s-.3.8-.8.8z"></path>
                                                        <path
                                                            d="m123.5 101.7h-20.2c-.5 0-.8-.4-.8-.8 0-.5.4-.8.8-.8h20.2c.5 0 .8.4.8.8.1.5-.3.8-.8.8z"></path>
                                                        <path
                                                            d="m151.1 101.7h-19.6c-.5 0-.8-.4-.8-.8 0-.5.4-.8.8-.8h19.6c.5 0 .8.4.8.8.1.5-.3.8-.8.8z"></path>
                                                    </g>
                                                    <path d="m103.4 62h20.2v20.2h-20.2z"
                                                          fill="#19a0ff"></path>
                                                    <path d="m131.2 62h20.2v20.2h-20.2z"
                                                          fill="#19a0ff"></path>
                                                    <path d="m103.4 62h20.2v20.2h-20.2z"
                                                          fill="#fff"></path>
                                                    <path
                                                        d="m123.5 83h-20.2c-.5 0-.8-.4-.8-.8v-20.2c0-.5.4-.8.8-.8h20.2c.5 0 .8.4.8.8v20.2c.1.4-.3.8-.8.8zm-19.3-1.7h18.5v-18.5h-18.5z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m123.5 83c-.2 0-.4-.1-.6-.2l-20.2-20.2c-.3-.3-.3-.9 0-1.2s.9-.3 1.2 0l20.2 20.2c.3.3.3.9 0 1.2-.2.1-.4.2-.6.2z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m103.4 83c-.2 0-.4-.1-.6-.2-.3-.3-.3-.9 0-1.2l20.2-20.2c.3-.3.9-.3 1.2 0s.3.9 0 1.2l-20.2 20.1c-.2.2-.4.3-.6.3z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m185.5 59.7-3.8-10-3.7 10v42.7h7.5z"
                                                        fill="#19a0ff"></path>
                                                    <path
                                                        d="m185.5 103.3h-7.5c-.5 0-.8-.4-.8-.8v-42.8c0-.1 0-.2.1-.3l3.8-10c.1-.3.4-.6.8-.6.4 0 .7.2.8.6l3.8 10c0 .1.1.2.1.3v42.8c-.3.4-.7.8-1.1.8zm-6.7-1.7h5.8v-41.8l-2.9-7.7-2.9 7.7z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m185.5 103.3h-7.5c-.5 0-.8-.4-.8-.8v-42.8c0-.1 0-.2.1-.3l3.8-10c.1-.3.4-.6.8-.6.4 0 .7.2.8.6l3.8 10c0 .1.1.2.1.3v42.8c-.3.4-.7.8-1.1.8zm-6.7-1.7h5.8v-41.8l-2.9-7.7-2.9 7.7z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m185.5 109.9h-7.5c-.5 0-.8-.4-.8-.8v-6.7c0-.5.4-.8.8-.8h7.5c.5 0 .8.4.8.8v6.7c0 .4-.4.8-.8.8zm-6.7-1.7h5.8v-5h-5.8z"
                                                        fill="#377dff"></path>
                                                    <path d="m178 96.9h7.5v5.4h-7.5z"
                                                          fill="#5ac2f3"></path>
                                                    <path
                                                        d="m185.5 103.1h-7.5c-.5 0-.8-.4-.8-.8v-5.4c0-.5.4-.8.8-.8h7.5c.5 0 .8.4.8.8v5.4c0 .5-.4.8-.8.8zm-6.7-1.7h5.8v-3.7h-5.8z"
                                                        fill="#377dff"></path>
                                                    <path d="m178 59.7h7.5l-3.8-10z"
                                                          fill="#fff"></path>
                                                    <path
                                                        d="m185.5 60.5h-7.5c-.3 0-.5-.1-.7-.4-.2-.2-.2-.5-.1-.8l3.8-10c.1-.3.4-.6.8-.6.4 0 .7.2.8.6l3.8 10c.1.3.1.6-.1.8-.3.3-.5.4-.8.4zm-6.3-1.7h5.1l-2.5-6.7z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m204.5 104.9s-2.6-1.8-2.6-11.5v-.5c0-9.6 2.6-11.5 2.6-11.5h-11.7s2.6 1.8 2.6 11.5v.5c0 9.6-2.6 11.5-2.6 11.5z"
                                                        fill="#fff"></path>
                                                    <path
                                                        d="m204.5 105.6h-11.7c-.3 0-.6-.2-.7-.5s0-.6.3-.8c0 0 2.3-1.9 2.3-10.9v-.5c0-9-2.3-10.9-2.3-10.9-.3-.2-.4-.5-.3-.8s.4-.5.7-.5h11.7c.3 0 .6.2.7.5s0 .6-.3.8c0 0-2.3 1.9-2.3 10.9v.5c0 9 2.3 10.9 2.3 10.9.3.2.4.5.3.8s-.4.5-.7.5zm-10.3-1.4h8.8c-.8-1.5-1.9-4.6-1.9-10.8v-.5c0-6.2 1.1-9.3 1.9-10.8h-8.8c.8 1.5 1.9 4.6 1.9 10.8v.5c0 6.1-1 9.3-1.9 10.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m198.6 100.6c-.4 0-.7-.3-.7-.7v-13.5c0-.4.3-.7.7-.7s.7.3.7.7v13.5c0 .4-.3.7-.7.7z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m202.5 67.8v6.8c0 .9-.7 1.5-1.6 1.5h-4.4c-.9 0-1.6-.7-1.6-1.5v-6.9c-2.7 1.3-4.5 3.9-4.5 6.9v1.1c0 4.3 3.6 7.7 8.1 7.7h.5c4.5 0 8.1-3.5 8.1-7.7v-1.1c-.2-2.9-2-5.5-4.6-6.8z"
                                                        fill="#fff"></path>
                                                    <path
                                                        d="m198.9 84.2h-.5c-4.8 0-8.8-3.8-8.8-8.4v-1.1c0-3.2 1.9-6.1 4.9-7.6.2-.1.5-.1.7 0s.3.4.3.6v6.9c0 .5.4.8.9.8h4.4c.5 0 .9-.4.9-.8v-6.8c0-.2.1-.5.3-.6s.5-.1.7 0c3 1.4 4.9 4.3 4.9 7.5v1.1c0 4.7-3.9 8.4-8.7 8.4zm-4.8-15.2c-2 1.3-3.1 3.4-3.1 5.7v1.1c0 3.9 3.3 7 7.4 7h.5c4.1 0 7.4-3.2 7.4-7v-1.1c0-2.3-1.1-4.4-3.1-5.7v5.6c0 1.2-1 2.2-2.3 2.2h-4.4c-1.3 0-2.3-1-2.3-2.2v-5.6z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m202.5 118.5v-6.8c0-.9-.7-1.5-1.6-1.5h-4.4c-.9 0-1.6.7-1.6 1.5v6.9c-2.7-1.3-4.5-3.9-4.5-6.9v-1.1c0-4.3 3.6-7.7 8.1-7.7h.5c4.5 0 8.1 3.5 8.1 7.7v1.1c-.2 3-2 5.5-4.6 6.8z"
                                                        fill="#fff"></path>
                                                    <path
                                                        d="m194.8 119.3c-.1 0-.2 0-.3-.1-3.1-1.4-4.9-4.3-4.9-7.6v-1.1c0-4.7 3.9-8.4 8.8-8.4h.5c4.8 0 8.8 3.8 8.8 8.4v1.1c0 3.2-1.9 6.1-4.9 7.5-.2.1-.5.1-.7 0s-.3-.4-.3-.6v-6.8c0-.5-.4-.8-.9-.8h-4.4c-.5 0-.9.4-.9.8v6.9c0 .2-.1.5-.3.6-.2 0-.3.1-.5.1zm3.6-15.8c-4.1 0-7.4 3.2-7.4 7v1.1c0 2.3 1.2 4.4 3.1 5.7v-5.7c0-1.2 1-2.2 2.3-2.2h4.4c1.3 0 2.3 1 2.3 2.2v5.6c1.9-1.3 3.1-3.4 3.1-5.7v-1.1c0-3.9-3.3-7-7.4-7h-.4z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m177.1 20.7h21.2v21.2h-21.2z"
                                                        fill="#5ac2f3"></path>
                                                    <path class="opal-svg-spinner"
                                                          d="m199.6 53.5c-.3-.9.3-1.9 1.2-2.2.9-.3 1.9.3 2.2 1.2.3.9-.3 1.9-1.2 2.2-.2 0-.3.1-.5.1-.8 0-1.5-.5-1.7-1.3zm-6.6.9c-.9-.3-1.4-1.3-1-2.2.3-.9 1.3-1.4 2.2-1 .9.3 1.4 1.3 1 2.2-.3.7-.9 1.1-1.6 1.1-.2 0-.4 0-.6-.1zm13.6-4.4c-.8-.6-.9-1.7-.3-2.4.6-.8 1.7-.9 2.4-.3.8.6.9 1.7.3 2.4-.3.5-.9.7-1.4.7-.3 0-.7-.1-1-.4zm-20.5-1.1c-.5-.8-.3-1.9.5-2.4.8-.5 1.9-.3 2.4.5.5.8.3 1.9-.5 2.4-.3.2-.6.3-.9.3-.6 0-1.2-.3-1.5-.8zm22.2-7.6c0-1 .8-1.7 1.7-1.7 1 0 1.7.8 1.7 1.7 0 1-.8 1.7-1.7 1.7-.9.1-1.7-.7-1.7-1.7zm-22.8.9c-1-.1-1.7-.9-1.6-1.9.1-1 .9-1.7 1.9-1.6 1 .1 1.7.9 1.6 1.9-.1.9-.8 1.6-1.7 1.6-.1 0-.1 0-.2 0zm21.5-5.9c-.5-.8-.2-1.9.7-2.4.8-.5 1.9-.2 2.4.7.5.8.2 1.9-.7 2.4-.3.1-.6.2-.8.2-.7 0-1.2-.3-1.6-.9zm-19.5-1.6c-.7-.6-.8-1.7-.2-2.5.6-.7 1.7-.8 2.5-.2.7.6.8 1.7.2 2.5-.3.4-.8.6-1.3.6-.5 0-.9-.2-1.2-.4zm14.7-2.9c-.9-.4-1.3-1.4-.9-2.3.4-.9 1.4-1.3 2.3-.9.9.4 1.3 1.4.9 2.3-.3.6-.9 1-1.6 1-.2.1-.5 0-.7-.1zm-8.7-2c-.2-.9.4-1.9 1.3-2.1.9-.2 1.9.4 2.1 1.3.2.9-.4 1.9-1.3 2.1-.1 0-.2 0-.4 0-.8 0-1.5-.5-1.7-1.3z"
                                                          fill="#377dff"></path>
                                                    <path class="opal-svg-spinner"
                                                          d="m136.1 31.9-6.7 1.8-1.8 6.7 4.9 4.9 6.7-1.8 1.8-6.7zm-1.8 9.3c-1.5 0-2.6-1.2-2.6-2.6s1.2-2.6 2.6-2.6c1.5 0 2.6 1.2 2.6 2.6s-1.2 2.6-2.6 2.6z"
                                                          fill="#5ac2f3"></path>
                                                    <path class="opal-svg-spinner"
                                                          d="m159.4 16.2c-.3-.3-.6-.6-.9-.9l1.7-2.9-3.3-2-1.7 2.9c-.7-.3-1.5-.4-2.2-.5l-.3-3.4-3.9.3.3 3.4c-.7.2-1.4.5-2.1.9l-2.2-2.6-2.9 2.6 2.2 2.6c-.5.6-.9 1.3-1.2 2l-3.3-.8-.9 3.8 3.3.8c0 .8 0 1.5.2 2.3l-3.1 1.3 1.5 3.6 3.1-1.3c.2.4.5.7.7 1.1.2.3.5.5.8.7l-1.7 2.8 3.3 2 1.7-2.8c.8.3 1.5.5 2.3.6l.3 3.2 3.9-.3-.3-3.2c.8-.2 1.5-.5 2.2-.9l2.1 2.4 2.9-2.6-2.1-2.4c.5-.7.9-1.4 1.2-2.1l3.1.7.9-3.8-3.2-.7c0-.8-.1-1.6-.2-2.4l3-1.3-1.5-3.6-3.1 1.3c-.2-.3-.4-.6-.6-.8zm-3.9 10.8c-2.4 2.1-6 1.8-8.1-.6s-1.8-6 .6-8.1 6-1.8 8.1.6 1.8 6-.6 8.1z"
                                                          fill="#19a0ff"></path>
                                                    <path
                                                        d="m149.7 24.4c-1-1.1-.9-2.9.3-3.9 1.1-1 2.9-.9 3.9.3 1 1.1.9 2.9-.3 3.9s-2.9.9-3.9-.3z"
                                                        fill="#19a0ff"></path>
                                                    <path
                                                        d="m107.2 36.2c-.1 0-.3 0-.4-.1-.4-.2-.6-.8-.4-1.2l5.3-10.3c.2-.4.8-.6 1.2-.4s.6.8.4 1.2l-5.3 10.3c-.2.3-.5.5-.8.5z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m104.4 36.2c-.2 0-.5-.1-.7-.3l-5-5.1c-.3-.4-.3-.9 0-1.3l5-5.1c.4-.4.9-.4 1.3 0s.4.9 0 1.3l-4.4 4.5 4.4 4.5c.4.4.3.9 0 1.3-.2.1-.4.2-.6.2z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m115.8 36.2c-.2 0-.5-.1-.6-.3-.4-.4-.4-.9 0-1.3l4.4-4.5-4.4-4.5c-.4-.4-.3-.9 0-1.3.4-.4.9-.3 1.3 0l5 5.1c.3.4.3.9 0 1.3l-5 5.1c-.2.3-.5.4-.7.4z"
                                                        fill="#377dff"></path>
                                                </svg>
                                            </figure>
                                        </div>
                                        <div class="elementor-image-box-content"><h3
                                                class="elementor-image-box-title">
                                                Create</h3>
                                            <p class="elementor-image-box-description">
                                                Achieve virtually any design and layout
                                                from within the one template.</p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="elementor-element elementor-element-bb0d642 animated-fast elementor-invisible elementor-column elementor-col-33 elementor-top-column"
                    data-id="bb0d642" data-element_type="column"
                    data-settings="{&quot;animation&quot;:&quot;opal-move-up&quot;,&quot;animation_delay&quot;:150}">
                    <div class="elementor-column-wrap  elementor-element-populated">
                        <div class="elementor-widget-wrap">
                            <div
                                class="elementor-element elementor-element-4ba6d6e elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-image-box"
                                data-id="4ba6d6e" data-element_type="widget"
                                data-widget_type="image-box.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-image-box-wrapper">
                                        <div class="elementor-image-framed">
                                            <figure class="elementor-image-box-img">
                                                <svg enable-background="new 0 0 250 120"
                                                     viewBox="0 0 250 120"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="m19.9 118.5h205.3v1.5h-205.3z"
                                                        fill="#377dff"></path>
                                                    <path d="m12.9 118.5h4.8v1.5h-4.8z"
                                                          fill="#377dff"></path>
                                                    <path d="m227.7 118.5h9.4v1.5h-9.4z"
                                                          fill="#377dff"></path>
                                                    <circle cx="174.1" cy="33.6"
                                                            fill="#19a0ff"
                                                            r="11.4"></circle>
                                                    <path
                                                        d="m195.8 54.8c0-1.7-1.4-3.1-3.1-3.1s-3.1 1.4-3.1 3.1v-2.3-.1c0-1.7-1.4-3.1-3.1-3.1s-3.1 1.4-3.1 3.1v-1.9-.1c0-1.7-1.4-3.1-3.1-3.1-1.6 0-3 1.3-3.1 2.9v-15.1c0-1.7-1.4-3.1-3.1-3.1s-3.1 1.4-3.1 3.1v18.1.2h-.1c-.3-3.2-2.9-5.6-6.1-5.6v5.6.5 6.8 6.1 6.1c0 4.1 2 7.7 5.1 9.9v17.2h21v-17.2c2.8-2.2 4.7-5.6 4.7-9.4v-18.4c.2-.1.2-.1.2-.2zm-18.4-2.3v-2z"
                                                        fill="#fff"></path>
                                                    <path
                                                        d="m191.2 100.8h-21c-.4 0-.8-.3-.8-.8v-16.8c-3.2-2.5-5.1-6.3-5.1-10.3v-25.2c0-.4.3-.8.8-.8 2.2 0 4.1 1 5.4 2.6v-14.4c0-2.1 1.7-3.8 3.8-3.8s3.7 1.6 3.8 3.7v.1 12.3c.6-.5 1.4-.8 2.3-.8 1.8 0 3.3 1.2 3.7 2.9.7-.5 1.5-.9 2.4-.9 1.9 0 3.5 1.4 3.8 3.2.7-.5 1.5-.8 2.4-.8 2.1 0 3.8 1.7 3.8 3.8v.2 18.4c0 3.8-1.7 7.4-4.7 9.8v16.8c.1.5-.2.8-.6.8zm-20.2-1.5h19.4v-16.5c0-.2.1-.5.3-.6 2.8-2.1 4.4-5.3 4.4-8.8v-18.4c0-.1 0-.1 0-.2 0-1.3-1.1-2.3-2.3-2.3-1.3 0-2.3 1-2.3 2.3 0 .4-.3.8-.8.8-.4 0-.8-.3-.8-.8v-2.3c0-.1 0-.1 0-.2 0-1.3-1.1-2.3-2.3-2.3-1.3 0-2.3 1-2.3 2.3 0 .4-.3.8-.8.8-.4 0-.8-.3-.8-.8v-1.9s0 0 0-.1v-.1c0-1.3-1.1-2.3-2.3-2.3s-2.3 1-2.3 2.2v.1.1.1 1.9c0 .4-.3.8-.8.8-.4 0-.8-.3-.8-.8v-2-.1-.1-14.9-.1c0-1.3-1-2.3-2.3-2.3s-2.3 1-2.3 2.3v18.3c0 .2-.1.4-.2.5s-.3.2-.6.2h-.1c-.4 0-.7-.3-.7-.7-.2-2.6-2.1-4.6-4.6-4.9v24.4c0 3.7 1.8 7.2 4.8 9.3.2.1.3.4.3.6v16.5z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m171.3 62.1c-.4 0-.8-.3-.8-.8v-8.5c0-.4.3-.8.8-.8s.8.3.8.8v8.5c-.1.5-.4.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m161.7 91.3h37.6v27.2h-37.6z"
                                                        fill="#5ac2f3"></path>
                                                    <path
                                                        d="m173.7 25.1c-.4 0-.8-.3-.8-.8v-2.8c0-.4.3-.8.8-.8s.8.3.8.8v2.8c0 .5-.4.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m173.7 25.1c-.4 0-.8-.3-.8-.8v-2.8c0-.4.3-.8.8-.8s.8.3.8.8v2.8c0 .5-.4.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m166.9 28.3c-.2 0-.4-.1-.5-.2l-2-2c-.3-.3-.3-.8 0-1.1s.8-.3 1.1 0l2 2c.3.3.3.8 0 1.1-.2.1-.4.2-.6.2z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m164.3 35.4h-2.8c-.4 0-.8-.3-.8-.8 0-.4.3-.8.8-.8h2.8c.4 0 .8.3.8.8s-.4.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m186.8 34.5h-2.8c-.4 0-.8-.3-.8-.8 0-.4.3-.8.8-.8h2.8c.4 0 .8.3.8.8s-.3.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m180.8 27.7c-.2 0-.4-.1-.5-.2-.3-.3-.3-.8 0-1.1l2-2c.3-.3.8-.3 1.1 0s.3.8 0 1.1l-2 2c-.2.1-.4.2-.6.2z"
                                                        fill="#377dff"></path>
                                                    <circle cx="167.4" cy="101"
                                                            fill="#377dff"
                                                            r="1.8"></circle>
                                                    <path
                                                        d="m200 114.9h-38.6c-.4 0-.8-.3-.8-.8s.3-.8.8-.8h38.6c.4 0 .8.3.8.8s-.4.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m54 101.9c0 .2-.2.4-.4.4h-2.4c-.2 0-.4-.2-.4-.4v-10.1c0-.1 0-.2.1-.3s.2-.1.3-.1h2.4c.2 0 .4.2.4.4zm-2.4-.5h1.5v-9.2h-1.5z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m52.4 119.2c2.7 0 2.7-2.7 2.7-2.7v-17.6h-5.4v17.6c0-.1 0 2.7 2.7 2.7z"
                                                        fill="#19a0ff"></path>
                                                    <circle cx="52.4" cy="116.2"
                                                            fill="#fff" r=".8"></circle>
                                                    <circle cx="52.4" cy="80.7"
                                                            fill="#5ac2f3"
                                                            r="13.6"></circle>
                                                    <path
                                                        d="m66.4 80.7c0 7.8-6.3 14.1-14.1 14.1s-14.1-6.3-14.1-14.1c0-3.8 1.5-7.3 4.1-9.9 2.7-2.7 6.2-4.1 9.9-4.1 7.9-.1 14.2 6.2 14.2 14zm-27.2 0c0 7.3 5.9 13.2 13.2 13.2s13.2-5.9 13.2-13.2h.4-.4c0-7.3-5.9-13.2-13.2-13.2-3.5 0-6.8 1.4-9.3 3.9-2.5 2.4-3.9 5.7-3.9 9.3z"
                                                        fill="#377dff"></path>
                                                    <circle cx="52.4" cy="80.7"
                                                            fill="#fff"
                                                            r="11.2"></circle>
                                                    <path
                                                        d="m51.8 73.7v6.5h-6.5c.1-3.6 3-6.5 6.5-6.5z"
                                                        fill="#fff"></path>
                                                    <path
                                                        d="m52.3 73.7v6.5c0 .2-.2.4-.4.4h-6.5c-.2 0-.4-.2-.4-.4 0-3.8 3.1-6.9 6.8-6.9h.1c.2 0 .4.2.4.4zm-.9 6v-5.6c-3 .2-5.3 2.6-5.6 5.6z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m60.1 80.6c0 1.8-.7 3.4-1.9 4.6l-4.6-4.6v-6.5c3.6 0 6.5 2.9 6.5 6.5z"
                                                        fill="#5ac2f3"></path>
                                                    <path
                                                        d="m52.3 88.4c1.8 0 3.4-.7 4.6-1.9l-4.6-4.6h-6.5c.1 3.6 3 6.5 6.5 6.5z"
                                                        fill="#19a0ff"></path>
                                                    <path
                                                        d="m66.4 99.2h21.2v19.3h-21.2z"
                                                        fill="#fff"></path>
                                                    <path
                                                        d="m87.7 119.3h-21.3c-.4 0-.8-.4-.8-.8v-19.3c0-.4.4-.8.8-.8h21.2c.4 0 .8.4.8.8v19.3c.1.4-.3.8-.7.8zm-20.5-1.6h19.6v-17.7h-19.6z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m131.7 99.2h21.2v19.3h-21.2z"
                                                        fill="#fff"></path>
                                                    <path
                                                        d="m153 119.3h-21.2c-.4 0-.8-.4-.8-.8v-19.3c0-.4.4-.8.8-.8h21.2c.4 0 .8.4.8.8v19.3c0 .4-.4.8-.8.8zm-20.4-1.6h19.6v-17.7h-19.6z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m92.2 37.9h35.5v19.3h-35.5z"
                                                        fill="#fff"></path>
                                                    <path
                                                        d="m127.7 58h-35.5c-.4 0-.8-.4-.8-.8v-19.3c0-.4.4-.8.8-.8h35.5c.4 0 .8.4.8.8v19.3c0 .4-.4.8-.8.8zm-34.7-1.7h33.9v-17.6h-33.9z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m109.9 67.1c-.4 0-.8-.4-.8-.8v-9.1c0-.4.4-.8.8-.8s.8.4.8.8v9.1c0 .4-.3.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m82.4 95.5c-.4 0-.8-.4-.8-.8v-9.1c0-.4.4-.8.8-.8s.8.4.8.8v9.1c0 .4-.3.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m109.9 95.5c-.4 0-.8-.4-.8-.8v-9.1c0-.4.4-.8.8-.8s.8.4.8.8v9.1c0 .4-.3.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m137.4 95.5c-.4 0-.8-.4-.8-.8v-9.1c0-.4.4-.8.8-.8s.8.4.8.8v9.1c.1.4-.3.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path d="m102 29.3h37.7v21.3h-37.7z"
                                                          fill="#19a0ff"></path>
                                                    <path
                                                        d="m131.7 40.7h-21.2c-.4 0-.8-.4-.8-.8s.4-.8.8-.8h21.2c.4 0 .8.4.8.8.1.5-.3.8-.8.8z"></path>
                                                    <g class="opal-svg-spinner"
                                                       fill="#377dff">
                                                        <path
                                                            d="m74.8 42c-.4 0-.8-.4-.8-.8v-1.8c0-.4.4-.8.8-.8s.8.4.8.8v1.8c0 .4-.4.8-.8.8z"></path>
                                                        <path
                                                            d="m74.8 42c-.4 0-.8-.4-.8-.8v-1.8c0-.4.4-.8.8-.8s.8.4.8.8v1.8c0 .4-.4.8-.8.8z"></path>
                                                        <path
                                                            d="m70.3 44c-.2 0-.4-.1-.6-.2l-1.3-1.3c-.3-.3-.3-.8 0-1.1s.8-.3 1.1 0l1.3 1.3c.3.3.3.8 0 1.1 0 .2-.3.2-.5.2z"></path>
                                                        <path
                                                            d="m68.7 48.6h-1.8c-.4 0-.8-.4-.8-.8s.4-.8.8-.8h1.8c.4 0 .8.4.8.8 0 .5-.4.8-.8.8z"></path>
                                                        <path
                                                            d="m69.5 54.3c-.2 0-.4-.1-.6-.2-.3-.3-.3-.8 0-1.1l1.3-1.3c.3-.3.8-.3 1.1 0s.3.8 0 1.1l-1.3 1.3c-.1.1-.3.2-.5.2z"></path>
                                                        <path
                                                            d="m75.3 56.5c-.4 0-.8-.4-.8-.8v-1.8c0-.4.4-.8.8-.8s.8.4.8.8v1.8c0 .5-.3.8-.8.8z"></path>
                                                        <path
                                                            d="m81 53.9c-.2 0-.4-.1-.6-.2l-1.3-1.3c-.3-.3-.3-.8 0-1.1s.8-.3 1.1 0l1.3 1.3c.3.3.3.8 0 1.1 0 .1-.2.2-.5.2z"></path>
                                                        <path
                                                            d="m83.2 48h-1.8c-.4 0-.8-.4-.8-.8s.4-.8.8-.8h1.8c.4 0 .8.4.8.8.1.5-.3.8-.8.8z"></path>
                                                        <path
                                                            d="m79.3 43.6c-.2 0-.4-.1-.6-.2-.3-.3-.3-.8 0-1.1l1.3-1.3c.3-.3.8-.3 1.1 0s.3.8 0 1.1l-1.3 1.3c0 .1-.2.2-.5.2z"></path>
                                                    </g>
                                                    <path
                                                        d="m71.8 66.3h76.3v19.3h-76.3z"
                                                        fill="#5ac2f3"></path>
                                                    <path
                                                        d="m129.8 76.2h-38.4c-.4 0-.8-.4-.8-.8s.4-.8.8-.8h38.4c.4 0 .8.4.8.8s-.4.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m71.8 94.7h21.2v19.3h-21.2z"
                                                        fill="#19a0ff"></path>
                                                    <path
                                                        d="m99.3 94.7h21.2v19.3h-21.2z"
                                                        fill="#19a0ff"></path>
                                                    <path
                                                        d="m126.8 94.7h21.2v19.3h-21.2z"
                                                        fill="#19a0ff"></path>
                                                    <circle cx="82.4" cy="104.3"
                                                            fill="#377dff"
                                                            r="2.3"></circle>
                                                    <circle cx="109.9" cy="104.3"
                                                            fill="#377dff"
                                                            r="2.3"></circle>
                                                    <circle cx="137.4" cy="104.3"
                                                            fill="#377dff"
                                                            r="2.3"></circle>
                                                    <path class="opal-svg-spinner"
                                                          d="m60.9 53.2v-1.6h-1.8c0-.3-.1-.5-.2-.8l1.7-.7-.6-1.3-1.7.7c-.1-.2-.3-.5-.4-.7l1.3-1.3-1.1-1.1-1.3 1.3c-.2-.2-.4-.3-.7-.4l.7-1.7-1.4-.6-.7 1.7c-.3-.1-.5-.1-.8-.2v-1.8h-1.6v1.8c-.3 0-.5.1-.8.2l-.6-1.7-1.4.6.7 1.7c-.2.1-.5.3-.7.4l-1.3-1.3-1.1 1.1 1.3 1.3c-.2.2-.3.4-.4.7l-1.7-.7-.6 1.4 1.7.7c-.1.3-.1.5-.2.8h-1.8v1.6h1.8c0 .3.1.5.2.8l-1.7.7.6 1.4 1.7-.7c.1.2.3.5.4.7l-1.3 1.3 1.1 1.1 1.3-1.3c.2.2.4.3.7.4l-.7 1.7 1.4.6.7-1.7c.3.1.5.1.8.2v1.8h1.6v-1.8c.3 0 .5-.1.8-.2l.7 1.7 1.4-.6-.7-1.7c.2-.1.5-.3.7-.4l1.3 1.3 1.1-1.1-1.3-1.4c.2-.2.3-.4.4-.7l1.7.7.6-1.4-1.7-.7c.1-.3.1-.5.2-.8zm-3.8-2.9c.2.4.1.9-.3 1.1s-.9.1-1.1-.3-.1-.9.3-1.1c.3-.3.8-.1 1.1.3zm-3.9-2.3c.5 0 .8.4.8.8 0 .5-.4.8-.8.8-.5 0-.8-.4-.8-.8-.1-.4.3-.8.8-.8zm-2.8 2c.4.2.5.7.3 1.1s-.7.5-1.1.3-.5-.7-.3-1.1.7-.6 1.1-.3zm-1.1 4.8c-.2-.4-.1-.9.3-1.1s.9-.1 1.1.3.1.9-.3 1.1-.9.1-1.1-.3zm3.9 2.2c-.5 0-.8-.4-.8-.8 0-.5.4-.8.8-.8.5 0 .8.4.8.8 0 .5-.4.8-.8.8zm0-2.9c-.9 0-1.6-.7-1.6-1.6s.7-1.6 1.6-1.6 1.6.7 1.6 1.6c-.1.9-.8 1.6-1.6 1.6zm2.7 1c-.4-.2-.5-.7-.3-1.1s.7-.5 1.1-.3.5.7.3 1.1-.7.5-1.1.3z"
                                                          fill="#5ac2f3"></path>
                                                    <path d="m26.6 96.3h16v16h-16z"
                                                          fill="#19a0ff"></path>
                                                    <path
                                                        d="m39.5 114.8c-.3 0-.5-.2-.5-.5v-9.9c0-.3.2-.5.5-.5s.5.2.5.5v9.9c0 .2-.2.5-.5.5z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m44.5 109.8h-9.9c-.3 0-.5-.2-.5-.5s.2-.5.5-.5h9.9c.3 0 .5.2.5.5s-.3.5-.5.5z"
                                                        fill="#377dff"></path>
                                                    <path class="opal-svg-spinner"
                                                          d="m218 93.4c-.2-.8.2-1.7 1.1-1.9.8-.2 1.7.2 1.9 1.1.2.8-.2 1.7-1.1 1.9-.1 0-.3.1-.4.1-.7-.1-1.3-.6-1.5-1.2zm-5.9.7c-.8-.3-1.2-1.2-.9-2 .3-.8 1.2-1.2 2-.9.8.3 1.2 1.2.9 2-.2.6-.8 1-1.5 1-.2 0-.4 0-.5-.1zm12.1-3.9c-.7-.5-.8-1.5-.3-2.2.5-.7 1.5-.8 2.2-.3.7.5.8 1.5.3 2.2-.3.4-.8.6-1.2.6-.4 0-.7-.1-1-.3zm-18.3-1c-.5-.7-.3-1.7.5-2.1.7-.5 1.7-.3 2.1.5.5.7.3 1.7-.5 2.1-.3.2-.6.2-.8.2-.5 0-1-.2-1.3-.7zm19.8-6.7c0-.9.7-1.6 1.6-1.6.9 0 1.6.7 1.6 1.6 0 .9-.7 1.6-1.6 1.6-.9-.1-1.6-.8-1.6-1.6zm-20.3.7c-.9-.1-1.5-.8-1.4-1.7.1-.9.8-1.5 1.7-1.4.9.1 1.5.8 1.4 1.7-.1.8-.7 1.4-1.5 1.4-.1 0-.1 0-.2 0zm19.2-5.2c-.4-.8-.1-1.7.6-2.1.8-.4 1.7-.1 2.1.6.4.8.1 1.7-.6 2.1-.2.1-.5.2-.7.2-.6 0-1.1-.3-1.4-.8zm-17.4-1.5c-.6-.6-.7-1.5-.2-2.2.6-.6 1.5-.7 2.2-.2.6.6.7 1.5.2 2.2-.3.4-.7.5-1.2.5-.4.1-.7 0-1-.3zm13.1-2.5c-.8-.4-1.1-1.3-.8-2.1.4-.8 1.3-1.1 2.1-.8.8.4 1.1 1.3.8 2.1-.3.6-.8.9-1.4.9-.3 0-.5 0-.7-.1zm-7.7-1.8c-.2-.8.4-1.7 1.2-1.8.8-.2 1.7.4 1.8 1.2.2.8-.4 1.7-1.2 1.8-.1 0-.2 0-.3 0-.7 0-1.4-.5-1.5-1.2z"
                                                          fill="#377dff"></path>
                                                </svg>
                                            </figure>
                                        </div>
                                        <div class="elementor-image-box-content"><h3
                                                class="elementor-image-box-title">
                                                Discover</h3>
                                            <p class="elementor-image-box-description">
                                                We strive to figure out ways to help
                                                your business grow through all
                                                platforms.</p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="elementor-element elementor-element-f0cd066 animated-fast elementor-invisible elementor-column elementor-col-33 elementor-top-column"
                    data-id="f0cd066" data-element_type="column"
                    data-settings="{&quot;animation&quot;:&quot;opal-move-up&quot;,&quot;animation_delay&quot;:300}">
                    <div class="elementor-column-wrap  elementor-element-populated">
                        <div class="elementor-widget-wrap">
                            <div
                                class="elementor-element elementor-element-2cfaa9a elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-image-box"
                                data-id="2cfaa9a" data-element_type="widget"
                                data-widget_type="image-box.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-image-box-wrapper">
                                        <div class="elementor-image-framed">
                                            <figure class="elementor-image-box-img">
                                                <svg enable-background="new 0 0 250 120"
                                                     viewBox="0 0 250 120"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="m133.8 97c0 1.6-1.3 2.9-2.9 2.9h-100.6c-1.6 0-2.9-1.3-2.9-2.9v-68.9c0-1.6 1.3-2.9 2.9-2.9h100.7c1.6 0 2.9 1.3 2.9 2.9v68.9z"
                                                        fill="#5ac2f3"></path>
                                                    <path
                                                        d="m130.9 100.6h-100.6c-2 0-3.6-1.6-3.6-3.6v-68.9c0-2 1.6-3.6 3.6-3.6h100.7c2 0 3.6 1.6 3.6 3.6v68.9c-.1 2-1.7 3.6-3.7 3.6zm-100.6-74.6c-1.2 0-2.2 1-2.2 2.2v68.8c0 1.2 1 2.2 2.2 2.2h100.7c1.2 0 2.2-1 2.2-2.2v-68.9c0-1.2-1-2.2-2.2-2.2h-100.7z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m33.1 30.5h94.9v57.1h-94.9z"
                                                        fill="#fff"></path>
                                                    <path
                                                        d="m128.1 88.3h-95c-.4 0-.7-.3-.7-.7v-57.1c0-.4.3-.7.7-.7h94.9c.4 0 .7.3.7.7v57.1c.1.4-.2.7-.6.7zm-94.2-1.4h93.5v-55.6h-93.5z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m93.5 114.2h-25.8l1.8-14.3h22.3z"
                                                        fill="#fff"></path>
                                                    <path
                                                        d="m93.5 115h-25.8c-.2 0-.4-.1-.5-.2-.1-.2-.2-.4-.2-.6l1.8-14.3c0-.4.3-.6.7-.6h22.3c.4 0 .7.3.7.6l1.8 14.2v.2c-.1.3-.4.7-.8.7zm-25-1.5h24.2l-1.6-12.9h-21z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m106 119.2h-50.8c-.4 0-.7-.3-.7-.7v-4.2c0-.4.3-.7.7-.7h50.8c.4 0 .7.3.7.7v4.2c0 .4-.3.7-.7.7zm-50.1-1.4h49.4v-2.8h-49.4z"
                                                        fill="#377dff"></path>
                                                    <circle cx="80.6" cy="93.8"
                                                            fill="#377dff"
                                                            r="1.7"></circle>
                                                    <path
                                                        d="m33.1 30.5h94.9v57.1h-94.9z"
                                                        fill="#fff"></path>
                                                    <path
                                                        d="m128.1 88.3h-95c-.4 0-.7-.3-.7-.7v-57.1c0-.4.3-.7.7-.7h94.9c.4 0 .7.3.7.7v57.1c.1.4-.2.7-.6.7zm-94.2-1.4h93.5v-55.6h-93.5z"
                                                        fill="#377dff"></path>
                                                    <path d="m33.1 30.5h94.9v7h-94.9z"
                                                          fill="#fff"></path>
                                                    <path
                                                        d="m128.1 38.3h-95c-.4 0-.7-.3-.7-.7v-7c0-.4.3-.7.7-.7h94.9c.4 0 .7.3.7.7v7c.1.4-.2.7-.6.7zm-94.2-1.5h93.5v-5.6h-93.5z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m122.9 34.8h-64.8c-.4 0-.7-.3-.7-.7s.3-.7.7-.7h64.7c.4 0 .7.3.7.7.1.3-.3.7-.6.7z"
                                                        fill="#377dff"></path>
                                                    <path d="m33.1 30.5h19.6v7h-19.6z"
                                                          fill="#fff"></path>
                                                    <path
                                                        d="m52.7 38.3h-19.6c-.4 0-.7-.3-.7-.7v-7c0-.4.3-.7.7-.7h19.6c.4 0 .7.3.7.7v7c0 .4-.3.7-.7.7zm-18.8-1.5h18.1v-5.6h-18.1z"
                                                        fill="#377dff"></path>
                                                    <path d="m37.6 33.1h1.8v1.8h-1.8z"
                                                          fill="#5ac2f3"></path>
                                                    <path d="m42.1 33.1h1.8v1.8h-1.8z"
                                                          fill="#19a0ff"></path>
                                                    <path d="m46.5 33.1h1.8v1.8h-1.8z"
                                                          fill="#5ac2f3"></path>
                                                    <path
                                                        d="m39.5 43.9h22.7v22.7h-22.7z"
                                                        fill="#19a0ff"></path>
                                                    <path
                                                        d="m92 67.3h-2.1c-.4 0-.7-.3-.7-.7s.3-.7.7-.7h1.4v-1.4c0-.4.3-.7.7-.7s.7.3.7.7v2.1c0 .4-.3.7-.7.7z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m86.1 67.3h-3.7c-.4 0-.7-.3-.7-.7s.3-.7.7-.7h3.7c.4 0 .7.3.7.7s-.3.7-.7.7zm-7.3 0h-3.7c-.4 0-.7-.3-.7-.7s.3-.7.7-.7h3.7c.4 0 .7.3.7.7s-.3.7-.7.7z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m71.4 67.3h-2.1c-.4 0-.7-.3-.7-.7v-2.1c0-.4.3-.7.7-.7s.7.3.7.7v1.4h1.4c.4 0 .7.3.7.7s-.3.7-.7.7z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m69.3 61.5c-.4 0-.7-.3-.7-.7v-3.7c0-.4.3-.7.7-.7s.7.3.7.7v3.7c0 .4-.3.7-.7.7zm0-7.3c-.4 0-.7-.3-.7-.7v-3.7c0-.4.3-.7.7-.7s.7.3.7.7v3.7c0 .3-.3.7-.7.7z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m69.3 46.8c-.4 0-.7-.3-.7-.7v-2.1c0-.4.3-.7.7-.7h2.1c.4 0 .7.3.7.7s-.3.7-.7.7h-1.4v1.4c0 .4-.3.7-.7.7z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m86.1 44.6h-3.7c-.4 0-.7-.3-.7-.7s.3-.7.7-.7h3.7c.4 0 .7.3.7.7s-.3.7-.7.7zm-7.3 0h-3.7c-.4 0-.7-.3-.7-.7s.3-.7.7-.7h3.7c.4 0 .7.3.7.7s-.3.7-.7.7z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m92 46.8c-.4 0-.7-.3-.7-.7v-1.4h-1.4c-.4 0-.7-.3-.7-.7s.3-.7.7-.7h2.1c.4 0 .7.3.7.7v2.1c0 .4-.3.7-.7.7z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m92 61.5c-.4 0-.7-.3-.7-.7v-3.7c0-.4.3-.7.7-.7s.7.3.7.7v3.7c0 .4-.3.7-.7.7zm0-7.3c-.4 0-.7-.3-.7-.7v-3.7c0-.4.3-.7.7-.7s.7.3.7.7v3.7c0 .3-.3.7-.7.7z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m100.2 43.9h22.7v22.7h-22.7z"
                                                        fill="#5ac2f3"></path>
                                                    <g fill="#377dff">
                                                        <path
                                                            d="m54.3 72.2h-8.8c-.4 0-.7-.3-.7-.7s.3-.7.7-.7h8.8c.4 0 .7.3.7.7s-.3.7-.7.7z"></path>
                                                        <path
                                                            d="m85 72.2h-8.8c-.4 0-.7-.3-.7-.7s.3-.7.7-.7h8.8c.4 0 .7.3.7.7s-.3.7-.7.7z"></path>
                                                        <path
                                                            d="m115.9 72.2h-8.8c-.4 0-.7-.3-.7-.7s.3-.7.7-.7h8.8c.4 0 .7.3.7.7s-.3.7-.7.7z"></path>
                                                        <path
                                                            d="m61.3 77.4h-23.7c-.4 0-.7-.3-.7-.7s.3-.7.7-.7h23.6c.4 0 .7.3.7.7.1.4-.2.7-.6.7z"></path>
                                                        <path
                                                            d="m61.3 80.6h-23.7c-.4 0-.7-.3-.7-.7s.3-.7.7-.7h23.6c.4 0 .7.3.7.7s-.2.7-.6.7z"></path>
                                                        <path
                                                            d="m61.3 83.8h-23.7c-.4 0-.7-.3-.7-.7s.3-.7.7-.7h23.6c.4 0 .7.3.7.7.1.4-.2.7-.6.7z"></path>
                                                        <path
                                                            d="m92.5 77.4h-23.6c-.4 0-.7-.3-.7-.7s.3-.7.7-.7h23.6c.4 0 .7.3.7.7s-.3.7-.7.7z"></path>
                                                        <path
                                                            d="m92.5 80.6h-23.6c-.4 0-.7-.3-.7-.7s.3-.7.7-.7h23.6c.4 0 .7.3.7.7s-.3.7-.7.7z"></path>
                                                        <path
                                                            d="m92.5 83.8h-23.6c-.4 0-.7-.3-.7-.7s.3-.7.7-.7h23.6c.4 0 .7.3.7.7s-.3.7-.7.7z"></path>
                                                        <path
                                                            d="m123.3 77.4h-23.6c-.4 0-.7-.3-.7-.7s.3-.7.7-.7h23.6c.4 0 .7.3.7.7s-.3.7-.7.7z"></path>
                                                        <path
                                                            d="m123.3 80.6h-23.6c-.4 0-.7-.3-.7-.7s.3-.7.7-.7h23.6c.4 0 .7.3.7.7s-.3.7-.7.7z"></path>
                                                        <path
                                                            d="m123.3 83.8h-23.6c-.4 0-.7-.3-.7-.7s.3-.7.7-.7h23.6c.4 0 .7.3.7.7s-.3.7-.7.7z"></path>
                                                        <circle cx="80.6" cy="55.3"
                                                                r="1"></circle>
                                                        <path
                                                            d="m19.9 118.4h205.3v1.5h-205.3z"></path>
                                                        <path
                                                            d="m12.9 118.4h4.8v1.5h-4.8z"></path>
                                                        <path
                                                            d="m227.7 118.4h9.4v1.5h-9.4z"></path>
                                                    </g>
                                                    <path
                                                        d="m162.1 45.6h-40.6c-2.8 0-5.1 2.3-5.1 5.1v63.5c0 2.8 2.3 5.1 5.1 5.1h40.6c2.8 0 5.1-2.3 5.1-5.1v-63.6c0-2.8-2.3-5-5.1-5zm.3 64.3h-41.1v-57.6h41.1z"
                                                        fill="#19a0ff"></path>
                                                    <path
                                                        d="m162.1 120h-40.6c-3.2 0-5.8-2.6-5.8-5.8v-63.6c0-3.2 2.6-5.8 5.8-5.8h40.6c3.2 0 5.8 2.6 5.8 5.8v63.5c.1 3.3-2.5 5.9-5.8 5.9zm-40.6-73.6c-2.4 0-4.3 1.9-4.3 4.3v63.5c0 2.4 1.9 4.3 4.3 4.3h40.6c2.4 0 4.3-1.9 4.3-4.3v-63.6c0-2.4-1.9-4.3-4.3-4.3h-40.6zm40.9 64.2h-41.1c-.4 0-.8-.3-.8-.8v-57.5c0-.4.3-.8.8-.8h41.1c.4 0 .8.3.8.8v57.5c-.1.5-.4.8-.8.8zm-40.3-1.5h39.5v-56h-39.5z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m121.3 52.3h41.1v57.5h-41.1z"
                                                        fill="#fff"></path>
                                                    <path
                                                        d="m162.4 110.6h-41.1c-.4 0-.8-.3-.8-.8v-57.5c0-.4.3-.8.8-.8h41.1c.4 0 .8.3.8.8v57.5c-.1.5-.4.8-.8.8zm-40.3-1.5h39.5v-56h-39.5z"
                                                        fill="#377dff"></path>
                                                    <circle cx="141.8" cy="114.6"
                                                            fill="#377dff"
                                                            r="1.6"></circle>
                                                    <path
                                                        d="m154.5 94h-25.4c-.4 0-.8-.3-.8-.8s.3-.8.8-.8h25.4c.4 0 .8.3.8.8s-.4.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m154.5 97.5h-25.4c-.4 0-.8-.3-.8-.8s.3-.8.8-.8h25.4c.4 0 .8.3.8.8s-.4.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m154.5 101h-25.4c-.4 0-.8-.3-.8-.8s.3-.8.8-.8h25.4c.4 0 .8.3.8.8s-.4.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m147.2 49.8h-10.2c-.4 0-.8-.3-.8-.8 0-.4.3-.8.8-.8h10.2c.4 0 .8.3.8.8s-.4.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <circle cx="142.1" cy="70.3"
                                                            fill="#377dff"
                                                            r="1.1"></circle>
                                                    <path
                                                        d="m154.3 83.2h-2.3c-.4 0-.8-.3-.8-.8s.3-.8.8-.8h1.5v-1.5c0-.4.3-.8.8-.8.4 0 .8.3.8.8v2.3c0 .5-.3.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m148.1 83.2h-4c-.4 0-.8-.3-.8-.8s.3-.8.8-.8h4c.4 0 .8.3.8.8s-.4.8-.8.8zm-7.9 0h-4c-.4 0-.8-.3-.8-.8s.3-.8.8-.8h4c.4 0 .8.3.8.8s-.4.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m132.2 83.2h-2.3c-.4 0-.8-.3-.8-.8v-2.3c0-.4.3-.8.8-.8.4 0 .8.3.8.8v1.5h1.5c.4 0 .8.3.8.8s-.3.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m129.9 77c-.4 0-.8-.3-.8-.8v-4c0-.4.3-.8.8-.8.4 0 .8.3.8.8v4c0 .4-.3.8-.8.8zm0-7.9c-.4 0-.8-.3-.8-.8v-4c0-.4.3-.8.8-.8.4 0 .8.3.8.8v4c0 .4-.3.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m129.9 61.1c-.4 0-.8-.3-.8-.8v-2.3c0-.4.3-.8.8-.8h2.3c.4 0 .8.3.8.8s-.3.8-.8.8h-1.5v1.5c0 .5-.3.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m148.1 58.8h-4c-.4 0-.8-.3-.8-.8s.3-.8.8-.8h4c.4 0 .8.3.8.8s-.4.8-.8.8zm-7.9 0h-4c-.4 0-.8-.3-.8-.8s.3-.8.8-.8h4c.4 0 .8.3.8.8s-.4.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m154.3 61.1c-.4 0-.8-.3-.8-.8v-1.5h-1.5c-.4 0-.8-.3-.8-.8s.3-.8.8-.8h2.3c.4 0 .8.3.8.8v2.3c0 .5-.3.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <path
                                                        d="m154.3 77c-.4 0-.8-.3-.8-.8v-4c0-.4.3-.8.8-.8.4 0 .8.3.8.8v4c0 .4-.3.8-.8.8zm0-7.9c-.4 0-.8-.3-.8-.8v-4c0-.4.3-.8.8-.8.4 0 .8.3.8.8v4c0 .4-.3.8-.8.8z"
                                                        fill="#377dff"></path>
                                                    <g class="opal-svg-spinner">
                                                        <path
                                                            d="m183.5 18.6c-.3-.5-.7-.9-1.1-1.3l2.9-3.4-4.2-3.1-2.9 3.4c-.9-.4-1.9-.8-3-1l.2-4.3-5.4-.1-.2 4.4c-1 .2-2 .4-3 .8l-2.6-3.6-4.5 2.8 2.6 3.6c-.8.7-1.4 1.5-1.9 2.3l-4.4-1.5-1.9 4.7 4.4 1.5c-.2 1-.2 1.9-.1 2.9l-4.5 1.2 1.5 4.8 4.4-1.2c.3.5.5 1 .8 1.5.3.4.6.7.9 1l-2.8 3.3 4.2 3.1 2.8-3.3c1 .5 2 .8 3.1 1.1l-.2 4.1 5.4.2.2-4.1c1.1-.2 2.2-.4 3.2-.8l2.4 3.4 4.5-2.8-2.4-3.4c.8-.7 1.5-1.6 2-2.5l4.1 1.4 1.9-4.7-4.2-1.4c.2-1 .2-2 .1-3l4.3-1.1-1.5-4.8-4.4 1.2c-.2-.5-.4-.9-.7-1.3zm-8.9 10.6c-2.1 1.3-4.9.8-6.3-1.2-1.4-1.9-.8-4.6 1.3-5.8 2.1-1.3 4.9-.8 6.3 1.2 1.3 1.9.8 4.5-1.3 5.8z"
                                                            fill="#fff"></path>
                                                        <path
                                                            d="m174 42.9c-.1 0-.1 0 0 0l-5.4-.2c-.2 0-.3-.1-.4-.2s-.2-.3-.2-.4l.2-3.6c-.7-.2-1.5-.5-2.2-.8l-2.5 2.9c-.2.3-.6.3-.9.1l-4.2-3.1c-.1-.1-.2-.2-.2-.4s0-.3.1-.4l2.5-2.9c-.2-.2-.4-.5-.6-.7-.3-.4-.5-.7-.6-1.1l-3.9 1c-.3.1-.7-.1-.8-.4l-1.5-4.8c0-.2 0-.3.1-.4s.2-.2.4-.3l4-1c0-.7 0-1.4.1-2.1l-3.9-1.3c-.3-.1-.5-.5-.4-.8l1.9-4.7c.1-.1.2-.3.3-.3.2-.1.3-.1.5 0l3.9 1.3c.4-.6.9-1.1 1.4-1.6l-2.3-3.2c-.1-.1-.1-.3-.1-.4 0-.2.1-.3.3-.4l4.5-2.8c.3-.2.7-.1.9.2l2.3 3.2c.7-.2 1.4-.4 2.2-.6l.2-3.9c0-.3.3-.6.7-.6l5.4.2c.2 0 .3.1.4.2s.2.3.2.4l-.2 3.9c.7.2 1.4.4 2.1.7l2.6-3c.2-.3.6-.3.9-.1l4.2 3.1c.1.1.2.2.2.4s0 .3-.1.4l-2.5 3c.3.3.6.6.8 1 .2.3.3.5.5.8l3.9-1c.3-.1.7.1.8.4l1.5 4.8c0 .2 0 .3-.1.4s-.2.2-.4.3l-3.8 1c0 .8 0 1.5-.1 2.2l3.7 1.3c.2.1.3.2.4.3s.1.3 0 .5l-1.9 4.7c-.1.3-.5.5-.8.4l-3.6-1.2c-.5.7-1 1.3-1.5 1.8l2.1 3c.2.3.1.6-.2.8l-4.5 2.8c-.3.2-.7.1-.9-.2l-2.1-3c-.7.2-1.5.4-2.3.6l-.1 3.6c-.4 0-.7.2-1 .2zm-4.8-1.3 4.1.1.1-3.5c0-.3.2-.5.5-.6 1.1-.2 2.1-.4 3.1-.8.3-.1.6 0 .8.2l2.1 2.9 3.4-2.1-2.1-2.9c-.2-.2-.1-.5.1-.7.7-.7 1.3-1.4 1.9-2.3.2-.2.5-.3.8-.3l3.5 1.2 1.4-3.6-3.6-1.2c-.3-.1-.5-.4-.4-.6.2-.9.2-1.9.1-2.9 0-.3.2-.5.5-.6l3.7-1-1.1-3.6-3.8 1c-.3.1-.6 0-.7-.3-.1-.1-.1-.3-.2-.4-.1-.3-.3-.5-.4-.7-.2-.3-.5-.6-.8-.9-.1-.1-.2-.2-.2-.3-.2-.2-.2-.5 0-.8l2.5-2.9-3.2-2.3-2.5 2.9c-.2.2-.5.3-.8.2-1-.4-1.9-.8-2.8-1-.3-.1-.5-.3-.5-.6l.2-3.8-4.1-.1-.2 3.8c0 .3-.2.5-.5.6-1.1.2-2 .4-2.9.7-.3.1-.6 0-.8-.2l-2.3-3.1-3.4 2.1 2.3 3.1c.2.2.1.5-.1.7-.7.6-1.3 1.3-1.8 2.2-.2.2-.5.3-.8.2l-3.8-1.3-1.4 3.6 3.8 1.3c.3.1.5.4.4.7-.2.9-.2 1.8-.1 2.8 0 .3-.2.5-.5.6l-3.9 1 1.1 3.6 3.8-1c.3-.1.6.1.7.3l.1.3c.2.4.4.8.7 1.1.2.2.4.4.6.6.1.1.2.2.3.3.2.2.2.5 0 .7l-2.4 2.9 3.2 2.3 2.4-2.8c.2-.2.5-.3.8-.2 1 .5 1.9.8 2.9 1 .3.1.5.3.5.6zm2.9-11.1c-1.7 0-3.4-.8-4.3-2.1-1.6-2.2-1-5.2 1.4-6.7.9-.5 1.9-.8 2.9-.8 1.7 0 3.4.8 4.3 2.1.8 1.1 1 2.3.8 3.6-.3 1.3-1.1 2.3-2.2 3.1-.9.5-1.9.8-2.9.8zm0-8.4c-.8 0-1.5.2-2.2.6-1.8 1.1-2.3 3.4-1.1 5 .7 1 1.9 1.6 3.3 1.6.8 0 1.5-.2 2.2-.6.9-.5 1.5-1.4 1.7-2.3s0-1.9-.6-2.7c-.8-1-2-1.6-3.3-1.6z"
                                                            fill="#377dff"></path>
                                                    </g>
                                                    <g class="opal-svg-spinner">
                                                        <path
                                                            d="m138.1 7.2c-.5 0-1-.4-1-1v-2.2c0-.5.4-1 1-1s1 .4 1 1v2.2c0 .5-.5 1-1 1z"
                                                            fill="#377dff"></path>
                                                        <path
                                                            d="m138.1 7.2c-.5 0-1-.4-1-1v-2.2c0-.5.4-1 1-1s1 .4 1 1v2.2c0 .5-.5 1-1 1z"
                                                            fill="#377dff"></path>
                                                        <path
                                                            d="m132.8 9.7c-.3 0-.5-.1-.7-.3l-1.6-1.6c-.4-.4-.4-1 0-1.4s1-.4 1.4 0l1.6 1.6c.4.4.4 1 0 1.4-.2.2-.5.3-.7.3z"
                                                            fill="#377dff"></path>
                                                        <path
                                                            d="m130.7 15.2h-2.2c-.5 0-1-.4-1-1s.4-1 1-1h2.2c.5 0 1 .4 1 1s-.4 1-1 1z"
                                                            fill="#377dff"></path>
                                                        <path
                                                            d="m131.7 22.1c-.3 0-.5-.1-.7-.3-.4-.4-.4-1 0-1.4l1.6-1.6c.4-.4 1-.4 1.4 0s.4 1 0 1.4l-1.6 1.6c-.2.2-.5.3-.7.3z"
                                                            fill="#377dff"></path>
                                                        <path
                                                            d="m138.8 24.7c-.5 0-1-.4-1-1v-2.2c0-.5.4-1 1-1 .5 0 1 .4 1 1v2.2c0 .6-.5 1-1 1z"
                                                            fill="#377dff"></path>
                                                        <path
                                                            d="m145.7 21.6c-.3 0-.5-.1-.7-.3l-1.6-1.6c-.4-.4-.4-1 0-1.4s1-.4 1.4 0l1.6 1.6c.4.4.4 1 0 1.4-.2.2-.5.3-.7.3z"
                                                            fill="#377dff"></path>
                                                        <path
                                                            d="m148.3 14.5h-2.2c-.5 0-1-.4-1-1s.4-1 1-1h2.2c.5 0 1 .4 1 1s-.4 1-1 1z"
                                                            fill="#377dff"></path>
                                                        <path
                                                            d="m143.6 9.2c-.3 0-.5-.1-.7-.3-.4-.4-.4-1 0-1.4l1.6-1.6c.4-.4 1-.4 1.4 0s.4 1 0 1.4l-1.6 1.6c-.2.2-.4.3-.7.3z"
                                                            fill="#377dff"></path>
                                                    </g>
                                                    <path
                                                        d="m173.6 104.6c-.3-.3-.3-.7-.1-1l7.1-10c-3.6-4.1-5.6-9.3-5.6-14.7 0-12.4 10.1-22.4 22.4-22.4 12.4 0 22.4 10.1 22.4 22.4 0 12.4-10.1 22.4-22.4 22.4-4.4 0-8.7-1.3-12.5-3.8l-10.5 7.2c-.1.1-.3.1-.5.1.1 0-.1-.1-.3-.2zm11.9-8.8c3.6 2.5 7.7 3.8 12 3.8 11.5 0 20.8-9.3 20.8-20.8s-9.3-20.8-20.8-20.8-20.8 9.3-20.8 20.8c0 5.2 2 10.2 5.6 14.1.3.3.3.7.1 1l-4.9 6.8 7.1-4.9c.1-.1.3-.1.5-.1.1 0 .3 0 .4.1z"
                                                        fill="#377dff"></path>
                                                    <circle cx="197.3" cy="78.8"
                                                            fill="#5ac2f3"
                                                            r="14.2"></circle>
                                                    <path d="m195.4 77.8h3.7v8.6h-3.7z"
                                                          fill="#fff"></path>
                                                    <circle cx="197.3" cy="73.2"
                                                            fill="#fff"
                                                            r="1.9"></circle>
                                                    <path class="opal-svg-spinner"
                                                          d="m201.5 37.2c-.2-.3-.5-.5-.7-.7l1.4-2.3-2.6-1.6-1.4 2.3c-.6-.2-1.2-.3-1.8-.4l-.2-2.7-3.1.3.2 2.7c-.6.2-1.1.4-1.6.7l-1.8-2-2.3 2 1.8 2c-.4.5-.7 1-.9 1.5l-2.6-.6-.7 3 2.6.6c0 .6 0 1.2.2 1.8l-2.5 1 1.2 2.8 2.4-1c.2.3.4.6.6.8s.4.4.6.6l-1.3 2.2 2.6 1.6 1.3-2.2c.6.2 1.2.4 1.8.4l.2 2.5 3.1-.3-.2-2.5c.6-.2 1.2-.4 1.8-.7l1.6 1.9 2.3-2-1.6-1.9c.4-.5.7-1.1 1-1.6l2.5.6.7-3-2.5-.6c0-.6-.1-1.2-.2-1.9l2.4-1-1.2-2.8-2.4 1c-.3-.1-.5-.3-.7-.5zm-3 8.5c-1.9 1.6-4.7 1.4-6.4-.4-1.6-1.9-1.4-4.7.4-6.4 1.9-1.6 4.7-1.4 6.4.4 1.7 1.9 1.5 4.7-.4 6.4z"
                                                          fill="#5ac2f3"></path>
                                                    <circle cx="195.5" cy="42.3"
                                                            fill="#377dff"
                                                            r="1.1"></circle>
                                                </svg>
                                            </figure>
                                        </div>
                                        <div class="elementor-image-box-content"><h3
                                                class="elementor-image-box-title">
                                                Explore</h3>
                                            <p class="elementor-image-box-description">
                                                Find what you need in one template and
                                                combine features at will.</p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div
        class="elementor-element elementor-element-42082a8 animated-fast elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-invisible elementor-section elementor-top-section"
        data-id="42082a8" data-element_type="section"
        data-settings="{&quot;animation&quot;:&quot;opal-move-up&quot;}">
        <div class="elementor-container elementor-column-gap-no">
            <div class="elementor-row">
                <div
                    class="elementor-element elementor-element-23df1dc elementor-column elementor-col-100 elementor-top-column"
                    data-id="23df1dc" data-element_type="column">
                    <div class="elementor-column-wrap  elementor-element-populated">
                        <div class="elementor-widget-wrap">
                            <div
                                class="elementor-element elementor-element-085d04d elementor-widget elementor-widget-divider"
                                data-id="085d04d" data-element_type="widget"
                                data-widget_type="divider.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-divider"><span
                                            class="elementor-divider-separator"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div
        class="elementor-element elementor-element-75eda22 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
        data-id="75eda22" data-element_type="section">
        <div class="elementor-container elementor-column-gap-no">
            <div class="elementor-row">
                <div
                    class="elementor-element elementor-element-1088eac elementor-column elementor-col-50 elementor-top-column"
                    data-id="1088eac" data-element_type="column">
                    <div class="elementor-column-wrap  elementor-element-populated">
                        <div class="elementor-widget-wrap">
                            <div
                                class="elementor-element elementor-element-e1c8a85 animated-fast elementor-mobile-align-center elementor-invisible elementor-widget elementor-widget-heading"
                                data-id="e1c8a85" data-element_type="widget"
                                data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;}"
                                data-widget_type="heading.default">
                                <div class="elementor-widget-container"><span
                                        class="sub-title">What We Do</span>
                                    <h2 class="elementor-heading-title elementor-size-default">
                                        Build for everyone,<br> enjoyable usage</h2>
                                </div>
                            </div>
                            <div
                                class="elementor-element elementor-element-1da6c8b animated-fast elementor-invisible elementor-widget elementor-widget-text-editor"
                                data-id="1da6c8b" data-element_type="widget"
                                data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;}"
                                data-widget_type="text-editor.default">
                                <div class="elementor-widget-container">
                                    <div
                                        class="elementor-text-editor elementor-clearfix">
                                        <p>Experience a level of our quality in both
                                            design &amp; customization works.</p></div>
                                </div>
                            </div>
                            <div
                                class="elementor-element elementor-element-bc10ba4 elementor-mobile-align-center animated-fast elementor-invisible elementor-widget elementor-widget-heading"
                                data-id="bc10ba4" data-element_type="widget"
                                data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;}"
                                data-widget_type="heading.default">
                                <div class="elementor-widget-container"><h3
                                        class="elementor-heading-title elementor-size-default">
                                        Startor is for you.</h3></div>
                            </div>
                            <div
                                class="elementor-element elementor-element-6d7a43a animated-fast elementor-invisible elementor-widget elementor-widget-text-editor"
                                data-id="6d7a43a" data-element_type="widget"
                                data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;}"
                                data-widget_type="text-editor.default">
                                <div class="elementor-widget-container">
                                    <div
                                        class="elementor-text-editor elementor-clearfix">
                                        Whether you&#8217;re a company interested in
                                        bolstering your culture and philanthropic
                                        footprint, or simply someone who wants to join a
                                        community of kindness.
                                    </div>
                                </div>
                            </div>
                            <div
                                class="elementor-element elementor-element-151f6fc elementor-mobile-align-center animated-fast elementor-button-primary elementor-invisible elementor-widget elementor-widget-button"
                                data-id="151f6fc" data-element_type="widget"
                                data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;}"
                                data-widget_type="button.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-button-wrapper"><a href="#"
                                                                             class="elementor-button-link elementor-button elementor-size-md"
                                                                             role="button">
                                                                            <span
                                                                                class="elementor-button-content-wrapper"> <span
                                                                                    class="elementor-button-text">Get Started</span> </span>
                                        </a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="elementor-element elementor-element-50ddd99 animated-fast elementor-invisible elementor-column elementor-col-50 elementor-top-column"
                    data-id="50ddd99" data-element_type="column"
                    data-settings="{&quot;animation&quot;:&quot;opal-move-up&quot;}">
                    <div class="elementor-column-wrap  elementor-element-populated">
                        <div class="elementor-widget-wrap">
                            <div
                                class="elementor-element elementor-element-172498b elementor-widget elementor-widget-opal-video-popup"
                                data-id="172498b" data-element_type="widget"
                                data-widget_type="opal-video-popup.default">
                                <div class="elementor-widget-container">
                                    <div
                                        class="elementor-video-wrapper opal-video-popup">
                                        <a class="elementor-video-popup" role="button"
                                           href="#" data-effect="mfp-zoom-in"> <span
                                                class="elementor-video-icon"><i
                                                    class="opal-icon-play"></i></span>
                                        </a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div
        class="elementor-element elementor-element-d99e8ad elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
        data-id="d99e8ad" data-element_type="section"
        data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-container elementor-column-gap-no">
            <div class="elementor-row">
                <div
                    class="elementor-element elementor-element-2063b06 elementor-column elementor-col-100 elementor-top-column"
                    data-id="2063b06" data-element_type="column">
                    <div class="elementor-column-wrap  elementor-element-populated">
                        <div class="elementor-widget-wrap">
                            <div
                                class="elementor-element elementor-element-bbc3b88 elementor-align-center animated-fast elementor-invisible elementor-widget elementor-widget-heading"
                                data-id="bbc3b88" data-element_type="widget"
                                data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;}"
                                data-widget_type="heading.default">
                                <div class="elementor-widget-container"><h2
                                        class="elementor-heading-title elementor-size-default">
                                        Trusted by 100,000+ user</h2></div>
                            </div>
                            <div
                                class="elementor-element elementor-element-a8eec46 animated-fast elementor-invisible elementor-widget elementor-widget-opal-brand"
                                data-id="a8eec46" data-element_type="widget"
                                data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;}"
                                data-widget_type="opal-brand.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-brands">
                                        <div class="elementor-brand-wrapper">
                                            <div class="row owl-carousel owl-theme"
                                                 data-settings="{&quot;navigation&quot;:&quot;none&quot;,&quot;autoplayHoverPause&quot;:true,&quot;autoplay&quot;:true,&quot;autoplayTimeout&quot;:5000,&quot;items&quot;:&quot;6&quot;,&quot;items_tablet&quot;:&quot;3&quot;,&quot;items_mobile&quot;:&quot;2&quot;,&quot;loop&quot;:true,&quot;item_margin&quot;:20}"
                                                 data-elementor-columns="6"
                                                 data-elementor-columns-tablet="3"
                                                 data-elementor-columns-mobile="2">
                                                <div>
                                                    <div class="elementor-brand-image">
                                                        <a href="#" target="_blank"
                                                           title="Brand Name"><img
                                                                width="150" height="43"
                                                                src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/brand_01.png"
                                                                class="attachment-full size-full"
                                                                alt=""/></a></div>
                                                </div>
                                                <div>
                                                    <div class="elementor-brand-image">
                                                        <a href="#"
                                                           target="_blank _blank"
                                                           title="Brand Name"><img
                                                                width="150" height="43"
                                                                src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/brand_02.png"
                                                                class="attachment-full size-full"
                                                                alt=""/></a></div>
                                                </div>
                                                <div>
                                                    <div class="elementor-brand-image">
                                                        <a href="#"
                                                           target="_blank _blank _blank"
                                                           title="Brand Name"><img
                                                                width="150" height="43"
                                                                src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/brand_03.png"
                                                                class="attachment-full size-full"
                                                                alt=""/></a></div>
                                                </div>
                                                <div>
                                                    <div class="elementor-brand-image">
                                                        <a href="#"
                                                           target="_blank _blank _blank _blank"
                                                           title="Brand Name"><img
                                                                width="150" height="43"
                                                                src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/brand_04.png"
                                                                class="attachment-full size-full"
                                                                alt=""/></a></div>
                                                </div>
                                                <div>
                                                    <div class="elementor-brand-image">
                                                        <a href="#"
                                                           target="_blank _blank _blank _blank _blank"
                                                           title="Brand Name"><img
                                                                width="150" height="43"
                                                                src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/brand_05.png"
                                                                class="attachment-full size-full"
                                                                alt=""/></a></div>
                                                </div>
                                                <div>
                                                    <div class="elementor-brand-image">
                                                        <a href="#"
                                                           target="_blank _blank _blank _blank _blank _blank"
                                                           title="Brand Name"><img
                                                                width="150" height="43"
                                                                src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/brand_06.png"
                                                                class="attachment-full size-full"
                                                                alt=""/></a></div>
                                                </div>
                                                <div>
                                                    <div class="elementor-brand-image">
                                                        <a href="#"
                                                           target="_blank _blank _blank _blank _blank _blank _blank"
                                                           title="Brand Name"><img
                                                                width="150" height="43"
                                                                src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/brand_13.png"
                                                                class="attachment-full size-full"
                                                                alt=""/></a></div>
                                                </div>
                                                <div>
                                                    <div class="elementor-brand-image">
                                                        <a href="#"
                                                           target="_blank _blank _blank _blank _blank _blank _blank _blank"
                                                           title="Brand Name"><img
                                                                width="150" height="43"
                                                                src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/brand_01.png"
                                                                class="attachment-full size-full"
                                                                alt=""/></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div
                                class="elementor-element elementor-element-ea04c5f elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                data-id="ea04c5f" data-element_type="section">
                                <div
                                    class="elementor-container elementor-column-gap-no">
                                    <div class="elementor-row">
                                        <div
                                            class="elementor-element elementor-element-f38b3f6 elementor-column elementor-col-100 elementor-inner-column"
                                            data-id="f38b3f6"
                                            data-element_type="column">
                                            <div
                                                class="elementor-column-wrap  elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div
                                                        class="elementor-element elementor-element-21a4e47 content-box-move-down animated-fast elementor-testimonial-text-align-center elementor-invisible elementor-widget elementor-widget-opal-testimonials"
                                                        data-id="21a4e47"
                                                        data-element_type="widget"
                                                        data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;}"
                                                        data-widget_type="opal-testimonials.default">
                                                        <div
                                                            class="elementor-widget-container">
                                                            <div
                                                                class="elementor-testimonial-wrapper layout_2">
                                                                <div class="row"
                                                                     data-elementor-columns="3"
                                                                     data-elementor-columns-tablet="2"
                                                                     data-elementor-columns-mobile="1">
                                                                    <div
                                                                        class="elementor-testimonial-item column-item">
                                                                        <div
                                                                            class="item-box">
                                                                            <div
                                                                                class="elementor-testimonial-content-box">
                                                                                <div
                                                                                    class="elementor-testimonial-quote">
                                                                                    <i class="opal-icon-quote"
                                                                                       aria-hidden="true"></i>
                                                                                </div>
                                                                                <div
                                                                                    class="elementor-testimonial-content">
                                                                                    "I
                                                                                    would
                                                                                    be
                                                                                    lost
                                                                                    without
                                                                                    Startor.
                                                                                    I
                                                                                    could'nt
                                                                                    have
                                                                                    asked
                                                                                    for
                                                                                    more
                                                                                    than
                                                                                    this.
                                                                                    Thanks
                                                                                    for
                                                                                    the
                                                                                    great
                                                                                    service."
                                                                                </div>
                                                                                <div
                                                                                    class="elementor-testimonial-rating">
                                                                                    <i class="fa fa-star"
                                                                                       aria-hidden="true"></i>
                                                                                    <i class="fa fa-star"
                                                                                       aria-hidden="true"></i>
                                                                                    <i class="fa fa-star"
                                                                                       aria-hidden="true"></i>
                                                                                    <i class="fa fa-star"
                                                                                       aria-hidden="true"></i>
                                                                                    <i class="fa fa-star"
                                                                                       aria-hidden="true"></i>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-testimonial-image">
                                                                                <img
                                                                                    src="https://demo2.wpopal.com/startor/wp-content/uploads/elementor/thumbs/01w-o6vswikpg8147jtt4v1j5tbxo8o4fo7z8dxkigk4ha.jpg"
                                                                                    title="01w"
                                                                                    alt="01w"/>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-testimonial-details">
                                                                                <div
                                                                                    class="elementor-testimonial-name">
                                                                                    Lisa
                                                                                    Jones
                                                                                </div>
                                                                                <div
                                                                                    class="elementor-testimonial-job">
                                                                                    Marketing,
                                                                                    SEO
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        class="elementor-testimonial-item column-item">
                                                                        <div
                                                                            class="item-box">
                                                                            <div
                                                                                class="elementor-testimonial-content-box">
                                                                                <div
                                                                                    class="elementor-testimonial-quote">
                                                                                    <i class="opal-icon-quote"
                                                                                       aria-hidden="true"></i>
                                                                                </div>
                                                                                <div
                                                                                    class="elementor-testimonial-content">
                                                                                    "I
                                                                                    would
                                                                                    be
                                                                                    lost
                                                                                    without
                                                                                    Startor.
                                                                                    I
                                                                                    could'nt
                                                                                    have
                                                                                    asked
                                                                                    for
                                                                                    more
                                                                                    than
                                                                                    this.
                                                                                    Thanks
                                                                                    for
                                                                                    the
                                                                                    great
                                                                                    service."
                                                                                </div>
                                                                                <div
                                                                                    class="elementor-testimonial-rating">
                                                                                    <i class="fa fa-star"
                                                                                       aria-hidden="true"></i>
                                                                                    <i class="fa fa-star"
                                                                                       aria-hidden="true"></i>
                                                                                    <i class="fa fa-star"
                                                                                       aria-hidden="true"></i>
                                                                                    <i class="fa fa-star"
                                                                                       aria-hidden="true"></i>
                                                                                    <i class="fa fa-star"
                                                                                       aria-hidden="true"></i>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-testimonial-image">
                                                                                <img
                                                                                    src="https://demo2.wpopal.com/startor/wp-content/uploads/elementor/thumbs/02m-o6vswikpg8147jtt4v1j5tbxo8o4fo7z8dxkigk4ha.jpg"
                                                                                    title="02m"
                                                                                    alt="02m"/>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-testimonial-details">
                                                                                <div
                                                                                    class="elementor-testimonial-name">
                                                                                    Mark
                                                                                    Leon
                                                                                </div>
                                                                                <div
                                                                                    class="elementor-testimonial-job">
                                                                                    Designer
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        class="elementor-testimonial-item column-item">
                                                                        <div
                                                                            class="item-box">
                                                                            <div
                                                                                class="elementor-testimonial-content-box">
                                                                                <div
                                                                                    class="elementor-testimonial-quote">
                                                                                    <i class="opal-icon-quote"
                                                                                       aria-hidden="true"></i>
                                                                                </div>
                                                                                <div
                                                                                    class="elementor-testimonial-content">
                                                                                    "I
                                                                                    would
                                                                                    be
                                                                                    lost
                                                                                    without
                                                                                    Startor.
                                                                                    I
                                                                                    could'nt
                                                                                    have
                                                                                    asked
                                                                                    for
                                                                                    more
                                                                                    than
                                                                                    this.
                                                                                    Thanks
                                                                                    for
                                                                                    the
                                                                                    great
                                                                                    service."
                                                                                </div>
                                                                                <div
                                                                                    class="elementor-testimonial-rating">
                                                                                    <i class="fa fa-star"
                                                                                       aria-hidden="true"></i>
                                                                                    <i class="fa fa-star"
                                                                                       aria-hidden="true"></i>
                                                                                    <i class="fa fa-star"
                                                                                       aria-hidden="true"></i>
                                                                                    <i class="fa fa-star"
                                                                                       aria-hidden="true"></i>
                                                                                    <i class="fa fa-star"
                                                                                       aria-hidden="true"></i>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-testimonial-image">
                                                                                <img
                                                                                    src="https://demo2.wpopal.com/startor/wp-content/uploads/elementor/thumbs/03w-o6vswikpg8147jtt4v1j5tbxo8o4fo7z8dxkigk4ha.jpg"
                                                                                    title="03w"
                                                                                    alt="03w"/>
                                                                            </div>
                                                                            <div
                                                                                class="elementor-testimonial-details">
                                                                                <div
                                                                                    class="elementor-testimonial-name">
                                                                                    Emma
                                                                                    Stone
                                                                                </div>
                                                                                <div
                                                                                    class="elementor-testimonial-job">
                                                                                    Graphic
                                                                                    Design
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div
        class="elementor-element elementor-element-6325a04 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
        data-id="6325a04" data-element_type="section">
        <div class="elementor-container elementor-column-gap-no">
            <div class="elementor-row">
                <div
                    class="elementor-element elementor-element-a431acc animated-fast elementor-invisible elementor-column elementor-col-50 elementor-top-column"
                    data-id="a431acc" data-element_type="column"
                    data-settings="{&quot;animation&quot;:&quot;opal-move-up&quot;}">
                    <div class="elementor-column-wrap  elementor-element-populated">
                        <div class="elementor-widget-wrap">
                            <div
                                class="elementor-element elementor-element-7a21e10 elementor-widget elementor-widget-image"
                                data-id="7a21e10" data-element_type="widget"
                                data-widget_type="image.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-image"><img width="700"
                                                                      height="525"
                                                                      src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/04/image_home1.png"
                                                                      class="attachment-full size-full"
                                                                      alt=""
                                                                      srcset="https://demo2.wpopal.com/startor/wp-content/uploads/2019/04/image_home1.png 700w, https://demo2.wpopal.com/startor/wp-content/uploads/2019/04/image_home1-300x225.png 300w"
                                                                      sizes="(max-width: 700px) 100vw, 700px"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="elementor-element elementor-element-f5a2dd8 elementor-column elementor-col-50 elementor-top-column"
                    data-id="f5a2dd8" data-element_type="column">
                    <div class="elementor-column-wrap  elementor-element-populated">
                        <div class="elementor-widget-wrap">
                            <div
                                class="elementor-element elementor-element-cdcfaea animated-fast elementor-mobile-align-left elementor-invisible elementor-widget elementor-widget-heading"
                                data-id="cdcfaea" data-element_type="widget"
                                data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;}"
                                data-widget_type="heading.default">
                                <div class="elementor-widget-container"><h2
                                        class="elementor-heading-title elementor-size-default">
                                        What makes Startor<br> different?</h2></div>
                            </div>
                            <div
                                class="elementor-element elementor-element-9b16988 animated-fast elementor-invisible elementor-widget elementor-widget-text-editor"
                                data-id="9b16988" data-element_type="widget"
                                data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;}"
                                data-widget_type="text-editor.default">
                                <div class="elementor-widget-container">
                                    <div
                                        class="elementor-text-editor elementor-clearfix">
                                        Experience a level of our quality in both design
                                        &amp; customization works.Whether it&#8217;s for
                                        you or by you, capture a good deed to get
                                        started.
                                    </div>
                                </div>
                            </div>
                            <div
                                class="elementor-element elementor-element-503ba02 animated-fast elementor-invisible elementor-widget elementor-widget-icon-list"
                                data-id="503ba02" data-element_type="widget"
                                data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;}"
                                data-widget_type="icon-list.default">
                                <div class="elementor-widget-container">
                                    <ul class="elementor-icon-list-items">
                                        <li class="elementor-icon-list-item"><a
                                                href="#"> <span
                                                    class="elementor-icon-list-icon"> <i
                                                        class="fa fa-circle"
                                                        aria-hidden="true"></i> </span>
                                                <span class="elementor-icon-list-text">Award Winning Projects</span>
                                            </a></li>
                                        <li class="elementor-icon-list-item"><a
                                                href="#"> <span
                                                    class="elementor-icon-list-icon"> <i
                                                        class="fa fa-circle"
                                                        aria-hidden="true"></i> </span>
                                                <span class="elementor-icon-list-text">Powerfull Product Strategy</span>
                                            </a></li>
                                        <li class="elementor-icon-list-item"><a
                                                href="#"> <span
                                                    class="elementor-icon-list-icon"> <i
                                                        class="fa fa-circle"
                                                        aria-hidden="true"></i> </span>
                                                <span
                                                    class="elementor-icon-list-text">Quality Code & Programming </span>
                                            </a></li>
                                        <li class="elementor-icon-list-item"><a
                                                href="#"> <span
                                                    class="elementor-icon-list-icon"> <i
                                                        class="fa fa-circle"
                                                        aria-hidden="true"></i> </span>
                                                <span class="elementor-icon-list-text">Customer Support & Updates</span>
                                            </a></li>
                                    </ul>
                                </div>
                            </div>
                            <div
                                class="elementor-element elementor-element-00b5892 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                data-id="00b5892" data-element_type="section">
                                <div
                                    class="elementor-container elementor-column-gap-no">
                                    <div class="elementor-row">
                                        <div
                                            class="elementor-element elementor-element-bf4c250 elementor-column elementor-col-100 elementor-inner-column"
                                            data-id="bf4c250"
                                            data-element_type="column">
                                            <div
                                                class="elementor-column-wrap  elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div
                                                        class="elementor-element elementor-element-25b7066 animated-fast elementor-widget__width-auto elementor-widget-mobile__width-inherit elementor-button-primary elementor-invisible elementor-widget elementor-widget-button"
                                                        data-id="25b7066"
                                                        data-element_type="widget"
                                                        data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;}"
                                                        data-widget_type="button.default">
                                                        <div
                                                            class="elementor-widget-container">
                                                            <div
                                                                class="elementor-button-wrapper">
                                                                <a href="#"
                                                                   class="elementor-button-link elementor-button elementor-size-md"
                                                                   role="button"> <span
                                                                        class="elementor-button-content-wrapper"> <span
                                                                            class="elementor-button-text">Try It Out</span> </span>
                                                                </a></div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="elementor-element elementor-element-bb80f0d elementor-button-link animated-fast elementor-widget__width-auto elementor-widget-mobile__width-inherit elementor-invisible elementor-widget elementor-widget-button"
                                                        data-id="bb80f0d"
                                                        data-element_type="widget"
                                                        data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;}"
                                                        data-widget_type="button.default">
                                                        <div
                                                            class="elementor-widget-container">
                                                            <div
                                                                class="elementor-button-wrapper">
                                                                <a href="https://demo2.wpopal.com/startor/contact-1/"
                                                                   class="elementor-button-link elementor-button elementor-size-md"
                                                                   target="_blank"
                                                                   role="button"> <span
                                                                        class="elementor-button-content-wrapper"> <span
                                                                            class="elementor-button-text">Contact us</span> </span>
                                                                </a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div
        class="elementor-element elementor-element-39e3901 animated-fast elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-invisible elementor-section elementor-top-section"
        data-id="39e3901" data-element_type="section"
        data-settings="{&quot;animation&quot;:&quot;opal-move-up&quot;}">
        <div class="elementor-container elementor-column-gap-no">
            <div class="elementor-row">
                <div
                    class="elementor-element elementor-element-1b6c94a elementor-column elementor-col-100 elementor-top-column"
                    data-id="1b6c94a" data-element_type="column">
                    <div class="elementor-column-wrap  elementor-element-populated">
                        <div class="elementor-widget-wrap">
                            <div
                                class="elementor-element elementor-element-d78c1de elementor-widget elementor-widget-divider"
                                data-id="d78c1de" data-element_type="widget"
                                data-widget_type="divider.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-divider"><span
                                            class="elementor-divider-separator"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div
        class="elementor-element elementor-element-98e6321 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
        data-id="98e6321" data-element_type="section">
        <div class="elementor-container elementor-column-gap-no">
            <div class="elementor-row">
                <div
                    class="elementor-element elementor-element-237632b elementor-column elementor-col-100 elementor-top-column"
                    data-id="237632b" data-element_type="column">
                    <div class="elementor-column-wrap  elementor-element-populated">
                        <div class="elementor-widget-wrap">
                            <div
                                class="elementor-element elementor-element-31531be elementor-align-center animated-fast elementor-invisible elementor-widget elementor-widget-heading"
                                data-id="31531be" data-element_type="widget"
                                data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;}"
                                data-widget_type="heading.default">
                                <div class="elementor-widget-container"><span
                                        class="sub-title">Our Pricing</span>
                                    <h2 class="elementor-heading-title elementor-size-default">
                                        Choose your pack and<br> enjoy Startor</h2>
                                </div>
                            </div>
                            <div
                                class="elementor-element elementor-element-c0b685f animated-fast elementor-invisible elementor-widget elementor-widget-text-editor"
                                data-id="c0b685f" data-element_type="widget"
                                data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;}"
                                data-widget_type="text-editor.default">
                                <div class="elementor-widget-container">
                                    <div
                                        class="elementor-text-editor elementor-clearfix">
                                        Choose the most suitable service for your needs
                                        with reasonable price.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div
        class="elementor-element elementor-element-6477d49 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
        data-id="6477d49" data-element_type="section">
        <div class="elementor-container elementor-column-gap-no">
            <div class="elementor-row">
                <div
                    class="elementor-element elementor-element-6f9e77d animated-fast elementor-invisible elementor-column elementor-col-50 elementor-top-column"
                    data-id="6f9e77d" data-element_type="column"
                    data-settings="{&quot;animation&quot;:&quot;opal-move-up&quot;}">
                    <div class="elementor-column-wrap  elementor-element-populated">
                        <div class="elementor-widget-wrap">
                            <div
                                class="elementor-element elementor-element-2ae9829 elementor-button-default elementor-price-table-center elementor-button-justify elementor-widget elementor-widget-opal-price-table"
                                data-id="2ae9829" data-element_type="widget"
                                data-widget_type="opal-price-table.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-price-table">
                                        <div
                                            class="elementor-price-table__wrapper-header">
                                            <div
                                                class="elementor-price-table__icon-wrapper">
                                                                                <span class="elementor-icon"> <i
                                                                                        class="opal-icon-pricing_professional"
                                                                                        aria-hidden="true"></i> <span
                                                                                        class="price-table-icon-bkg"></span> </span>
                                            </div>
                                            <div class="elementor-price-table__header">
                                                <h3 class="elementor-price-table__heading">
                                                    Professional</h3></div>
                                            <div class="elementor-price-table__price">
                                                                                <span
                                                                                    class="elementor-price-table__currency">&#36;</span>
                                                <span
                                                    class="elementor-price-table__integer-part">59</span>
                                            </div>
                                            <div class="elementor-price-table__period">
                                                Per <span>month</span></div>
                                        </div>
                                        <ul class="elementor-price-table__features-list">
                                            <li class="elementor-repeater-item-9ab74ab">
                                                <div
                                                    class="elementor-price-table__feature-inner">
                                                    <span class="item-active"> 500 Max Connections </span>
                                                </div>
                                            </li>
                                            <li class="elementor-repeater-item-e475c8a">
                                                <div
                                                    class="elementor-price-table__feature-inner">
                                                    <span class="item-active"> Limited Support </span>
                                                </div>
                                            </li>
                                            <li class="elementor-repeater-item-a45c47b">
                                                <div
                                                    class="elementor-price-table__feature-inner">
                                                    <span class="item-active"> One click demo </span>
                                                </div>
                                            </li>
                                            <li class="elementor-repeater-item-0642dda">
                                                <div
                                                    class="elementor-price-table__feature-inner">
                                                    <span class="item-active"> SSL Protection </span>
                                                </div>
                                            </li>
                                            <li class="elementor-repeater-item-e0aa491">
                                                <div
                                                    class="elementor-price-table__feature-inner">
                                                    <span class="item-active"> Auto Updates </span>
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="elementor-price-table__footer"><a
                                                class="elementor-price-table__button elementor-button elementor-size-md"
                                                href="https://demo2.wpopal.com/startor/contact-1/"
                                                target="_blank">Get Started</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="elementor-element elementor-element-001d8c4 animated-fast elementor-invisible elementor-column elementor-col-50 elementor-top-column"
                    data-id="001d8c4" data-element_type="column"
                    data-settings="{&quot;animation&quot;:&quot;opal-move-up&quot;,&quot;animation_delay&quot;:200}">
                    <div class="elementor-column-wrap  elementor-element-populated">
                        <div class="elementor-widget-wrap">
                            <div
                                class="elementor-element elementor-element-da390bd elementor-button-default elementor-price-table-center elementor-button-justify elementor-widget elementor-widget-opal-price-table"
                                data-id="da390bd" data-element_type="widget"
                                data-widget_type="opal-price-table.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-price-table">
                                        <div
                                            class="elementor-price-table__wrapper-header">
                                            <div
                                                class="elementor-price-table__icon-wrapper">
                                                                                <span class="elementor-icon"> <i
                                                                                        class="opal-icon-pricing_personal"
                                                                                        aria-hidden="true"></i> <span
                                                                                        class="price-table-icon-bkg"></span> </span>
                                            </div>
                                            <div class="elementor-price-table__header">
                                                <h3 class="elementor-price-table__heading">
                                                    Personal</h3></div>
                                            <div class="elementor-price-table__price">
                                                                                <span
                                                                                    class="elementor-price-table__currency">&#36;</span>
                                                <span
                                                    class="elementor-price-table__integer-part">39</span>
                                            </div>
                                            <div class="elementor-price-table__period">
                                                Per <span>month</span></div>
                                        </div>
                                        <ul class="elementor-price-table__features-list">
                                            <li class="elementor-repeater-item-9ab74ab">
                                                <div
                                                    class="elementor-price-table__feature-inner">
                                                    <span class="item-active"> 500 Max Connections </span>
                                                </div>
                                            </li>
                                            <li class="elementor-repeater-item-e475c8a">
                                                <div
                                                    class="elementor-price-table__feature-inner">
                                                    <span class="item-active"> Limited Support </span>
                                                </div>
                                            </li>
                                            <li class="elementor-repeater-item-a45c47b">
                                                <div
                                                    class="elementor-price-table__feature-inner">
                                                    <span class="item-active"> One click demo </span>
                                                </div>
                                            </li>
                                            <li class="elementor-repeater-item-0642dda">
                                                <div
                                                    class="elementor-price-table__feature-inner">
                                                    <span> SSL Protection </span></div>
                                            </li>
                                            <li class="elementor-repeater-item-e0aa491">
                                                <div
                                                    class="elementor-price-table__feature-inner">
                                                    <span> Auto Updates </span></div>
                                            </li>
                                        </ul>
                                        <div class="elementor-price-table__footer"><a
                                                class="elementor-price-table__button elementor-button elementor-size-md"
                                                href="https://demo2.wpopal.com/startor/contact-1/"
                                                target="_blank">Get Started</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div
        class="elementor-element elementor-element-821e011 animated-fast elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-invisible elementor-section elementor-top-section"
        data-id="821e011" data-element_type="section"
        data-settings="{&quot;animation&quot;:&quot;opal-move-up&quot;}">
        <div class="elementor-container elementor-column-gap-no">
            <div class="elementor-row">
                <div
                    class="elementor-element elementor-element-744d450 elementor-column elementor-col-100 elementor-top-column"
                    data-id="744d450" data-element_type="column">
                    <div class="elementor-column-wrap  elementor-element-populated">
                        <div class="elementor-widget-wrap">
                            <div
                                class="elementor-element elementor-element-117005c elementor-widget__width-auto elementor-widget elementor-widget-text-editor"
                                data-id="117005c" data-element_type="widget"
                                data-widget_type="text-editor.default">
                                <div class="elementor-widget-container">
                                    <div
                                        class="elementor-text-editor elementor-clearfix">
                                        <p>Don&#8217;t hesitate, contact us for better
                                            help and services. <a
                                                style="color: #000000; font-weight: 500;"
                                                href="https://demo2.wpopal.com/startor/contact-1/">Lets
                                                get started</a></p></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div
        class="elementor-element elementor-element-aa75aa0 elementor-section-stretched elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
        data-id="aa75aa0" data-element_type="section"
        data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-container elementor-column-gap-no">
            <div class="elementor-row">
                <div
                    class="elementor-element elementor-element-6bc4f73 elementor-column elementor-col-100 elementor-top-column"
                    data-id="6bc4f73" data-element_type="column">
                    <div class="elementor-column-wrap  elementor-element-populated">
                        <div class="elementor-widget-wrap">
                            <div
                                class="elementor-element elementor-element-ff35a2a elementor-mobile-align-center animated-fast elementor-invisible elementor-widget elementor-widget-heading"
                                data-id="ff35a2a" data-element_type="widget"
                                data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;}"
                                data-widget_type="heading.default">
                                <div class="elementor-widget-container"><span
                                        class="sub-title">Get The App</span>
                                    <h2 class="elementor-heading-title elementor-size-default">
                                        Create your story<br> share it with Startor</h2>
                                </div>
                            </div>
                            <div
                                class="elementor-element elementor-element-6c2fb29 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                data-id="6c2fb29" data-element_type="section">
                                <div
                                    class="elementor-container elementor-column-gap-no">
                                    <div class="elementor-row">
                                        <div
                                            class="elementor-element elementor-element-e5048ca animated-fast elementor-invisible elementor-column elementor-col-50 elementor-inner-column"
                                            data-id="e5048ca" data-element_type="column"
                                            data-settings="{&quot;animation&quot;:&quot;opal-move-up&quot;}">
                                            <div
                                                class="elementor-column-wrap  elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div
                                                        class="elementor-element elementor-element-19744dc animated-fast elementor-invisible elementor-widget elementor-widget-image"
                                                        data-id="19744dc"
                                                        data-element_type="widget"
                                                        data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;,&quot;_animation_delay&quot;:150}"
                                                        data-widget_type="image.default">
                                                        <div
                                                            class="elementor-widget-container">
                                                            <div
                                                                class="elementor-image">
                                                                <a href="#"
                                                                   data-elementor-open-lightbox="">
                                                                    <img width="190"
                                                                         height="62"
                                                                         src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/googplay.png"
                                                                         class="elementor-animation-shrink attachment-full size-full"
                                                                         alt=""/> </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="elementor-element elementor-element-5837242 animated-fast elementor-invisible elementor-column elementor-col-50 elementor-inner-column"
                                            data-id="5837242" data-element_type="column"
                                            data-settings="{&quot;animation&quot;:&quot;opal-move-up&quot;,&quot;animation_delay&quot;:200}">
                                            <div
                                                class="elementor-column-wrap  elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div
                                                        class="elementor-element elementor-element-7e5bff4 animated-fast elementor-invisible elementor-widget elementor-widget-image"
                                                        data-id="7e5bff4"
                                                        data-element_type="widget"
                                                        data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;,&quot;_animation_delay&quot;:300}"
                                                        data-widget_type="image.default">
                                                        <div
                                                            class="elementor-widget-container">
                                                            <div
                                                                class="elementor-image">
                                                                <a href="#"
                                                                   data-elementor-open-lightbox="">
                                                                    <img width="190"
                                                                         height="62"
                                                                         src="https://demo2.wpopal.com/startor/wp-content/uploads/2019/03/appstore.png"
                                                                         class="elementor-animation-shrink attachment-full size-full"
                                                                         alt=""/> </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
