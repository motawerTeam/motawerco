@extends('layouts.app')
@section('title')
    Contact Us
@endsection
@section('menu')
    active
@endsection

@section('header')
    <style>
        .elementor-599 .elementor-element.elementor-element-be15109 {
            background-color: #212d4b !important;
        }

        .home .page-title-bar {
            display: block !important;
            width: 100% !important;
        }
    </style>
    <div id="page-title-bar" class="page-title-bar">
        <div class="container">
            <div class="wrap w-100 d-flex align-items-center">
                <div class="page-title-bar-inner d-flex flex-column align-items-center w-100">
                    <div class="page-header"><h1 class="page-title typo-heading">Contact Us</h1></div>
                    <div class="breadcrumb"><span property="itemListElement" typeof="ListItem">
                            <a property="item"
                               typeof="WebPage"
                               title="Go to Startor."
                               href="{{url('/')}}"
                               class="home"><span
                                    property="name">Motawer</span></a><meta property="position" content="1"></span> &gt;
                        <span class="post post-page current-item">Contact Us</span></div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <br>
    <div class="container">
        <div class="site-content-contain">
            <div id="content" class="site-content">
                <div class="wrap">
                    <div id="primary" class="content-area">
                        <main id="main" class="site-main">
                            <div data-elementor-type="post" data-elementor-id="1327" class="elementor elementor-1327"
                                 data-elementor-settings="[]">
                                <div class="elementor-inner">
                                    <div class="elementor-section-wrap">
                                        <div
                                            class="elementor-element elementor-element-bcea30a elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                                            data-id="bcea30a" data-element_type="section"
                                            data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
                                            <div class="elementor-container elementor-column-gap-no">
                                                <div class="elementor-row">
                                                    <div
                                                        class="elementor-element elementor-element-775e150 animated-fast elementor-invisible elementor-column elementor-col-33 elementor-top-column"
                                                        data-id="775e150" data-element_type="column"
                                                        data-settings="{&quot;animation&quot;:&quot;opal-move-up&quot;}">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div
                                                                    class="elementor-element elementor-element-282b2d8 elementor-position-left elementor-view-default elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                    data-id="282b2d8" data-element_type="widget"
                                                                    data-widget_type="icon-box.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-box-wrapper">
                                                                            <div class="elementor-icon-box-icon"><span
                                                                                    class="elementor-icon elementor-animation-"> <i
                                                                                        class="opal-icon-location"
                                                                                        aria-hidden="true"></i> </span>
                                                                            </div>
                                                                            <div class="elementor-icon-box-content">
                                                                                <span
                                                                                    class="elementor-icon-box-subtitle"></span>
                                                                                <h3 class="elementor-icon-box-title">
                                                                                    <span>Location</span></h3>
                                                                                <p class="elementor-icon-box-description">
                                                                                    44 Shirley Ave. West Chicago,
                                                                                    New York, USA</p></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="elementor-element elementor-element-166e4e9 animated-fast elementor-invisible elementor-column elementor-col-33 elementor-top-column"
                                                        data-id="166e4e9" data-element_type="column"
                                                        data-settings="{&quot;animation&quot;:&quot;opal-move-up&quot;,&quot;animation_delay&quot;:200}">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div
                                                                    class="elementor-element elementor-element-bbb3c9e elementor-position-left elementor-view-default elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                    data-id="bbb3c9e" data-element_type="widget"
                                                                    data-widget_type="icon-box.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-box-wrapper">
                                                                            <div class="elementor-icon-box-icon"><span
                                                                                    class="elementor-icon elementor-animation-"> <i
                                                                                        class="opal-icon-phone"
                                                                                        aria-hidden="true"></i> </span>
                                                                            </div>
                                                                            <div class="elementor-icon-box-content">
                                                                                <span
                                                                                    class="elementor-icon-box-subtitle"></span>
                                                                                <h3 class="elementor-icon-box-title">
                                                                                    <span>Call support</span></h3>
                                                                                <p class="elementor-icon-box-description">
                                                                                    8(800) 123 - 45 - 99<br> 8(800) 123
                                                                                    - 45 - 66</p></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="elementor-element elementor-element-3a8f34f animated-fast elementor-invisible elementor-column elementor-col-33 elementor-top-column"
                                                        data-id="3a8f34f" data-element_type="column"
                                                        data-settings="{&quot;animation&quot;:&quot;opal-move-up&quot;,&quot;animation_delay&quot;:350}">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div
                                                                    class="elementor-element elementor-element-9b3fdc1 elementor-position-left elementor-view-default elementor-vertical-align-top elementor-widget elementor-widget-icon-box"
                                                                    data-id="9b3fdc1" data-element_type="widget"
                                                                    data-widget_type="icon-box.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-icon-box-wrapper">
                                                                            <div class="elementor-icon-box-icon"><span
                                                                                    class="elementor-icon elementor-animation-"> <i
                                                                                        class="opal-icon-email"
                                                                                        aria-hidden="true"></i> </span>
                                                                            </div>
                                                                            <div class="elementor-icon-box-content">
                                                                                <span
                                                                                    class="elementor-icon-box-subtitle"></span>
                                                                                <h3 class="elementor-icon-box-title">
                                                                                    <span>Email us</span></h3>
                                                                                <p class="elementor-icon-box-description">
                                                                                    consultek@example.com <br>
                                                                                    support@example.com</p></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="elementor-element elementor-element-8b65b03 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                                            data-id="8b65b03" data-element_type="section"
                                            data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
                                            <div class="elementor-container elementor-column-gap-no">
                                                <div class="elementor-row">
                                                    <div
                                                        class="elementor-element elementor-element-658f4db animated-fast elementor-invisible elementor-column elementor-col-50 elementor-top-column"
                                                        data-id="658f4db" data-element_type="column"
                                                        data-settings="{&quot;animation&quot;:&quot;opal-move-up&quot;}">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div
                                                                    class="elementor-element elementor-element-a6fd72e elementor-widget elementor-widget-google_maps"
                                                                    data-id="a6fd72e" data-element_type="widget"
                                                                    data-widget_type="google_maps.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-custom-embed">
                                                                            <iframe frameborder="0" scrolling="no"
                                                                                    marginheight="0" marginwidth="0"
                                                                                    src="https://maps.google.com/maps?q=London%20Eye%2C%20London%2C%20United%20Kingdom&amp;t=m&amp;z=10&amp;output=embed&amp;iwloc=near"
                                                                                    aria-label="London Eye, London, United Kingdom"></iframe>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="elementor-element elementor-element-69127e4 elementor-column elementor-col-50 elementor-top-column"
                                                        data-id="69127e4" data-element_type="column">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div
                                                                    class="elementor-element elementor-element-7d888bf animated-fast elementor-invisible elementor-widget elementor-widget-heading"
                                                                    data-id="7d888bf" data-element_type="widget"
                                                                    data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;}"
                                                                    data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container"><span
                                                                            class="sub-title">Get In Touch</span>
                                                                        <h2 class="elementor-heading-title elementor-size-default">
                                                                            We love to hear from you</h2></div>
                                                                </div>
                                                                <div
                                                                    class="elementor-element elementor-element-a04bef3 animated-fast elementor-invisible elementor-widget elementor-widget-text-editor"
                                                                    data-id="a04bef3" data-element_type="widget"
                                                                    data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;,&quot;_animation_delay&quot;:200}"
                                                                    data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div
                                                                            class="elementor-text-editor elementor-clearfix">
                                                                            Subscribe to our newsletters, call us to
                                                                            book a meetup or send us emails to request
                                                                            for service consultation.
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="elementor-element elementor-element-db403cc animated-fast elementor-wpcf7-button-primary elementor-wpcf7-button-md elementor-invisible elementor-widget elementor-widget-opal-contactform7"
                                                                    data-id="db403cc" data-element_type="widget"
                                                                    data-settings="{&quot;_animation&quot;:&quot;opal-move-up&quot;,&quot;_animation_delay&quot;:350}"
                                                                    data-widget_type="opal-contactform7.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div role="form" class="wpcf7"
                                                                             id="wpcf7-f4-p1327-o1" lang="en-US"
                                                                             dir="ltr">
                                                                            <div class="screen-reader-response"></div>
                                                                            <form
                                                                                action="https://demo2.wpopal.com/startor/contact-2/#wpcf7-f4-p1327-o1"
                                                                                method="post" class="wpcf7-form"
                                                                                novalidate="novalidate">
                                                                                <div style="display: none;"><input
                                                                                        type="hidden" name="_wpcf7"
                                                                                        value="4"/> <input type="hidden"
                                                                                                           name="_wpcf7_version"
                                                                                                           value="5.1.1"/>
                                                                                    <input type="hidden"
                                                                                           name="_wpcf7_locale"
                                                                                           value="en_US"/> <input
                                                                                        type="hidden"
                                                                                        name="_wpcf7_unit_tag"
                                                                                        value="wpcf7-f4-p1327-o1"/>
                                                                                    <input type="hidden"
                                                                                           name="_wpcf7_container_post"
                                                                                           value="1327"/> <input
                                                                                        type="hidden"
                                                                                        name="g-recaptcha-response"
                                                                                        value=""/></div>
                                                                                <div class="row">
                                                                                    <div class="col-md-6"><p><label>
                                                                                                Your Name *<br/> <span
                                                                                                    class="wpcf7-form-control-wrap your-name"><input
                                                                                                        type="text"
                                                                                                        name="your-name"
                                                                                                        value=""
                                                                                                        size="40"
                                                                                                        class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                                        aria-required="true"
                                                                                                        aria-invalid="false"
                                                                                                        placeholder="e.g., John Doe"/></span>
                                                                                            </label></p></div>
                                                                                    <div class="col-md-6"><label> Your
                                                                                            Email *<br/> <span
                                                                                                class="wpcf7-form-control-wrap your-email"><input
                                                                                                    type="email"
                                                                                                    name="your-email"
                                                                                                    value="" size="40"
                                                                                                    class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                                    aria-required="true"
                                                                                                    aria-invalid="false"
                                                                                                    placeholder="name@example.com"/></span>
                                                                                        </label></div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-6"><p><label>
                                                                                                Your Phone *<br/> <span
                                                                                                    class="wpcf7-form-control-wrap your-phone"><input
                                                                                                        type="text"
                                                                                                        name="your-phone"
                                                                                                        value=""
                                                                                                        size="40"
                                                                                                        class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                                        aria-required="true"
                                                                                                        aria-invalid="false"
                                                                                                        placeholder="Phone number"/></span>
                                                                                            </label></p></div>
                                                                                    <div class="col-md-6"><label>
                                                                                            Subject<br/> <span
                                                                                                class="wpcf7-form-control-wrap your-subject"><input
                                                                                                    type="text"
                                                                                                    name="your-subject"
                                                                                                    value=""
                                                                                                    size="40"
                                                                                                    class="wpcf7-form-control wpcf7-text"
                                                                                                    aria-invalid="false"
                                                                                                    placeholder="Service name"/></span>
                                                                                        </label></div>
                                                                                </div>
                                                                                <p><label> Your Message<br/> <span
                                                                                            class="wpcf7-form-control-wrap your-message"><textarea
                                                                                                name="your-message"
                                                                                                cols="40"
                                                                                                rows="4"
                                                                                                class="wpcf7-form-control wpcf7-textarea"
                                                                                                aria-invalid="false"
                                                                                                placeholder="Text here"></textarea></span>
                                                                                    </label></p>
                                                                                <div class="text-center"><p><input
                                                                                            type="submit"
                                                                                            value="Send Message"
                                                                                            class="wpcf7-form-control wpcf7-submit w-100"/>
                                                                                    </p></div>
                                                                                <div
                                                                                    class="wpcf7-response-output wpcf7-display-none"></div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </main>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
